#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <cstdlib>
#include <sstream>
#include <sys/stat.h>

#include <pcl/common/common_headers.h>
#include <pcl/filters/passthrough.h>
#include <pcl/io/ply_io.h>
#include <pcl/conversions.h>

#include <Eigen/Dense>

typedef pcl::PointXYZRGBNormal Point;
typedef pcl::PointCloud<Point> Cloud;

/**
 * load the point cloud from ply file
 * @param filename
 * @return
 */
Cloud::Ptr
load_ply (const std::string &filename)
{
  pcl::PCLPointCloud2::Ptr cloud_in (new pcl::PCLPointCloud2 ());
  Cloud::Ptr cloud (new Cloud ());
  pcl::PLYReader reader;
  reader.read (filename, *cloud_in);
  pcl::fromPCLPointCloud2 (*cloud_in, *cloud);
  return cloud;
}

/**
 * whether file exists
 * @param name
 * @return
 */
bool
is_file_valid (const std::string &name)
{
  struct stat buffer;
  return (stat (name.c_str (), &buffer) == 0);
}

/**
 * pass through filter on point cloud
 * @param cloud_in
 * @param limx
 * @param limy
 * @param limz
 * @return
 */
Cloud::Ptr
pass_through (const Cloud::ConstPtr &cloud_in,
              const Eigen::Vector2f &limx,
              const Eigen::Vector2f &limy,
              const Eigen::Vector2f &limz)
{
  Cloud::Ptr filtered (new Cloud ());
  pcl::PassThrough<Point> pass;

  pass.setInputCloud (cloud_in);
  pass.setFilterFieldName ("x");
  pass.setFilterLimits (limx (0), limx (1));
  pass.filter (*filtered);

  pass.setInputCloud (filtered);
  pass.setFilterFieldName ("y");
  pass.setFilterLimits (limy (0), limy (1));
  pass.filter (*filtered);

  pass.setInputCloud (filtered);
  pass.setFilterFieldName ("z");
  pass.setFilterLimits (limz (0), limz (1));
  pass.filter (*filtered);

  return filtered;
}

void
print_help ()
{
  std::cout << "./band_pass path_input_ply path_output_ply min_x max_x min_y max_y min_z min_z"
            << std::endl;
}

int main (int argc, char **argv)
{
  if (argc <= 1)
  {
    print_help ();
    return 1;
  }

  const std::string fn_in (argv[1]);
  const std::string fn_out (argv[2]);
  const float min_x = (float) std::atof (argv[3]);
  const float max_x = (float) std::atof (argv[4]);
  const float min_y = (float) std::atof (argv[5]);
  const float max_y = (float) std::atof (argv[6]);
  const float min_z = (float) std::atof (argv[7]);
  const float max_z = (float) std::atof (argv[8]);

  if (!is_file_valid (fn_in))
  {
    std::cout << "file " << fn_in << "does not exist" << std::endl;
    return 1;
  }

  Cloud::Ptr cloud_in = load_ply (fn_in);
  Cloud::Ptr cloud_out = pass_through (cloud_in,
                                       Eigen::Vector2f (min_x, max_x),
                                       Eigen::Vector2f (min_y, max_y),
                                       Eigen::Vector2f (min_z, max_z));


  pcl::PLYWriter writer;
  writer.write<Point> (fn_out, *cloud_out, true, true);

  return 0;
}