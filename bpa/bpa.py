#! /bin/python2

import os
import sys
import threading

if len(sys.argv) < 4:
    print "argv insufficient"
    print "python2 bpa.py split_size searching_radius scene_name(room1)"
    sys.exit()

SIZE = float(sys.argv[1])
RADIUS = float(sys.argv[2])
SCENE_ID = sys.argv[3]

def is_proc(proc_name):
    return os.popen('ps aux').read().count(proc_name) > 1
    #return proc_name in os.popen('ps aux').read()

_SIZE_INT = int(round(SIZE * 2.0))
STR_SIZE = "{}_{}".format(_SIZE_INT / 2, (_SIZE_INT % 2) * 5)
HOME = os.getenv('HOME')
DIR_SRC = HOME + "/workspace/navvis_data/" + SCENE_ID + "/sub" + STR_SIZE
DIR_RECON = HOME+ "/workspace/navvis_data/" + SCENE_ID \
        + "/bpa/{}_{}".format(STR_SIZE, RADIUS)
EXE = HOME+ "/workspace/ma/bpa/build/bpa"

if os.path.isdir(DIR_RECON) and is_proc(" ".join(sys.argv)):
    with open("log", "a") as out:
        out.write(" ".join(sys.argv) + " in process, is it computed?\n")
    sys.exit()

#os.system("rm -rf " + DIR_RECON)
os.system("mkdir -p " + DIR_RECON)

#task_pool = os.listdir(DIR_SRC)
done_pool = os.listdir(DIR_RECON)
task_pool = [fn for fn in os.listdir(DIR_SRC) if (".ply" in fn) and (fn not in done_pool)]

for task in task_pool:
    fn_in = DIR_SRC + "/" + task
    fn_out = DIR_RECON + "/" + task
    os.system(EXE + " " + fn_in + " " + fn_out + " {}".format(RADIUS))
    print (EXE + " " + fn_in + " " + fn_out + " {}".format(RADIUS))

