#include <iostream>
#include <string>
#include <thread>
#include <chrono>
#include <mutex>
#include <vector>
#include <cstdlib>
#include <sstream>
#include <fstream>
#include <memory>
#include <sys/stat.h>
#include <unistd.h>

#include <Eigen/Dense>

#include <pcl/common/common_headers.h>
#include <pcl/io/ply_io.h>
#include <pcl/conversions.h>

#include "impl/Pivoter.hpp"
#include "Timer.hpp"

typedef pcl::PointNormal Point;
typedef pcl::PointCloud<Point> Cloud;

Cloud::Ptr
load_ply (const std::string &filename)
{
  pcl::PCLPointCloud2::Ptr cloud_in (new pcl::PCLPointCloud2 ());
  Cloud::Ptr cloud (new Cloud ());
  pcl::PLYReader reader;
  reader.read (filename, *cloud_in);
  pcl::fromPCLPointCloud2 (*cloud_in, *cloud);
  return cloud;
}

std::pair<std::string, std::string>
split_path_filename (const std::string &full_path)
{
  std::pair<std::string, std::string> re;
  std::size_t id = full_path.find_last_of ("/");

  re.first = full_path.substr (0, id);
  re.second = full_path.substr (id + 1);

  return re;
}

/**
 * read the starting time, ending time and duration from log file
 */
bool
read_log (const std::string &filename,
          double &duration,
          double &time0,
          double &time1)
{
  std::fstream in;
  std::string shit;
  in.open (filename, std::fstream::in);
  if (in.is_open ())
  {
    in >> shit >> shit >> shit >> duration >> shit >> shit
       >> time0 >> shit >> time1;
    in.close ();
    if (duration > 1e-3 && duration < 1e12)
    { // likely wrong input
      return true;
    }
  }
  return false;
}

/**
 * checks whether a files exist
 */
bool
is_file_exist (const std::string &filename)
{
  struct stat buffer;
  return (stat (filename.c_str (), &buffer) == 0);
}

void
print_help ()
{
  std::cout << "./bpa path_input_ply path_output_ply searching radius"
            << std::endl;
}

int
main (int argc, char **argv)
{
  if (argc <= 1)
  {
    print_help ();
    return 1;
  }
  const std::string fn_in (argv[1]);
  const std::string fn_out (argv[2]);
  const double radius = std::stof (argv[3]);

  std::pair<std::string, std::string> fn_o_split = split_path_filename (fn_out);

  // reconstruct with BPA
  Pivoter<Point> pivoter;
  Cloud::Ptr cloud = load_ply (fn_in);
  pivoter.setSearchRadius (radius);
  pivoter.setInputCloud (cloud);

  Timer timer;
  pcl::PolygonMesh::Ptr result = pivoter.proceed ();
  double time_elapsed = timer.getSecFromStart ();
  double time_start = timer.getStart ();
  double time_end = timer.getEnd ();
  // add time to log
  double duration_log, time0_log, time1_log; // from log
  if (read_log (fn_o_split.first + "/" + "log",
                duration_log,
                time0_log,
                time1_log))
  {
    time_elapsed += duration_log;
    time_start = time0_log;
  }

  if (!is_file_exist (fn_out))
  {
    pcl::io::savePLYFileBinary (fn_out, *result);

    std::ofstream os;
    os.open (fn_o_split.first + "/" + "log", std::ios_base::out);
    os << std::fixed << "running time is " << time_elapsed << ", from " << time_start
       << " to " << time_end << std::endl;
    os.close ();
  }

  return 0;
}
