//
// Created by tlou on 16.01.17.
//

#ifndef CLOUD_PROC_IO_HPP
#define CLOUD_PROC_IO_HPP

#include <iostream>

#include <string>
#include <sstream>
#include <fstream>
#include <boost/filesystem.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/PolygonMesh.h>

// with assumption that the point cloud has x, y, z, r, g, b
template <class T>
bool write_ply(const typename pcl::PointCloud<T>::ConstPtr& vertices,
               const std::vector<pcl::Vertices>& faces,
               const std::string &filename)
{
  std::stringstream ss;
  std::ofstream out;
  out.open(filename.c_str());
  const char CHAR_THREE = 3;

  ss << "ply" << std::endl;
  ss << "format binary_little_endian 1.0" << std::endl;
  ss << "element vertex " << vertices->size() << std::endl;
  ss << "property float x" << std::endl;
  ss << "property float y" << std::endl;
  ss << "property float z" << std::endl;
  ss << "property uchar red" << std::endl;
  ss << "property uchar green" << std::endl;
  ss << "property uchar blue" << std::endl;
  ss << "element face " << faces.size() << std::endl;
  ss << "property list uchar int vertex_index" << std::endl;
  ss << "end_header" << std::endl;
  out.write((const char*)ss.str().c_str(), ss.str().size());

  for(size_t id = 0; id < vertices->size(); ++id)
  {
    // get data after interval
    out.write((const char*)&(vertices->at(id).x), 12);
    out.write((const char*)&(vertices->at(id).r), 1);
    out.write((const char*)&(vertices->at(id).g), 1);
    out.write((const char*)&(vertices->at(id).b), 1);
  }

  for(size_t id = 0; id < faces.size(); ++id)
  {
    // get data after interval
    out.write(&CHAR_THREE, 1);
    out.write((const char *) &(faces.at(id).vertices.front()), 12);
  }

  out.close();
  return true;
}

#endif //CLOUD_PROC_IO_HPP
