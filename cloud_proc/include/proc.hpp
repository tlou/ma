//
// Created by tlou on 11.01.17.
//

#ifndef CLOUD_PROC_PROC_HPP
#define CLOUD_PROC_PROC_HPP

#include <string>
#include <vector>
#include <utility>
#include <boost/filesystem.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/ndt.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/surface/mls.h>
#include <pcl/surface/gp3.h>
#include <pcl/surface/poisson.h>

#include "surface/include/pcl/surface/marching_cubes.h"
#include "surface/include/pcl/surface/impl/marching_cubes.hpp"
#include "surface/include/pcl/surface/marching_cubes_hoppe.h"
#include "surface/include/pcl/surface/impl/marching_cubes_hoppe.hpp"
//#include "surface/include/pcl/surface/impl/marching_cubes_rbf.hpp"
//#include "surface/include/pcl/surface/marching_cubes_rbf.h"
#include <pcl/surface/marching_cubes_rbf.h>

#include "MarchingCubesHoppeBeta.hpp"

template<class T>
typename pcl::search::KdTree<T>::Ptr get_search_kdtree(const typename pcl::PointCloud<T>::ConstPtr &cloud)
{
  typename pcl::search::KdTree<T>::Ptr kdtree(new pcl::search::KdTree<T>);
  kdtree->setInputCloud(cloud);
  return kdtree;
}

template<class T>
typename pcl::PointCloud<T>::Ptr subsample_voxel_grid(const typename pcl::PointCloud<T>::ConstPtr &cloud,
                                                      const double voxel_size)
{
  typename pcl::PointCloud<T>::Ptr subsampled(new pcl::PointCloud<T>());
  pcl::VoxelGrid<T> sor;
  sor.setInputCloud(cloud);
  sor.setLeafSize(voxel_size, voxel_size, voxel_size);
  sor.filter(*subsampled);
  return subsampled;
}

template<class T>
typename pcl::PointCloud<T>::Ptr load_cloud_from_file(const std::string &filename)
{
  typename pcl::PointCloud<T>::Ptr cloud(new pcl::PointCloud<T>());
  pcl::io::loadPCDFile<T>(filename, *cloud);
  return cloud;
}

template<class T>
typename pcl::PointCloud<T>::Ptr concatenate_cloud(const typename pcl::PointCloud<T>::ConstPtr &cloud0,
                                                   const typename pcl::PointCloud<T>::ConstPtr &cloud1)
{
  typename pcl::PointCloud<T>::Ptr total(new pcl::PointCloud<T>);
  total->reserve(cloud0->size() + cloud1->size());
  total->insert(total->end(), cloud0->begin(), cloud0->end());
  total->insert(total->end(), cloud1->begin(), cloud1->end());
  return total;
}

template<class T>
Eigen::Matrix4f get_transform_ndt(const typename pcl::PointCloud<T>::ConstPtr &cloud_source,
                                  const typename pcl::PointCloud<T>::ConstPtr &cloud_target,
                                  const boost::shared_ptr<Eigen::Matrix4f> &init_pose = boost::shared_ptr<Eigen::Matrix4f>(),
                                  const double threshold = 0.01,
                                  const int max_iter = 50,
                                  const double resolution = 1.0,
                                  const double stepsize = 0.1)
{
  typename pcl::PointCloud<T>::Ptr cloud_aligned(new pcl::PointCloud<T>());
  typename pcl::NormalDistributionsTransform<T, T> ndt;
  ndt.setInputCloud(cloud_source);
  ndt.setInputTarget(cloud_target);
  ndt.setMaxCorrespondenceDistance(threshold);
  ndt.setMaximumIterations(max_iter);
  ndt.setTransformationEpsilon(1e-6);
  ndt.setEuclideanFitnessEpsilon(1);
  ndt.setResolution(resolution);
  ndt.setStepSize(stepsize);
  if(init_pose)
  {
    ndt.align(*cloud_aligned, *init_pose);
  }
  else
  {
    ndt.align(*cloud_aligned);
  }
  return ndt.getFinalTransformation();
}

template<class T>
Eigen::Matrix4f get_transform_icp(const typename pcl::PointCloud<T>::ConstPtr &cloud_source,
                                  const typename pcl::PointCloud<T>::ConstPtr &cloud_target,
                                  const boost::shared_ptr<Eigen::Matrix4f> &init_pose = boost::shared_ptr<Eigen::Matrix4f>(),
                                  const bool is_reciprocal = false,
                                  const double threshold = 0.01,
                                  const int max_iter = 50)
{
  typename pcl::PointCloud<T>::Ptr cloud_aligned(new pcl::PointCloud<T>());
  typename pcl::IterativeClosestPoint<T, T> icp;
  icp.setInputCloud(cloud_source);
  icp.setInputTarget(cloud_target);
  icp.setMaxCorrespondenceDistance(threshold);
  icp.setMaximumIterations(max_iter);
  icp.setUseReciprocalCorrespondences(is_reciprocal);
  icp.setTransformationEpsilon(1e-6);
  icp.setEuclideanFitnessEpsilon(1);
  if(init_pose)
  {
    icp.align(*cloud_aligned, *init_pose);
  }
  else
  {
    icp.align(*cloud_aligned);
  }
  return icp.getFinalTransformation();
}

template<class T>
typename pcl::PointCloud<T>::Ptr smooth_mls(const typename pcl::PointCloud<T>::ConstPtr &cloud,
                                            const double radius)
{
  typename pcl::PointCloud<T>::Ptr smoothed(new pcl::PointCloud<T>());
  typename pcl::MovingLeastSquares<T, T> mls;
  mls.setComputeNormals(false);
  mls.setInputCloud(cloud);
  mls.setPolynomialFit(true);
  mls.setSearchMethod(get_search_kdtree<T>(cloud));
  mls.setSearchRadius(radius);
  mls.process(*smoothed);
  return smoothed;
}

template<class T>
pcl::PolygonMesh::Ptr trianglulate_mesh(const typename pcl::PointCloud<T>::ConstPtr &cloud,
                                        const double radius)
{
  typename pcl::GreedyProjectionTriangulation<T> gpt;
  pcl::PolygonMesh::Ptr mesh(new pcl::PolygonMesh());

  // Set the maximum distance between connected points (maximum edge length)
  gpt.setSearchRadius(radius);

  gpt.setMu(2.5);
  gpt.setMaximumNearestNeighbors(50);
  gpt.setMaximumSurfaceAngle(M_PI / 4.0); // 45 degrees
  gpt.setMinimumAngle(M_PI / 18.0); // 10 degrees
  gpt.setMaximumAngle(2.0 * M_PI / 3.0); // 120 degrees
  gpt.setNormalConsistency(false);

  gpt.setInputCloud(cloud);
  gpt.setSearchMethod(get_search_kdtree<T>(cloud));
  gpt.reconstruct(*mesh);
  return mesh;
}

template<class T>
std::pair<typename pcl::PointCloud<T>::Ptr, std::vector<pcl::Vertices> >
trianglulate_mesh_poisson(const typename pcl::PointCloud<T>::ConstPtr &cloud,
                          const int depth)
{
  typename pcl::PointCloud<T>::Ptr vertices(new pcl::PointCloud<T>());
  std::vector<pcl::Vertices> faces;
  typename pcl::Poisson<T> poisson;
  poisson.setDepth(depth);
  poisson.setInputCloud(cloud);
  poisson.setSearchMethod(get_search_kdtree<T>(cloud));
//  poisson.reconstruct(*mesh);
  poisson.reconstruct(*vertices, faces);
  return std::pair<typename pcl::PointCloud<T>::Ptr, std::vector<pcl::Vertices> >(vertices, faces);
}

//template<class T>
//std::pair<typename pcl::PointCloud<T>::Ptr, std::vector<pcl::Vertices> >
//trianglulate_mesh_mc(const typename pcl::PointCloud<T>::ConstPtr &cloud,
//                     const int resolution, const float isoLevel)
//{
//  typename pcl::PointCloud<T>::Ptr vertices(new pcl::PointCloud<T>());
//  std::vector<pcl::Vertices> faces;
//  typename pcl::MarchingCubes<T> mc;
//  mc.setIsoLevel(isoLevel);
//  mc.setGridResolution(resolution, resolution, resolution);
//  mc.setInputCloud(cloud);
//  mc.setSearchMethod(get_search_kdtree<T>(cloud));
//  mc.reconstruct(*vertices, faces);
//  return std::pair<typename pcl::PointCloud<T>::Ptr, std::vector<pcl::Vertices> >(vertices, faces);
//}

template<class T>
std::pair<typename pcl::PointCloud<T>::Ptr, std::vector<pcl::Vertices> >
trianglulate_mesh_mc_hoppe(const typename pcl::PointCloud<T>::ConstPtr &cloud,
                           const int resolution_xy, const int resolution_z, const float isoLevel)
{
  typename pcl::PointCloud<T>::Ptr vertices(new pcl::PointCloud<T>());
  std::vector<pcl::Vertices> faces;
  // TODO modification from inheritance, may delete later
//  MarchingCubesHoppeBeta<T> mc;
//  mc.pcl::template MarchingCubesHoppe<T>::setInputCloud(cloud);
//  mc.pcl::template MarchingCubesHoppe<T>::setIsoLevel(isoLevel);
//  mc.pcl::template MarchingCubesHoppe<T>::setGridResolution(resolution_xy, resolution_xy, resolution_z);
//  mc.pcl::template MarchingCubesHoppe<T>::setSearchMethod(get_search_kdtree<T>(cloud));
//  mc.pcl::template MarchingCubesHoppe<T>::reconstruct(*vertices, faces);

  typename pcl::MarchingCubesHoppe<T> mc;
  mc.setInputCloud(cloud);
  mc.setIsoLevel(isoLevel);
  mc.setGridResolution(resolution_xy, resolution_xy, resolution_z);
  mc.setSearchMethod(get_search_kdtree<T>(cloud));
  // set distance threshold to half of grid size (approx)
  mc.setDistanceIgnore(0.5f * 1.2f / (float)resolution_xy);
  mc.reconstruct(*vertices, faces);
  return std::pair<typename pcl::PointCloud<T>::Ptr, std::vector<pcl::Vertices> >(vertices, faces);
}

template<class T>
std::pair<typename pcl::PointCloud<T>::Ptr, std::vector<pcl::Vertices> >
trianglulate_mesh_mc_rbf(const typename pcl::PointCloud<T>::ConstPtr &cloud,
                         const int resolution_xy, const int resolution_z, const float isoLevel)
{
  typename pcl::PointCloud<T>::Ptr vertices(new pcl::PointCloud<T>());
  std::vector<pcl::Vertices> faces;
  typename pcl::MarchingCubesRBF<T> mc;
  mc.setIsoLevel(isoLevel);
  mc.setGridResolution(resolution_xy, resolution_xy, resolution_z);
  mc.setInputCloud(cloud);
  // mc.setSearchMethod(get_search_kdtree<T>(cloud));
  mc.reconstruct(*vertices, faces);
  return std::pair<typename pcl::PointCloud<T>::Ptr, std::vector<pcl::Vertices> >(vertices, faces);
}

template<class T>
void color_resampled_cloud(typename pcl::PointCloud<T>::Ptr &cloud,
                           const typename pcl::PointCloud<T>::ConstPtr &reference,
                           const int size_neighborhood = 10)
{
  typename pcl::search::KdTree<T>::Ptr kdtree = get_search_kdtree<T>(reference);
  for(int id = 0; id < cloud->size(); ++id)
  {
    std::vector<int> indices;
    std::vector<float> distances;
    if(kdtree->nearestKSearch(cloud->at(id), size_neighborhood, indices, distances) > 0)
    {
      int r = 0, g = 0, b = 0;
      for(int i : indices)
      {
        r += (int) reference->at(i).r;
        g += (int) reference->at(i).g;
        b += (int) reference->at(i).b;
      }
      cloud->at(id).r = (uint8_t) (r / (int) indices.size());
      cloud->at(id).g = (uint8_t) (g / (int) indices.size());
      cloud->at(id).b = (uint8_t) (b / (int) indices.size());
    }
  }
}

template<class T>
typename pcl::PointCloud<T>::Ptr subsample_voxel_grid_inc(const typename pcl::PointCloud<T>::ConstPtr &cloud,
                                                          const double voxel_size)
{
  typename pcl::PointCloud<T>::Ptr subsampled(new pcl::PointCloud<T>());
  typename pcl::PointCloud<T>::Ptr tmp(new pcl::PointCloud<T>());
  const size_t size_batch = 100000;
  pcl::VoxelGrid<T> sor;
  for(size_t id = 0; id < cloud->size(); id += size_batch)
  {
    tmp->clear();
    tmp->reserve(size_batch);
    tmp->insert(tmp->end(), cloud->begin() + id, cloud->begin() + std::min(id + size_batch, cloud->size()));
    subsampled = concatenate_cloud<T>(subsampled, tmp);

    sor.setInputCloud(subsampled);
    sor.setLeafSize(voxel_size, voxel_size, voxel_size);
    sor.filter(*subsampled);

    std::cout << tmp->size() << " " << id << " " << subsampled->size() << " " << cloud->size() << std::endl;
  }
  return subsampled;
}

void back_project(const Eigen::Vector3f &xyz, const Eigen::Matrix4f &extrinsic_inv,
                  const Eigen::Matrix3f &intrinsic_inv, Eigen::Vector2i &uv, float &depth)
{
  Eigen::Vector4f xyz1;
  Eigen::Vector3f uv1;

  xyz1 << xyz, 1.0f;
  xyz1 = extrinsic_inv * xyz1;
  depth = xyz1(3);
  xyz1 /= xyz1(3);

  uv1 << xyz1(0), xyz1(1), 1,0f;
  uv1 = intrinsic_inv * uv1;

  uv << (int)uv1(0), (int)uv1(1);
}

#endif //CLOUD_PROC_PROC_HPP
