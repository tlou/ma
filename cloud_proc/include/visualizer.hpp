#ifndef CLOUD_PROC_VISUALIZER_HPP
#define CLOUD_PROC_VISUALIZER_HPP

#include <pcl/common/common_headers.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/PolygonMesh.h>

typedef pcl::visualization::PCLVisualizer Visualizer;

template<class T>
boost::shared_ptr<Visualizer> cloud_visualizer(const typename pcl::PointCloud<T>::ConstPtr &cloud)
{
  boost::shared_ptr<Visualizer> viewer(new Visualizer("3D Viewer"));
  viewer->setBackgroundColor(0, 0, 0);
  pcl::visualization::PointCloudColorHandlerRGBField<T> rgb(cloud);
  viewer->addPointCloud<T>(cloud, rgb, "sample cloud");
  viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
  viewer->addCoordinateSystem(1.0);
  viewer->initCameraParameters();
  return (viewer);
}

boost::shared_ptr<pcl::visualization::PCLVisualizer> normalsVis(
    pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud, pcl::PointCloud<pcl::Normal>::ConstPtr normals)
{
  // --------------------------------------------------------
  // -----Open 3D viewer and add point cloud and normals-----
  // --------------------------------------------------------
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("3D Viewer"));
  viewer->setBackgroundColor(0, 0, 0);
  pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(cloud);
  viewer->addPointCloud<pcl::PointXYZRGB>(cloud, rgb, "sample cloud");
  viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
  viewer->addPointCloudNormals<pcl::PointXYZRGB, pcl::Normal>(cloud, normals, 10, 0.05, "normals");
  viewer->addCoordinateSystem(1.0);
  viewer->initCameraParameters();
  return (viewer);
}

//boost::shared_ptr<Visualizer> cloud2_visualizer(const pcl::PCLPointCloud2::ConstPtr &cloud)
//{
//  boost::shared_ptr<Visualizer> viewer(new Visualizer("3D Viewer"));
//  viewer->setBackgroundColor(0, 0, 0);
//  pcl::visualization::PointCloudColorHandler<pcl::PCLPointCloud2> rgb(cloud);
//  viewer->addPointCloud(cloud, rgb, Eigen::Vector4f(0.0f, 0.0f, 0.0f, 0.0f),
//                        Eigen::Quaternionf(1.0f, 0.0f, 0.0f, 0.0f));
//  viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
//  viewer->addCoordinateSystem(1.0);
//  viewer->initCameraParameters();
//  return (viewer);
//}

boost::shared_ptr<Visualizer> mesh_visualizer(const pcl::PolygonMesh::ConstPtr &mesh)
{
  boost::shared_ptr<Visualizer> viewer(new Visualizer("3D Viewer"));
  viewer->setBackgroundColor(0, 0, 0);
  viewer->addPolygonMesh(*mesh);
  viewer->addCoordinateSystem(1.0);
  viewer->initCameraParameters();
  return (viewer);
}

#endif
