#include <iostream>
#include <vector>
#include <string>
#include <boost/filesystem.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/io/ply_io.h>
#include <pcl/conversions.h>

#include "visualizer.hpp"
#include "proc.hpp"
#include "io.hpp"

//typedef pcl::PointXYZRGB Point;
typedef pcl::PointXYZRGBNormal Point;
typedef pcl::PointCloud<Point> Cloud;

void inv_normal(Cloud::Ptr &cloud)
{
  for(size_t id = 0; id < cloud->size(); ++id)
  {
    cloud->at(id).normal_x = -cloud->at(id).normal_x;
    cloud->at(id).normal_y = -cloud->at(id).normal_y;
    cloud->at(id).normal_z = -cloud->at(id).normal_z;
  }
}

std::vector<std::string> load_all_files(const std::string &dir_name, const std::string &suffix)
{
  namespace fs = boost::filesystem;
  std::vector<std::string> result;
  fs::path path(dir_name);
  if(fs::exists(path) && fs::is_directory(path))
  {
    for(fs::directory_iterator iter(path); iter != fs::directory_iterator(); ++iter)
    {
      if(iter->path().extension().string() == suffix)
      {
        result.push_back(iter->path().string());
      }
    }
  }
  return result;
}

int main(int argc, char **argv)
{
//  std::cout << "started" << std::endl;
//  const std::string path("/home/tlou/workspace/navvis_data/transformed_clouds/");
//  const std::vector<std::string> filenames = load_all_files(path, ".pcd");
//  const double grid_size = 0.05;
//  Cloud::Ptr total_cloud(new Cloud());
//  for(std::string fn: filenames)
//  {
//    std::cout << fn << std::endl;
//    Cloud::Ptr sub_cloud = load_cloud_from_file<Point>(fn);
//    total_cloud = concatenate_cloud<Point>(sub_cloud, total_cloud);
//    total_cloud = subsample_voxel_grid_inc<Point>(total_cloud, grid_size);
//    std::cout << total_cloud->size() << std::endl;
//  }

//  Cloud::Ptr cloud0 = load_cloud_from_file<Point>(path + "2016-10-01_11.37.49_tum_Informatics_besprechung.pcd");
//  Cloud::Ptr cloud1 = load_cloud_from_file<Point>(path + "2016-10-01_12.06.41_tum_Informatics_03_balcony_02.pcd");

//  Cloud::Ptr sub = subsample_voxel_grid_inc<Point>(cloud1, 0.05);

//  std::cout << cloud1->size() << std::endl;
//  std::cout << sub->size() << std::endl;

//  std::cout << "loaded" << std::endl;

//  Eigen::Matrix4f tf(get_transform_icp<Point>(subsample_voxel_grid<Point>(cloud1, grid_size),
//                                              subsample_voxel_grid<Point>(cloud0, grid_size)));
//  std::cout << tf << std::endl;
//  pcl::transformPointCloud<Point>(*cloud1, *cloud1, tf);
//  total_cloud = concatenate_cloud<Point>(cloud0, total_cloud);
//  total_cloud = concatenate_cloud<Point>(cloud1, total_cloud);
//  total_cloud = subsample_voxel_grid<Point>(total_cloud, grid_size);
//  cloud0 = subsample_voxel_grid<Point>(cloud0, grid_size);

//  inv_normal(cloud0);
//  cloud0 = smooth_mls<Point>(cloud0, 0.05);
//
//  std::pair<Cloud::Ptr, std::vector<pcl::Vertices> > mesh = trianglulate_mesh_poisson<Point>(cloud0, 13);
//  std::cout << "size of cloud " << cloud0->size() << std::endl;
//  std::cout << "size of faces " << mesh.second.size() << std::endl;
//  std::cout << "size of vertices " << mesh.first->size() << std::endl;
//
//  color_resampled_cloud<Point>(mesh.first, cloud0, 10);

//  Visualizer::Ptr viewer = cloud_visualizer<Point>(cloud0);
//  while (!viewer->wasStopped())
//  {
//    viewer->spinOnce (100);
//    boost::this_thread::sleep(boost::posix_time::microseconds(100000));
//  }

//  write_ply<Point>(mesh.first, mesh.second, "/home/tlou/mesh.ply");

//  std::cout << "la fin" << std::endl;

//  Cloud::Ptr cloud_in(new Cloud());
  pcl::PCLPointCloud2::Ptr cloud_in(new pcl::PCLPointCloud2());
  Cloud::Ptr cloud(new Cloud());
  pcl::PLYReader reader;
  reader.read(std::string(argv[2]), *cloud_in);
  pcl::fromPCLPointCloud2(*cloud_in, *cloud);

  if(std::string(argv[1]) == "hoppe")
  {
    int resolution_xy = std::atoi(argv[4]);
    int resolution_z = resolution_xy * 3;
    float threshold = std::atof(argv[5]);

    std::pair<Cloud::Ptr, std::vector<pcl::Vertices> > mesh = trianglulate_mesh_mc_hoppe<Point>(cloud, resolution_xy,
                                                                                                resolution_z,
                                                                                                threshold);
    color_resampled_cloud<Point>(mesh.first, cloud, 5);
    write_ply<Point>(mesh.first, mesh.second, argv[3]);
  }
  else if(std::string(argv[1]) == "rbf")
  {
    int resolution_xy = std::atoi(argv[4]);
    int resolution_z = resolution_xy * 3;
    float threshold = std::atof(argv[5]);

    std::pair<Cloud::Ptr, std::vector<pcl::Vertices> > mesh = trianglulate_mesh_mc_rbf<Point>(cloud, resolution_xy,
                                                                                              resolution_z, threshold);
    color_resampled_cloud<Point>(mesh.first, cloud, 5);
    write_ply<Point>(mesh.first, mesh.second, argv[3]);
  }
  else if(std::string(argv[1]) == "vis")
  {
    Visualizer::Ptr viewer = cloud_visualizer<Point>(cloud);
    while(!viewer->wasStopped())
    {
      viewer->spinOnce(100);
      boost::this_thread::sleep(boost::posix_time::microseconds(100000));
    }
  }
  else if(std::string(argv[1]) == "norvis")
  {
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr pts(new pcl::PointCloud<pcl::PointXYZRGB>());
    pcl::PointCloud<pcl::Normal>::Ptr nors(new pcl::PointCloud<pcl::Normal>());
//    cloud = smooth_mls<Point>(cloud, 0.2);
    pcl::copyPointCloud(*cloud, *pts);
    pcl::copyPointCloud(*cloud, *nors);
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = normalsVis(pts, nors);
    while(!viewer->wasStopped())
    {
      viewer->spinOnce(100);
      boost::this_thread::sleep(boost::posix_time::microseconds(100000));
    }
  }

  return 0;
}
