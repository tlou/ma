#include <iostream>
#include <vector>
#include <utility>
#include <string>
#include <fstream>
#include <sstream>
#include <cmath>
#include <boost/filesystem.hpp>

#include <Eigen/Dense>
//#include <Eigen/Geometry>

#include <opencv2/core/core.hpp>
//#include <opencv2/highgui.hpp>

//#include "CommonVisualizer.hpp"

struct Point
{
  float x;
  float y;
  float z;
  float nx;
  float ny;
  float nz;
  float cur;
  unsigned char r;
  unsigned char g;
  unsigned char b;
};

union Bit32
{
  float f;
  char c[4];
  unsigned char u[4];
};

const float SIZE = 1.0f;
const float SIZE_WITH_BOUNDARY = SIZE * 1.1f;
const int batch_size = 100000;

std::vector<float> xyz_global;

Point centroid;

bool is_in_range(const Point &point0, const Point &point1)
{
  return fabs(point0.x - point1.x) < SIZE && fabs(point0.y - point1.y) < SIZE;
}

bool is_in_range_ext(const Point &point0, const Point &point1)
{
  return fabs(point0.x - point1.x) < SIZE_WITH_BOUNDARY && fabs(point0.y - point1.y) < SIZE_WITH_BOUNDARY;
}

void back_project(const Eigen::Vector3f &xyz, const Eigen::Matrix4f &extrinsic_inv,
                  const Eigen::Matrix3f &intrinsic, Eigen::Vector2i &uv, float &depth)
{
  static Eigen::Vector4f xyz1;
  static Eigen::Vector3f uv1;

  xyz1 << xyz, 1.0f;
  xyz1 = extrinsic_inv * xyz1;

  xyz_global.push_back(xyz1(0));
  xyz_global.push_back(xyz1(1));
  xyz_global.push_back(xyz1(2));

  depth = xyz1(2);
  xyz1 /= xyz1(2);

  uv1 = intrinsic * xyz1.segment(0, 3);

  uv << (int) uv1(0), (int) uv1(1);
}

Eigen::MatrixXf pinv(const Eigen::MatrixXf &mat)
{
  // pseudo inverse from svd
  // https://de.wikipedia.org/wiki/Singul%C3%A4rwertzerlegung
  Eigen::JacobiSVD<Eigen::MatrixXf> svd(mat, Eigen::ComputeThinU | Eigen::ComputeThinV);
  return svd.matrixV() * svd.singularValues().asDiagonal().inverse() * svd.matrixU().inverse();
}

//f:
//- 365.907898
//- 365.90789
//c:
//- 258.802887
//- 200.940903
Eigen::Matrix3f get_intrinsic(const float cx, const float cy, const float fx, const float fy)
{
  static Eigen::Matrix3f mat;
  mat << fx, 0.0f, cx, 0.0f, fy, cy, 0.0f, 0.0f, 1.0f;
  return mat;
}

/**
 *
 * @param transform x, y, z, w, x, y, z
 * @return
 */
Eigen::Matrix4f get_extrinsic(const std::vector<float> &transform)
{
  return (Eigen::Translation<float, 3>(transform.at(0), transform.at(1), transform.at(2))
          * Eigen::Quaternionf(transform.at(3), transform.at(4), transform.at(5), transform.at(6))).matrix();
}

cv::Mat get_depth_mat(const std::vector<Point> &points, const Eigen::Matrix4f &extrinsic_inv,
                      const Eigen::Matrix3f &intrinsic)
{
  cv::Mat mat(424, 512, CV_32FC1, NAN);
  Eigen::Vector2i uv;
  float depth;
  int count = 0;
  for(const Point &point: points)
  {
    back_project(Eigen::Vector3f(point.x, point.y, point.z), extrinsic_inv, intrinsic, uv, depth);
    if(uv(0) >= 0 && uv(1) >= 0 && uv(1) < 424 && uv(0) < 512)
    {
      // positive
      if(depth > 0.0f && (std::isnan(mat.at<float>(uv(1), uv(0))) || mat.at<float>(uv(1), uv(0)) < depth))
      {
        mat.at<float>(uv(1), uv(0)) = depth;
      }
      // negative
//      if(depth < 0.0f && (std::isnan(mat.at<float>(uv(1), uv(0))) || mat.at<float>(uv(1), uv(0)) < -depth))
//      {
//        mat.at<float>(uv(1), uv(0)) = -depth;
//      }
    }
    ++count;
    if(count % 10000 == 0)
      std::cout << count << " " << points.size() << "\n";
  }
  return mat;
}

void float_to_bytes(std::vector<unsigned char> &bytes, const int offset, const float value)
{
  Bit32 buffer;
  buffer.f = value;
  for(int i = 0; i < 4; ++i)
  {
    bytes.at(i + offset) = buffer.u[i];
  }
}

float bytes_to_float(const std::vector<unsigned char> &bytes, const int offset)
{
  Bit32 buffer;
  for(int i = 0; i < 4; ++i)
  {
    buffer.u[i] = bytes.at(offset + i);
  }
  return buffer.f;
}

void save_points_ply(const std::string &filename, const std::vector<Point> &points)
{
  std::ofstream out;
  out.open(filename.c_str());

  out << "ply" << std::endl;
  out << "format binary_little_endian 1.0" << std::endl;
  out << "element vertex " << points.size() << std::endl;
  out << "property float x" << std::endl;
  out << "property float y" << std::endl;
  out << "property float z" << std::endl;
  out << "property uchar red" << std::endl;
  out << "property uchar green" << std::endl;
  out << "property uchar blue" << std::endl;
  out << "property float nx" << std::endl;
  out << "property float ny" << std::endl;
  out << "property float nz" << std::endl;
  out << "property float curvature" << std::endl;
  out << "end_header" << std::endl;

  std::vector<unsigned char> buffer(31 * batch_size, 0);
  for(int id = 0; id < (int) points.size(); id += batch_size)
  {
    int num_left = std::min((int) points.size() - id, batch_size);
    for(int i = 0; i < num_left; ++i)
    {
      const Point &point = points.at(id + i);
      const int offset = 31 * i;

      float_to_bytes(buffer, 0 + offset, point.x);
      float_to_bytes(buffer, 4 + offset, point.y);
      float_to_bytes(buffer, 8 + offset, point.z);
      float_to_bytes(buffer, 15 + offset, point.nx);
      float_to_bytes(buffer, 19 + offset, point.ny);
      float_to_bytes(buffer, 23 + offset, point.nz);
      float_to_bytes(buffer, 27 + offset, point.cur);
      buffer.at(12 + offset) = point.r;
      buffer.at(13 + offset) = point.g;
      buffer.at(14 + offset) = point.b;
    }
    out.write((char *) &buffer.front(), 31 * num_left);
  }

  out.close();
}

std::vector<Point> load_points_ply(const std::string &filename)
{
  std::vector<Point> points;
  if(boost::filesystem::is_regular_file(boost::filesystem::path(filename)))
  {
    std::vector<unsigned char> buffer(31 * batch_size, 0);
    std::ifstream in;
    int length = 0;
    in.open(filename.c_str());

    for(int i = 0; i < 500; ++i)
    {
      std::string str;
      in >> str;
      if(str == "end_header")
      {
        break;
      }
      else if(str == "vertex")
      {
        in >> length;
      }
    }
    in.read((char *) &buffer.front(), 1);

    points.reserve(batch_size * 10);
//    float xmax = -1e6f, xmin = 1e6f;
//    float ymax = -1e6f, ymin = 1e6f;
//    float zmax = -1e6f, zmin = 1e6f;
    for(int iter = 0; iter < length; iter += batch_size)
    {
      int num_left = std::min(length - iter, batch_size);
      in.read((char *) &buffer.front(), 31 * num_left);
      for(int id = 0; id < num_left; ++id)
      {
        Point point;
        const int offset = 31 * id;
        point.x = bytes_to_float(buffer, 0 + offset);
        point.y = bytes_to_float(buffer, 4 + offset);

//        if(!is_in_range_ext(point, centroid))
//        {
//          continue;
//        }

        point.z = bytes_to_float(buffer, 8 + offset);
        point.r = buffer.at(12 + offset);
        point.g = buffer.at(13 + offset);
        point.b = buffer.at(14 + offset);
        point.nx = bytes_to_float(buffer, 15 + offset);
        point.ny = bytes_to_float(buffer, 19 + offset);
        point.nz = bytes_to_float(buffer, 23 + offset);
        point.cur = bytes_to_float(buffer, 27 + offset);

        points.push_back(point);
      }
    }

    in.close();
  }
  else
  {
    std::cout << "file not found\n";
  }
  return points;
}

void save_buffer(const std::string &filename, const std::vector<unsigned char> &buffer)
{
  std::ofstream out;
  out.open(filename.c_str());
  out.write((char*)&buffer.front(), buffer.size());
  out.close();
}

void depth_map_to_buffer(const cv::Mat &depth_map, std::vector<unsigned char> &buffer)
{
  buffer.resize(424 * 512 * 4);
  int offset = 0;
  for(int r = 0; r < 424; ++r)
  {
    for(int c = 0; c < 512; ++c)
    {
      float_to_bytes(buffer, offset, depth_map.at<float>(r, c));
      offset += 4;
    }
  }
}

int main(int argc, char **argv)
{
//  load_points_ply("/home/tlou/workspace/navvis_data/pointcloud-merged.ply");

//  centroid.x = std::stof(std::string(argv[2]));
//  centroid.y = std::stof(std::string(argv[3]));
  const std::string home("/home/lou");

  std::stringstream ss;
  ss << home << "/workspace/navvis_data/subs/" << centroid.x << "_" << centroid.y << ".ply";
  std::string path = home + std::string("/workspace/navvis_data/transformed_clouds/")
                     + "2016-10-01_10.50.25_tum_Informatics_Lst.Knoll.02_01.ply";
  std::vector<Point> points = load_points_ply(path);
  std::cout << "loaded\n";

  int iter = 0;
  float tf_base_data[] = {53.34161376953125f, -22.558853149414062f, 0.024128861725330353f,
                          0.9999770072439446f, 0.0f, 0.0f, 0.006780665277493092f};
  float tf_data[] = {-1.81958f, -0.181076f, -0.262887f,
                     0.428998f, 0.563741f, 0.430364f, 0.559413f};
  Eigen::Matrix4f extrinsic = get_extrinsic(std::vector<float>(tf_base_data, tf_base_data + 7))
                              * get_extrinsic(std::vector<float>(tf_data, tf_data + 7));
  Eigen::Matrix3f intrinsic = get_intrinsic(258.802887f, 200.940903f, 365.907898f, 365.90789f);
  Eigen::Matrix4f extrinsic_inv = pinv(extrinsic);
  Eigen::Matrix3f intrinsic_inv = pinv(intrinsic);

  extrinsic_inv = pinv(get_extrinsic(std::vector<float>(tf_data, tf_data + 7)))
                  * pinv(get_extrinsic(std::vector<float>(tf_base_data, tf_base_data + 7)));
  
  cv::Mat depth_map = get_depth_mat(points, extrinsic_inv, intrinsic);
  std::vector<unsigned char> buffer;
  depth_map_to_buffer(depth_map, buffer);
  save_buffer(home + "/workspace/example.raw", buffer);


//  cv::imshow("depth_map", depth_map / 30.0f);
//  cv::waitKey(0);
//  cv::waitKey(1);

//  loco::CommonVisualizer vis;
//  vis.addPointCloud(xyz_global, loco::color::GREEN);
//  vis.addCoordinateSign(loco::Transform{loco::Vec{0, 0, 0}, loco::Vec{0, 0, 0, 1}});
//  vis.setTheta((float)M_PI);
//
//  vis.play();

//  if(!points.empty())
//  {
//    save_points_ply(ss.str(), points);
//  }

  return 0;
}
