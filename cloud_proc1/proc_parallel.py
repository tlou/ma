#! /bin/python2

import os
import time
import threading

num_thread = 3
list_thread = []
task_pool = []

for x in range(-25, 81):
    for y in range(-117, 85):
        task_pool.append((x, y))

def convert_file(pos):
    global num_thread
    while num_thread < 1:
        time.sleep(0.1)
    num_thread -= 1
    print pos
    os.system("./build/main {} {}".format(pos[0], pos[1]))
    num_thread += 1

for task in task_pool:
    thread = threading.Thread(target = convert_file, args = (task, ))
    thread.start()

for thread in list_thread:
    thread.join()
