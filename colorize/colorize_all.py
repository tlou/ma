import sys
import os

DIR_MESH = sys.argv[1]
DIR_CLOUD = sys.argv[2]
SIZE = int(sys.argv[3])
ID_SIZE = int(sys.argv[4])
KEY = ""
if len(sys.argv) > 5:
    KEY = sys.argv[5]

if not os.path.isdir(DIR_MESH):
    print "dir not found"
    sys.exit()

def filter_dir(list_dir, id_key):
    ls = []
    for it_dir in list_dir:
        its = it_dir.split("_")
        if id_key < len(its) and int(its[id_key]) == SIZE:
            ls.append(it_dir)
    return ls

list_dir = os.listdir(DIR_MESH)
if len(KEY) > 0:
    list_dir = [d for d in list_dir if KEY in d]
list_dir = filter_dir(list_dir, ID_SIZE)

for it_dir in list_dir:
    dir_full = DIR_MESH + "/" + it_dir + "/"
    command = "python2 colorize_dir.py " + DIR_CLOUD + " " + dir_full \
            + " " + dir_full[:-1] + "_color"
    print command
    os.system(command)
