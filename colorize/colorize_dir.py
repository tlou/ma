#! /bin/python2

import sys
import os
import subprocess
import time

DIR_IN_CLOUD = sys.argv[1]
DIR_IN_MESH = sys.argv[2]
DIR_OUT = sys.argv[3]

HOME = os.getenv('HOME')
EXE = HOME+ "/workspace/ma/colorize/build/colorize"

os.system("rm -rf " + DIR_OUT)
os.system("mkdir " + DIR_OUT)

task_pool = os.listdir(DIR_IN_CLOUD)

time_start = time.time()

for task in task_pool:
    if len(task) > 4 and ".ply" in task and os.path.exists(DIR_IN_MESH+"/"+task):
        fn = "/" + task
        os.system(EXE + " " + DIR_IN_CLOUD + fn + " " + DIR_IN_MESH + fn \
                + " " + DIR_OUT + fn)

time_end = time.time()
with open(DIR_OUT + "/log", 'w') as out:
    out.write("running time is {}, from {} to {}".format(\
            time_end - time_start, time_start, time_end))
