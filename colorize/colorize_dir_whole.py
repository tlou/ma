import os
import sys

PATH_CLOUD = sys.argv[1]
DIR_IN = sys.argv[2]
DIR_OUT = sys.argv[3]

EXE = "./build/colorize"

assert os.path.isdir(DIR_IN)
assert os.path.isfile(PATH_CLOUD)
assert os.path.isfile(EXE)

targets = tuple([fn for fn in os.listdir(DIR_IN) if ".ply" in fn])
if not os.path.isdir(DIR_OUT):
    os.mkdir(DIR_OUT)

for target in targets:
    fn_in = DIR_IN + "/" + target
    fn_out = DIR_OUT + "/" + target

    command = EXE + " " + PATH_CLOUD + " " + fn_in + " " + fn_out
    print command
    os.system(command)

