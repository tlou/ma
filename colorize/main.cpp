#include <iostream>
#include <string>
#include <thread>
#include <chrono>
#include <mutex>
#include <vector>
#include <cstdlib>
#include <sstream>
#include <fstream>

#include <Eigen/Dense>

#include <pcl/common/common_headers.h>
#include <pcl/filters/passthrough.h>
#include <pcl/io/ply_io.h>
#include <pcl/surface/poisson.h>

#include "Timer.hpp"

typedef pcl::PointXYZRGB Point;
typedef pcl::PointCloud<Point> Cloud;

typedef pcl::PointXYZ Point0;
typedef pcl::PointCloud<Point0> Cloud0;

/**
 * build kdtree for searching
 * @param cloud
 * @return
 */
template<class T>
typename pcl::search::KdTree<T>::Ptr
get_search_kdtree (const typename pcl::PointCloud<T>::ConstPtr &cloud)
{
  typename pcl::search::KdTree<T>::Ptr kdtree (new pcl::search::KdTree<T>);
  kdtree->setInputCloud (cloud);
  return kdtree;
}

/**
 * load ply
 * @param filename
 * @return
 */
Cloud::Ptr
load_ply (const std::string &filename)
{
  pcl::PCLPointCloud2::Ptr cloud_in (new pcl::PCLPointCloud2 ());
  Cloud::Ptr cloud (new Cloud ());
  pcl::PLYReader reader;
  reader.read (filename, *cloud_in);
  pcl::fromPCLPointCloud2<Point> (*cloud_in, *cloud);
  return cloud;
}

/**
 * load mesh, in earlier PCL the IO module was not friendly
 * @param filename
 * @return
 */
pcl::PolygonMesh::Ptr
load_mesh (const std::string &filename)
{
  pcl::PolygonMesh::Ptr result (new pcl::PolygonMesh ());
  pcl::io::loadPLYFile (filename, *result);
  return result;
}

/**
 * paint the mesh with point cloud
 * @param cloud_color
 * @param mesh
 */
void
colorize (const Cloud::ConstPtr &cloud_color,
          pcl::PolygonMesh::Ptr &mesh)
{
  Cloud0 cloud_plain;
  pcl::fromPCLPointCloud2 (mesh->cloud, cloud_plain);
  pcl::search::KdTree<Point>::Ptr kdtree = get_search_kdtree<Point> (cloud_color);

  Cloud mesh_cloud_color;
  mesh_cloud_color.reserve (cloud_plain.size ());

  std::vector<int> k_indices (1, 0);
  std::vector<float> k_sqr_distances (1, 0.0f);
  for (const Point0 &pt : cloud_plain)
  {
    Point pt_color;
    pt_color.x = pt.x;
    pt_color.y = pt.y;
    pt_color.z = pt.z;

    kdtree->nearestKSearch (pt_color, 1, k_indices, k_sqr_distances);

    pt_color.rgb = cloud_color->at ((size_t) k_indices[0]).rgb;

    mesh_cloud_color.push_back (pt_color);
  }

  mesh->cloud = pcl::PCLPointCloud2 ();

  pcl::toPCLPointCloud2 (mesh_cloud_color, mesh->cloud);
}

void
print_help ()
{
  std::cout << "./colorize path_input_cloud_ply path_input_mesh_ply path_output_mesh_ply"
            << std::endl;
}

int
main (int argc, char **argv)
{
  if (argc <= 1)
  {
    print_help ();
    return 1;
  }
  
  const std::string fn_cloud (argv[1]);
  const std::string fn_mesh (argv[2]);
  const std::string fn_out (argv[3]);

  pcl::PolygonMesh::Ptr mesh = load_mesh (fn_mesh);
  Cloud::Ptr cloud_in = load_ply (fn_cloud);
  Timer timer;
  colorize (cloud_in, mesh);

  std::ofstream os;
  os.open (fn_out + "_log");
  os << "running time is " << timer.getSecFromStart () << ", from " << timer.getStart ()
     << " to " << timer.getEnd () << std::endl;
  os.close ();

  pcl::io::savePLYFileBinary (fn_out, *mesh);

  return 0;
}
