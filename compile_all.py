import os
import threading

list_dir = tuple([d for d in os.listdir(".") if os.path.isfile(d + "/CMakeLists.txt")])
list_thread = []
local = os.path.dirname(os.path.realpath(__file__))

for d in list_dir:
    path_build = d + "/build"
    if not os.path.isdir(path_build):
        os.mkdir(path_build)
    os.chdir(path_build)
    os.system("cmake ..")
    os.system("make -j4")
    os.chdir(local)
