#include <iostream>
#include <vector>

#include <Eigen/Dense>
#include <Eigen/StdVector>

#include <pcl/point_types.h>
#include <pcl/common/common_headers.h>
#include <pcl/io/ply_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/common/centroid.h>
#include <pcl/common/transforms.h>
#include <pcl/surface/concave_hull.h>
#include <pcl/surface/ear_clipping.h>
#include <pcl/features/normal_3d.h>
#include <pcl/segmentation/region_growing.h>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

typedef pcl::PointXYZRGBNormal Point;
typedef pcl::PointCloud<Point> Cloud;

/**
 * backproject the point cloud to depth and normal map
 * @param cloud
 * @param width
 * @param height
 * @param dp distance between pixels on image plane
 * @param depth_map
 * @param normal_map
 */
void
backproject (const Cloud::ConstPtr &cloud,
             const int width,
             const int height,
             const float dp,
             cv::Mat &depth_map,
             cv::Mat &normal_map)
{
  depth_map = cv::Mat (height, width, CV_32FC1, cv::Scalar (0.0f));
  normal_map = cv::Mat (height, width, CV_32FC3, cv::Scalar (0.0f, 0.0f, 0.0f));

  for (const Point &point: *cloud)
  {
    if (point.y > 0.0f)
    {
      const float depth = point.y;
      const float xp = point.x / depth;
      const float yp = -point.z / depth;
      const float uf = xp / dp;
      const float vf = yp / dp;
      const int u = (int) std::round (uf + 0.5f) + width / 2;
      const int v = (int) std::round (vf + 0.5f) + height / 2;

      if (u >= 0 && u < width && v > 0 && v < height && depth > depth_map.at<float> (v, u))
      {
        // point on map
        const Eigen::Vector3f normal = point.getNormalVector3fMap ();
        depth_map.at<float> (v, u) = depth;
        normal_map.at<cv::Vec3f> (v, u) = cv::Vec3f (normal (0), normal (1), normal (2));
      }
    }
  }
}

int
main (int argn, char **argv)
{
  // example pose
  // 2016-10-01_11.14.58
  // pos 13, cam0
  const Eigen::Translation3f translation_device (3.3675051087f, -6.0660379129f, -0.0000000000f);
  const Eigen::Quaternionf rotation_device (0.6652562658f, -0.0024182320f,
                                            0.0004823331f, -0.7466110234f);
  const Eigen::Translation3f translation_camera (0.155814959709613f, -3.77517106666813e-05f, 1.83259514312722f);
  const Eigen::Quaternionf rotation_camera (0.9998776236121352f, -0.008212617322887599f,
                                            0.009618460099176789f, 0.00920738517591752f);
  Cloud::Ptr cloud_in (new Cloud ());
  pcl::io::loadPLYFile ("/home/tlou/workspace/navvis_data/room1/pointcloud.ply", *cloud_in);

  Cloud::Ptr cloud_tf (new Cloud ());

  // transform the point cloud to camera space
  pcl::transformPointCloud<Point> (*cloud_in, *cloud_tf,
                                   (translation_camera * rotation_camera * translation_device *
                                    rotation_device).inverse ().matrix ());

  cv::Mat depth_map, normal_map;
  backproject (cloud_tf, 1280, 960, 0.0025f, depth_map, normal_map);

  // save cloud for debugging
  pcl::io::savePLYFileBinary ("/tmp/cloud_tf.ply", *cloud_tf);

  // save both maps for visualization
  cv::imwrite ("/tmp/depth.png", (depth_map / 4.0f) * 255.0f);
  cv::imwrite ("/tmp/normal.png", (normal_map + 1.0f) * 255.0f / 2.0f);


  return 0;
}
