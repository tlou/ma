#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <sstream>
#include <cmath>

#include <Eigen/Dense>

#include <pcl/point_types.h>
#include <pcl/io/ply_io.h>
#include <pcl/common/common_headers.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/surface/mls.h>

typedef pcl::PointXYZ PointP;
typedef pcl::PointCloud<PointP> CloudP;

typedef pcl::PointXYZI PointI;
typedef pcl::PointCloud<PointI> CloudI;

typedef pcl::PointNormal Point;
typedef pcl::PointCloud<Point> Cloud;

/**
 * estimate the normal in case not available
 * @param cloud_in
 * @param radius_search
 * @return
 */
Cloud::Ptr
estimate_normal (const CloudP::ConstPtr &cloud_in,
                 const double radius_search)
{
  Cloud::Ptr re (new Cloud ());
  pcl::search::KdTree<PointP>::Ptr tree (new pcl::search::KdTree<PointP> ());
  pcl::MovingLeastSquares<PointP, Point> mls;

  mls.setInputCloud (cloud_in);
  mls.setComputeNormals (true);
  mls.setPolynomialFit (true);
  mls.setSearchMethod (tree);
  mls.setSearchRadius (radius_search);
  mls.process (*re);

  return re;
}

/**
 *
 * @param filename
 * @return
 */
Cloud::Ptr
load_cloud (const std::string &filename)
{
  pcl::PCLPointCloud2::Ptr cloud_in (new pcl::PCLPointCloud2 ());
  Cloud::Ptr re (new Cloud ());
  pcl::PLYReader reader;
  reader.read (filename, *cloud_in);

  bool is_normal_included = false;
  for (const pcl::PCLPointField &field: cloud_in->fields)
  {
    if (field.name.find ("ormal") != std::string::npos
        || field.name.find ("nx") != std::string::npos
        || field.name.find ("ny") != std::string::npos
        || field.name.find ("nz") != std::string::npos)
    {
      is_normal_included = true;
      break;
    }
  }

  if (!is_normal_included)
  {
    CloudP::Ptr cloud_pos (new CloudP ());
    pcl::fromPCLPointCloud2 (*cloud_in, *cloud_pos);
    re = estimate_normal (cloud_pos, 0.1);
  }
  else
  {
    pcl::fromPCLPointCloud2 (*cloud_in, *re);
  }

  return re;
}

/**
 *
 * @param filename
 * @param vertices
 * @param triangles
 */
void
load_mesh (const std::string &filename,
           Cloud::Ptr &vertices,
           std::vector<pcl::Vertices> &triangles)
{
  pcl::PolygonMesh::Ptr mesh (new pcl::PolygonMesh ());
  pcl::io::loadPLYFile (filename, *mesh);
  pcl::fromPCLPointCloud2 (mesh->cloud, *vertices);
  triangles = mesh->polygons;
}

/**
 * get the distance from point to line
 * @param point point
 * @param start start of line
 * @param end end of line
 * @return
 */
float
get_distance_point_lineperiod (const Eigen::Vector3f &point,
                               const Eigen::Vector3f &start,
                               const Eigen::Vector3f &end)
{
  const float length = (start - end).norm ();
  const Eigen::Vector3f dir = (end - start).normalized ();
  const float d = -(start - point).dot (dir);

  return (d >= 0.0f && d <= length) ?
         (point - (start + d * dir)).norm () :
         std::min ((point - start).norm (), (point - end).norm ());
}

/**
 * check whether a point is in triangle with barycentric coordinate
 * @param v0 vertex on triangle
 * @param v1 vertex on triangle
 * @param v2 vertex on triangle
 * @param point
 * @return
 */
bool
is_point_in_triangle (const Eigen::Vector3f &v0,
                      const Eigen::Vector3f &v1,
                      const Eigen::Vector3f &v2,
                      const Eigen::Vector3f &point)
{
  auto func_get_area = [] (const Eigen::Vector3f &pos0,
                           const Eigen::Vector3f &pos1,
                           const Eigen::Vector3f &pos2)->float
  {
    const float a = (pos0 - pos1).norm ();
    const float b = (pos1 - pos2).norm ();
    const float c = (pos2 - pos0).norm ();
    const float s = (a + b + c) / 2.0f;
    return std::sqrt (std::abs (s * (s - a) * (s - b) * (s - c)));
  };

  const float u = func_get_area (v0, v1, point);
  const float v = func_get_area (v1, v2, point);
  return (v > 0.0f) && (u > 0.0f) && (v + u < 1.0f);
}

/**
 *
 * @param v0 vertex of triangle
 * @param v1
 * @param v2
 * @param point
 * @return
 */
float
get_distance_point_triangle (const Eigen::Vector3f &v0,
                             const Eigen::Vector3f &v1,
                             const Eigen::Vector3f &v2,
                             const Eigen::Vector3f &point)
{
  float re = 0.0f;

  const Eigen::Vector3f normal = (v0 - v1).cross (v2 - v1).normalized ();
  const float w = -normal.dot (v1);

  const float d = -w - normal.dot (point);
  const Eigen::Vector3f point0 = point + d * normal;

  if (is_point_in_triangle (v0, v1, v2, point0))
  {
    re = std::abs (d);
  }
  else
  {
    float dist_edges[3];
    dist_edges[0] = get_distance_point_lineperiod (point, v0, v1);
    dist_edges[1] = get_distance_point_lineperiod (point, v1, v2);
    dist_edges[2] = get_distance_point_lineperiod (point, v2, v0);
    re = *std::min_element (dist_edges, dist_edges + 3);
  }

  return re;
}

/**
 * get angle if normal vectors are valid
 * @param normal0
 * @param normal1
 * @return
 */
float
get_angle (const Eigen::Vector3f &normal0, const Eigen::Vector3f &normal1)
{
  return (normal0.norm () > 0.95f && normal1.norm () > 0.95f) ?
         std::acos (std::abs (normal0.dot (normal1))) : (float) M_PI / 2.0f;
}

/**
 *
 * @param vertices
 * @param triangles
 * @param center_cloud
 * @param cloud
 * @param search_radius
 * @param cloud_out
 * @param cloud_out_angular
 */
void
get_distance_cloud_to_mesh (const Cloud::ConstPtr &vertices,
                            const std::vector<pcl::Vertices> &triangles,
                            const Cloud::ConstPtr &center_cloud,
                            const Cloud::ConstPtr &cloud,
                            const float search_radius,
                            CloudI::Ptr &cloud_out,
                            CloudI::Ptr &cloud_out_angular)
{
  cloud_out = CloudI::Ptr (new CloudI);
  cloud_out->reserve (cloud->size ());
  cloud_out_angular = CloudI::Ptr (new CloudI);
  cloud_out_angular->reserve (cloud->size ());

  pcl::KdTreeFLANN<Point> kdtree;
  kdtree.setInputCloud (center_cloud);

  size_t count_obsolete = 0;
  // compute for each point in cloud
  for (size_t id_point = 0; id_point < cloud->size (); ++id_point)
  {
    const Point &point = cloud->at (id_point);

    if (std::isnan (point.x) || std::isnan (point.y) || std::isnan (point.z))
    {
      continue;
    }

    const Eigen::Vector3f position = point.getVector3fMap ();
    std::vector<float> dists;
    std::vector<int> index_neighbors;
    std::vector<float> sqr_dists;

    if (kdtree.radiusSearch (point, search_radius, index_neighbors, sqr_dists) > 0)
    {
      dists.reserve (index_neighbors.size ());
      for (const int id: index_neighbors)
      {
        const std::vector<uint32_t> &indice = triangles.at (id).vertices;
        dists.push_back (get_distance_point_triangle (vertices->at (indice.at (0)).getVector3fMap (),
                                                      vertices->at (indice.at (1)).getVector3fMap (),
                                                      vertices->at (indice.at (2)).getVector3fMap (),
                                                      position));
      }
    }

    const long index_nearest = dists.empty () ? 0 : std::distance (dists.begin (),
                                                                   std::min_element (dists.begin (),
                                                                                     dists.end ()));
    PointI point_i;
    point_i.getVector3fMap () = position;
    point_i.intensity = dists.empty () ? search_radius : dists.at (index_nearest);
    cloud_out->push_back (point_i);

    point_i.intensity = dists.empty () ? (float) M_PI / 2.0f
                                       : get_angle (center_cloud->at (index_neighbors.at (index_nearest))
                                                      .getNormalVector3fMap (),
                                                    point.getNormalVector3fMap ());
    cloud_out_angular->push_back (point_i);

    if (id_point % 10000 == 0)
    {
      std::cout << id_point << " " << cloud->size () << std::endl;
    }
  }

  std::cout << count_obsolete << " points out of " << cloud->size () << " have no neighour" << std::endl;
}

/**
 *
 * @param vertices
 * @param triangles
 * @param center_cloud
 * @param cloud
 * @param search_radius
 * @param cloud_out
 * @param cloud_out_angular
 */
void
get_distance_mesh_to_cloud (const Cloud::ConstPtr &vertices,
                            const std::vector<pcl::Vertices> &triangles,
                            const Cloud::ConstPtr &center_cloud,
                            const Cloud::ConstPtr &cloud,
                            const float search_radius,
                            CloudI::Ptr &cloud_out,
                            CloudI::Ptr &cloud_out_angular)
{
  cloud_out = CloudI::Ptr (new CloudI);
  cloud_out->reserve (triangles.size ());
  cloud_out_angular = CloudI::Ptr (new CloudI);
  cloud_out_angular->reserve (triangles.size ());


  pcl::KdTreeFLANN<Point> kdtree;
  kdtree.setInputCloud (cloud);

  size_t count_obsolete = 0;
  // compute for each triangle in mesh
  for (size_t id_vertice = 0; id_vertice < triangles.size (); ++id_vertice)
  {
    const std::vector<uint32_t> &indice = triangles.at (id_vertice).vertices;
    const Eigen::Vector3f pos0 = vertices->at (indice.at (0)).getVector3fMap ();
    const Eigen::Vector3f pos1 = vertices->at (indice.at (1)).getVector3fMap ();
    const Eigen::Vector3f pos2 = vertices->at (indice.at (2)).getVector3fMap ();

    if (std::isnan (pos0 (0)) || std::isnan (pos0 (1)) || std::isnan (pos0 (2))
        || std::isnan (pos1 (0)) || std::isnan (pos1 (1)) || std::isnan (pos1 (2))
        || std::isnan (pos2 (0)) || std::isnan (pos2 (1)) || std::isnan (pos2 (2)))
    {
      continue;
    }

    Point point;
    std::vector<float> dists;
    std::vector<int> index_neighbors;
    std::vector<float> sqr_dists;

    point.getVector3fMap () = center_cloud->at (id_vertice).getVector3fMap ();
    point.getNormalVector3fMap () = center_cloud->at (id_vertice).getNormalVector3fMap ();
    if (kdtree.radiusSearch (point, search_radius, index_neighbors, sqr_dists) > 0)
    {
      dists.reserve (index_neighbors.size ());
      for (const int id: index_neighbors)
      {
        dists.push_back (get_distance_point_triangle (pos0, pos1, pos2,
                                                      cloud->at (id).getVector3fMap ()));
      }
    }

    const long index_nearest = dists.empty () ? 0 : std::distance (dists.begin (),
                                                                   std::min_element (dists.begin (),
                                                                                     dists.end ()));
    PointI point_i;
    point_i.getVector3fMap () = center_cloud->at (id_vertice).getVector3fMap ();
    point_i.intensity = dists.empty () ? search_radius : dists.at (index_nearest);
    cloud_out->push_back (point_i);

    point_i.intensity = dists.empty () ? (float) M_PI / 2.0f
                                       : get_angle (cloud->at (index_neighbors.at (index_nearest))
                                                      .getNormalVector3fMap (),
                                                    point.getNormalVector3fMap ());
    cloud_out_angular->push_back (point_i);

    if (id_vertice % 10000 == 0)
    {
      std::cout << id_vertice << " " << triangles.size () << std::endl;
    }
  }

  std::cout << count_obsolete << " triangles out of " << triangles.size () << " have no neighour" << std::endl;
}

/**
 * get the centroids of triangles as point cloud with normal directions
 * @param vertices
 * @param triangles
 * @return
 */
Cloud::Ptr
get_centre_mesh (const Cloud::ConstPtr &vertices,
                 const std::vector<pcl::Vertices> &triangles)
{
  Cloud::Ptr centers (new Cloud);
  centers->reserve (triangles.size ());

  for (const pcl::Vertices &vertice: triangles)
  {
    const std::vector<uint32_t> &indice = vertice.vertices;
    Point point;
    const Eigen::Vector3f vec0 = vertices->at (indice[0]).getVector3fMap ()
                                 - vertices->at (indice[1]).getVector3fMap ();
    const Eigen::Vector3f vec1 = vertices->at (indice[2]).getVector3fMap ()
                                 - vertices->at (indice[1]).getVector3fMap ();
    point.getVector3fMap () = (vertices->at (indice[0]).getVector3fMap ()
                               + vertices->at (indice[1]).getVector3fMap ()
                               + vertices->at (indice[2]).getVector3fMap ()) / 3.0f;
    point.getNormalVector3fMap () = vec0.cross (vec1).normalized ();
    centers->push_back (point);
  }

  return centers;
}

void
print_help ()
{
  std::cout << "./disp_diff path_input_mesh_ply path_input_cloud_ply prefix_output searching_radius"
            << std::endl;
}

int
main (int argc, char **argv)
{
  if (argc <= 1)
  {
    print_help ();
    return 1;
  }
  const std::string fn_mesh (argv[1]);
  const std::string fn_cloud (argv[2]);
  const std::string fn_out_pre (argv[3]);
  const float search_radius = std::stof (argv[4]);

  Cloud::Ptr cloud_in = load_cloud (fn_cloud);
  Cloud::Ptr vertices_in (new Cloud);
  std::vector<pcl::Vertices> triangles_in;
  load_mesh (fn_mesh, vertices_in, triangles_in);

  Cloud::Ptr centers = get_centre_mesh (vertices_in, triangles_in);

  CloudI::Ptr score_to_pc;
  CloudI::Ptr score_from_pc;

  CloudI::Ptr score_angular_to_pc;
  CloudI::Ptr score_angular_from_pc;

  std::cout << "number of points: " << cloud_in->size () << std::endl;
  std::cout << "number of triangles: " << triangles_in.size () << std::endl;
  std::cout << "number of vertices: " << vertices_in->size () << std::endl;
  std::cout << "mesh to cloud" << std::endl;
  std::thread thread_to (get_distance_mesh_to_cloud, vertices_in, triangles_in,
                         centers, cloud_in, search_radius,
                         std::ref (score_to_pc), std::ref (score_angular_to_pc));

  std::cout << "cloud to mesh" << std::endl;
  std::thread thread_from (get_distance_cloud_to_mesh, vertices_in, triangles_in,
                           centers, cloud_in, search_radius,
                           std::ref (score_from_pc), std::ref (score_angular_from_pc));

  thread_to.join ();
  thread_from.join ();
  std::cout << "finished" << std::endl;

  pcl::io::savePLYFileBinary (fn_out_pre + "_d_to.ply", *score_to_pc);
  pcl::io::savePLYFileBinary (fn_out_pre + "_d_from.ply", *score_from_pc);
  pcl::io::savePLYFileBinary (fn_out_pre + "_a_to.ply", *score_angular_to_pc);
  pcl::io::savePLYFileBinary (fn_out_pre + "_a_from.ply", *score_angular_from_pc);

  return 0;
}
