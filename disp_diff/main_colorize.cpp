#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <sstream>

#include <Eigen/Dense>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <pcl/point_types.h>
#include <pcl/io/ply_io.h>

typedef pcl::PointXYZI PointI;
typedef pcl::PointCloud<PointI> CloudI;

typedef pcl::PointXYZRGBA PointC;
typedef pcl::PointCloud<PointC> CloudC;

/**
 * encode intensity to colour from green to red
 * @param cloud
 * @param upper_bound
 * @param lower_bound
 * @return
 */
CloudC::Ptr
get_colored_cloud (const CloudI::ConstPtr &cloud,
                   const float upper_bound,
                   const float lower_bound)
{
  auto func_saturate = [upper_bound, lower_bound] (const float value)->float
  {
    return std::min (std::max ((value - lower_bound) / (upper_bound - lower_bound),
                               0.0f),
                     1.0f);
  };

  auto func_convert = [] (const float value,
                          const Eigen::Vector3f &position)->PointC
  {
    cv::Mat color (1, 1, CV_8UC3, cv::Scalar ((unsigned char) ((1.0f - value) * 45.0f), 255, 255));
    cv::cvtColor (color, color, CV_HSV2RGB);
    const cv::Vec3b &pixel = color.at<cv::Vec3b> (0, 0);

    PointC point_c;
    point_c.getVector3fMap () = position;
    // pixel = (b, g, r)
    point_c.rgba = ((uint32_t) pixel (0)) << 16 | ((uint32_t) pixel (1)) << 8 | ((uint32_t) pixel (2));

    return point_c;
  };

  CloudC::Ptr re (new CloudC);

  re->reserve (cloud->size ());
  for (const PointI &point_i: *cloud)
  {
    re->push_back (func_convert (func_saturate (point_i.intensity),
                                 point_i.getVector3fMap ()));
  }

  return re;
}

void
print_help ()
{
  std::cout << "./colorize prefix_input prefix_output upperbound_distance loweround_distance "
            << "upperbound_angle loweround_angle" << std::endl;
}

int
main (int argc, char **argv)
{
  if (argc <= 1)
  {
    print_help ();
    return 1;
  }
  const std::string fn_in_pre (argv[1]);
  const std::string fn_out_pre (argv[2]);
  const float upper_bound_d = std::stof (argv[3]);
  const float lower_bound_d = std::stof (argv[4]);
  const float upper_bound_a = std::stof (argv[5]);
  const float lower_bound_a = std::stof (argv[6]);

  CloudI::Ptr cloud_d_to (new CloudI);
  CloudI::Ptr cloud_d_from (new CloudI);
  CloudI::Ptr cloud_a_to (new CloudI);
  CloudI::Ptr cloud_a_from (new CloudI);
  pcl::io::loadPLYFile (fn_in_pre + "_d_to.ply", *cloud_d_to);
  pcl::io::loadPLYFile (fn_in_pre + "_d_from.ply", *cloud_d_from);
  pcl::io::loadPLYFile (fn_in_pre + "_a_to.ply", *cloud_a_to);
  pcl::io::loadPLYFile (fn_in_pre + "_a_from.ply", *cloud_a_from);

  pcl::io::savePLYFileBinary (fn_out_pre + "_d_to_disp.ply",
                              *get_colored_cloud (cloud_d_to, upper_bound_d, lower_bound_d));
  pcl::io::savePLYFileBinary (fn_out_pre + "_d_from_disp.ply",
                              *get_colored_cloud (cloud_d_from, upper_bound_d, lower_bound_d));
  pcl::io::savePLYFileBinary (fn_out_pre + "_a_to_disp.ply",
                              *get_colored_cloud (cloud_a_to, upper_bound_a, lower_bound_a));
  pcl::io::savePLYFileBinary (fn_out_pre + "_a_from_disp.ply",
                              *get_colored_cloud (cloud_a_from, upper_bound_a, lower_bound_a));

  return 0;
}
