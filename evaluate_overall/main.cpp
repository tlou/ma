#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <cstdlib>
#include <sstream>
#include <algorithm>
#include <numeric>
#include <set>
#include <sys/stat.h>

#include <pcl/common/common_headers.h>
#include <pcl/filters/passthrough.h>
#include <pcl/io/ply_io.h>
#include <pcl/PolygonMesh.h>
#include <pcl/conversions.h>

#include <Eigen/Dense>

typedef pcl::PointXYZ Point;
typedef pcl::PointCloud<Point> Cloud;

/**
 * get the area of all triangles
 * @param mesh
 * @param cloud
 * @return
 */
std::vector<float>
get_area_vertice (const pcl::PolygonMeshConstPtr &mesh,
                  const Cloud::ConstPtr &cloud)
{
  std::vector<float> result (mesh->polygons.size (), 0.0f);
  std::vector<pcl::Vertices>::const_iterator it = mesh->polygons.begin ();

  std::generate (result.begin (), result.end (),
                 [&it, cloud]
                 {
                   const Eigen::Vector3f a = cloud->at (it->vertices.at (0)).getVector3fMap ();
                   const Eigen::Vector3f b = cloud->at (it->vertices.at (1)).getVector3fMap ();
                   const Eigen::Vector3f c = cloud->at (it->vertices.at (2)).getVector3fMap ();
                   ++it;
                   return 0.5f * (a - b).cross (c - b).norm ();
                 });

  return result;
}

/**
 *
 * @tparam T
 * @param areas
 * @return
 */
template<typename T>
std::string
vector_to_string (const std::vector<T> &areas)
{
  std::vector<std::string> str_areas (areas.size (), "");
  typename std::vector<T>::const_iterator it = areas.begin ();

  std::generate (str_areas.begin (), str_areas.end (), [&it]
  { return std::to_string (*(it++)) + " "; });
  return std::accumulate (str_areas.begin (), str_areas.end (), std::string (""));
}

/**
 *
 * @param mesh
 * @return
 */
size_t
get_number_vertex (const pcl::PolygonMeshConstPtr &mesh)
{
  // use set to avoid duplicate items
  std::set<uint32_t> set;
  for (const pcl::Vertices vertice: mesh->polygons)
  {
    for (const uint32_t id: vertice.vertices)
    {
      set.insert (id);
    }
  }
  return set.size ();
}

/**
 *
 * @param mesh
 * @return
 */
size_t
get_number_edge (const pcl::PolygonMeshConstPtr &mesh)
{
  // use edge for ascending indexed pair
  typedef std::pair<uint32_t, uint32_t> Edge;
  std::set<Edge> set;
  for (const pcl::Vertices vertice: mesh->polygons)
  {
    Edge edge;
    set.insert (vertice.vertices[0] < vertice.vertices[1] ? Edge (vertice.vertices[0], vertice.vertices[1])
                                                          : Edge (vertice.vertices[1], vertice.vertices[0]));
    set.insert (vertice.vertices[1] < vertice.vertices[2] ? Edge (vertice.vertices[1], vertice.vertices[2])
                                                          : Edge (vertice.vertices[2], vertice.vertices[1]));
    set.insert (vertice.vertices[2] < vertice.vertices[0] ? Edge (vertice.vertices[2], vertice.vertices[0])
                                                          : Edge (vertice.vertices[0], vertice.vertices[2]));
  }
  return set.size ();
}

/**
 *
 * @param mesh
 * @return
 */
size_t
get_number_triangle (const pcl::PolygonMeshConstPtr &mesh)
{
  return mesh->polygons.size ();
}

/**
 *
 * @param areas
 * @param min_area
 * @param max_area
 * @param num_steps
 * @return
 */
std::vector<int>
summarize_area (const std::vector<float> &areas, const float min_area,
                const float max_area, const int num_steps = 100)
{
  std::vector<float> thresholds (num_steps, 0.0f);
  std::vector<int> result (num_steps + 1, 0);

  float threshold_log = std::log (min_area);
  const float step_log = (std::log (max_area) - std::log (min_area)) / (float) (num_steps - 1);
  std::generate (thresholds.begin (), thresholds.end (),
                 [&threshold_log, step_log]
                 {
                   float this_ = std::exp (threshold_log);
                   threshold_log += step_log;
                   return this_;
                 });

  for (const float area: areas)
  {
    const long id = std::distance (thresholds.begin (),
                                   std::find_if (thresholds.begin (), thresholds.end (),
                                                 [area] (const float &thres)
                                                 { return area < thres; }));
    ++result.at (id);
  }

  return result;
}

/**
 * normalize the area distribution
 * @param data
 * @return
 */
std::vector<float>
normalize (const std::vector<int> &data)
{
  const int sum = std::accumulate (data.begin (), data.end (), 0);
  std::vector<float> result (data.size (), 0.0f);
  std::vector<int>::const_iterator it = data.begin ();

  std::generate (result.begin (), result.end (),
                 [&it, sum]
                 {
                   return (float) *(it++) / (float) sum;
                 });

  return result;
}

void
print_help ()
{
  std::cout << "./evaluate_overall path_input_mesh path_output_log_size path_output_log_distribution"
            << std::endl;
}

int
main (int argc, char **argv)
{
  if (argc <= 1)
  {
    print_help ();
  }
  const std::string fn_in = argv[1];
  const std::string fn_log_property = argv[2];
  const std::string fn_log_area = argv[3];

  pcl::PolygonMesh::Ptr mesh (new pcl::PolygonMesh);
  Cloud::Ptr cloud (new Cloud);

  pcl::io::loadPLYFile (fn_in, *mesh);
  pcl::fromPCLPointCloud2 (mesh->cloud, *cloud);

  std::vector<float> stat = normalize (summarize_area (get_area_vertice (mesh, cloud),
                                                       1e-6f, 1e-2f, 100));

  std::ofstream out_property;
  out_property.open (fn_log_property, std::ios_base::app);
  out_property << get_number_vertex (mesh) << ","
               << get_number_edge (mesh) << ","
               << get_number_triangle (mesh) << std::endl;
  out_property.close ();

  std::ofstream out_area;
  out_area.open (fn_log_area, std::ios_base::app);
  out_area << vector_to_string<float> (stat) << std::endl;
  out_area.close ();

  return 0;
}
