#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <sstream>

#include <Eigen/Dense>

#include <pcl/common/common_headers.h>
#include <pcl/filters/passthrough.h>
#include <pcl/io/ply_io.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/surface/gp3.h>

typedef pcl::PointXYZRGBNormal Point;
typedef pcl::PointCloud<Point> Cloud;

/**
 *
 * @param cloud
 * @return
 */
template<class T>
typename pcl::search::KdTree<T>::Ptr
get_search_kdtree (const typename pcl::PointCloud<T>::ConstPtr &cloud)
{
  typename pcl::search::KdTree<T>::Ptr kdtree (new pcl::search::KdTree<T>);
  kdtree->setInputCloud (cloud);
  return kdtree;
}

/**
 *
 * @param filename
 * @return
 */
Cloud::Ptr
load_ply (const std::string &filename)
{
  pcl::PCLPointCloud2::Ptr cloud_in (new pcl::PCLPointCloud2 ());
  Cloud::Ptr cloud (new Cloud ());
  pcl::PLYReader reader;
  reader.read (filename, *cloud_in);
  pcl::fromPCLPointCloud2 (*cloud_in, *cloud);
  return cloud;
}

/**
 * reconstruct with Greed Projection
 * @param cloud
 * @param radius
 * @return
 */
template<class T>
pcl::PolygonMesh::Ptr
trianglulate_greedy (const typename pcl::PointCloud<T>::ConstPtr &cloud,
                     const double radius)
{
  typename pcl::GreedyProjectionTriangulation<T> gpt;
  pcl::PolygonMesh::Ptr mesh (new pcl::PolygonMesh ());

  // Set the maximum distance between connected points (maximum edge length)
  gpt.setSearchRadius (radius);
  gpt.setMu (10.0);
  gpt.setMaximumNearestNeighbors (50);
//  gpt.setMaximumSurfaceAngle(M_PI / 4.0); // 45 degrees
//  gpt.setMinimumAngle(M_PI / 18.0); // 10 degrees
//  gpt.setMaximumAngle(2.0 * M_PI / 3.0); // 120 degrees
  gpt.setNormalConsistency (true);

  gpt.setInputCloud (cloud);
  gpt.setSearchMethod (get_search_kdtree<T> (cloud));
  gpt.reconstruct (*mesh);
  return mesh;
}

void
print_help ()
{
  std::cout << "./greedy path_input_cloud path_output_mesh radius"
            << std::endl;
}

int
main (int argc, char **argv)
{
  if (argc <= 1)
  {
    print_help ();
    return 1;
  }
  const std::string fn_in (argv[1]);
  const std::string fn_out (argv[2]);
  const float radius = std::stof (argv[3]);

  Cloud::Ptr cloud = load_ply (fn_in);

  pcl::PolygonMesh::Ptr result = trianglulate_greedy<Point> (cloud, radius);

  pcl::io::savePLYFileBinary (fn_out, *result);

  return 0;
}