#include <iostream>
#include <string>
#include <thread>
#include <chrono>
#include <mutex>
#include <vector>
#include <cstdlib>
#include <sstream>

#include <Eigen/Dense>

#include <pcl/common/common_headers.h>
#include <pcl/filters/passthrough.h>
#include <pcl/io/ply_io.h>
#include <pcl/conversions.h>

#include "surface/include/pcl/surface/marching_cubes.h"
#include "surface/include/pcl/surface/impl/marching_cubes.hpp"
#include "surface/include/pcl/surface/marching_cubes_hoppe.h"
#include "surface/include/pcl/surface/impl/marching_cubes_hoppe.hpp"

#include "Timer.hpp"

typedef pcl::PointXYZRGBNormal Point;
typedef pcl::PointCloud<Point> Cloud;

/**
 *
 * @param string
 * @param key
 * @return
 */
std::vector<std::string>
parse_string (const std::string &string, const std::string &key)
{
  std::vector<std::string> result;
  std::string copy = string;
  size_t pos = copy.find (key);
  while (!copy.empty () && pos != std::string::npos)
  {
    result.push_back (copy.substr (0, pos));
    copy = copy.substr (pos + 1);
    pos = copy.find (key);
  }
  if (!copy.empty ())
  {
    result.push_back (copy);
  }
  return result;
}

/**
 *
 * @param full_path
 * @return
 */
std::pair<std::string, std::string>
split_path_filename (const std::string &full_path)
{
  std::pair<std::string, std::string> re;
  std::size_t id = full_path.find_last_of ("/");

  re.first = full_path.substr (0, id);
  re.second = full_path.substr (id + 1);

  return re;
}

/**
 *
 * @param filename
 * @return
 */
std::string get_filename_no_suffix (const std::string &filename)
{
  size_t pos = filename.find_last_of (".");
  if (pos != std::string::npos)
  {
    return filename.substr (0, pos);
  }
  else
  {
    return filename;
  }
}

/**
 * read center and splitting size from filename
 * @param filename
 * @param center_x
 * @param center_y
 * @param size
 */
void
parse_filename (const std::string &filename,
                     float &center_x,
                     float &center_y,
                     float &size)
{
  std::vector<std::string> params = parse_string (
    get_filename_no_suffix (parse_string (filename, "/").back ()), "_");
  if (params.size () == 3)
  {
    center_x = std::stof (params.at (0));
    center_y = std::stof (params.at (1));
    size = std::stof (params.at (2));
  }
}

/**
 *
 * @param cloud
 * @return
 */
template<class T>
typename pcl::search::KdTree<T>::Ptr
get_search_kdtree (const typename pcl::PointCloud<T>::ConstPtr &cloud)
{
  typename pcl::search::KdTree<T>::Ptr kdtree (new pcl::search::KdTree<T>);
  kdtree->setInputCloud (cloud);
  return kdtree;
}

/**
 *
 * @param cloud
 * @param resolution
 * @param size_grid
 * @return
 */
template<class T>
pcl::PolygonMesh::Ptr
trianglulate_mesh_mc_hoppe (const typename pcl::PointCloud<T>::ConstPtr &cloud,
                            const Eigen::Vector3i &resolution, const float size_grid)
{
  const float thres_dist = size_grid * 2.0f;
  pcl::PolygonMesh::Ptr mesh (new pcl::PolygonMesh ());

  typename pcl::MarchingCubesHoppe<T> mc;
  mc.setInputCloud (cloud);
  mc.setGridResolution (resolution (0), resolution (1), resolution (2));
  mc.setSearchMethod (get_search_kdtree<T> (cloud));
  mc.setDistanceIgnore (thres_dist * thres_dist);
  mc.reconstruct (*mesh);
  return mesh;
}

/**
 *
 * @param cloud
 * @param size
 * @param size_grid
 * @return
 */
Eigen::Vector3i
get_resolution (const Cloud::ConstPtr &cloud,
                                const float size,
                                const float size_grid)
{
  Point max, min;
  pcl::getMinMax3D (*cloud, min, max);
  return Eigen::Vector3i ((int) std::ceil (std::fabs (max.x - min.x) / size_grid),
                          (int) std::ceil (std::fabs (max.y - min.y) / size_grid),
                          (int) std::ceil (std::fabs (max.z - min.z) / size_grid));
}

/**
 *
 * @param filename
 * @return
 */
Cloud::Ptr
load_ply (const std::string &filename)
{
  pcl::PCLPointCloud2::Ptr cloud_in (new pcl::PCLPointCloud2 ());
  Cloud::Ptr cloud (new Cloud ());
  pcl::PLYReader reader;
  reader.read (filename, *cloud_in);
  pcl::fromPCLPointCloud2 (*cloud_in, *cloud);
  return cloud;
}

/**
 *
 * @param filename
 * @param duration
 * @param time0 starting time
 * @param time1 ending time
 * @return
 */
bool
read_log (const std::string &filename,
               double &duration,
               double &time0,
               double &time1)
{
  std::fstream in;
  std::string shit;
  in.open (filename, std::fstream::in);
  if (in.is_open ())
  {
    in >> shit >> shit >> shit >> duration >> shit >> shit
       >> time0 >> shit >> time1;
    in.close ();
    if (duration > 1e-3 && duration < 1e12)
    { // likely wrong input
      return true;
    }
  }
  return false;
}

void
print_help ()
{
  std::cout << "./hoppe path_input_cloud path_output_mesh cube_size"
            << std::endl;
}

int
main (int argc, char **argv)
{
  if (argc <= 0)
  {
    print_help ();
    return 1;
  }
  const std::string fn_in (argv[1]);
  const std::string fn_out (argv[2]);
  const float size_grid = std::stof (argv[3]);

  Cloud::Ptr cloud = load_ply (fn_in);
  std::pair<std::string, std::string> fn_o_split = split_path_filename (fn_out);

  float center_x, center_y, size;
  parse_filename (fn_in, center_x, center_y, size);

  Timer timer;
  pcl::PolygonMesh::Ptr result =
    trianglulate_mesh_mc_hoppe<Point> (cloud, get_resolution (cloud, size, size_grid),
                                       size_grid);
  double time_elapsed = timer.getSecFromStart ();
  double time_start = timer.getStart ();
  double time_end = timer.getEnd ();
  double duration_log, time0_log, time1_log; // from log
  if (read_log (fn_o_split.first + "/" + "log", duration_log, time0_log, time1_log))
  {
    time_elapsed += duration_log;
    time_start = time0_log;
  }

  pcl::io::savePLYFileBinary (fn_out, *result);

  std::ofstream os;
  os.open (fn_o_split.first + "/" + "log", std::ios_base::out);
  os << std::fixed << "running time is " << time_elapsed << ", from " << time_start
     << " to " << time_end << std::endl;
  os.close ();

  return 0;
}
