//
// Created by tlou on 16.04.17.
//

#ifndef TIMER_HPP
#define TIMER_HPP

#include <ctime>
#include <chrono>
#include <vector>
#include <assert.h>

class Timer
{
protected:
  std::chrono::system_clock::time_point _start;
  std::vector<std::chrono::system_clock::time_point> _ends;

public:
  Timer()
  {
    startNow();
  }

  ~Timer()
  {}

  void startNow()
  {
    _start = std::chrono::system_clock::now();
  }

  double getSecFromStart() const
  {
    return 1e-6f * (double) std::chrono::duration_cast<std::chrono::microseconds>(
        std::chrono::system_clock::now() - _start).count();
  }

  double getStart() const
  {
    return 1e-6f * (double) std::chrono::duration_cast<std::chrono::microseconds>(
        _start.time_since_epoch()).count();
  }
  unsigned long long getStartRaw() const
  {
    return (unsigned long long) std::chrono::duration_cast<std::chrono::microseconds>(
        _start.time_since_epoch()).count();
  }

  void pause()
  {
    _ends.push_back(std::chrono::system_clock::now());
  }

  void clearEnds()
  {
    _ends.clear();
  }

  double getEnd(const int id) const    
  {
    assert(id > 0 && id < _ends.size());
    return 1e-6f * (double) std::chrono::duration_cast<std::chrono::microseconds>(
        _ends.at(id).time_since_epoch()).count();
  }

  double getEnd() const 
  {
    return 1e-6f * (double) std::chrono::duration_cast<std::chrono::microseconds>(
        std::chrono::system_clock::now().time_since_epoch()).count();
  }

  double getSecToEnd(const int id) const 
  {
    assert(id > 0 && id < _ends.size());
    return 1e-6f * (double) std::chrono::duration_cast<std::chrono::microseconds>(
        _ends.at(id) - _start).count();
  }
};

#endif //KL_PARALLEL_TIMER_HPP
