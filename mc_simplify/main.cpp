#include <iostream>
#include <string>
#include <thread>
#include <chrono>
#include <mutex>
#include <vector>
#include <map>
#include <ctime>
#include <sstream>
#include <fstream>
#include <algorithm>

#include <Eigen/Dense>

#include <pcl/point_types.h>
#include <pcl/common/common_headers.h>
#include <pcl/io/ply_io.h>
#include <pcl/kdtree/kdtree_flann.h>

typedef pcl::PointXYZRGB Point;
typedef pcl::PointCloud<Point> Cloud;
typedef pcl::PointXYZ PointXYZ;
typedef pcl::PointCloud<PointXYZ> CloudXYZ;

/**
 * remap the indices of the vertices
 * @param mesh
 * @param map
 */
void
remap_mesh (pcl::PolygonMesh::Ptr &mesh,
            std::vector<uint32_t> &map)
{
  for (pcl::Vertices &triangle: mesh->polygons)
  {
    for (uint32_t &id_vertice: triangle.vertices)
    {
      id_vertice = map.at (id_vertice);
    }
  }
}

/**
 * remove overlapping triangles
 * @param cloud
 * @param mesh
 * @param threshold
 */
void
filter_triangle (const CloudXYZ::ConstPtr &cloud,
                 pcl::PolygonMesh::Ptr &mesh,
                 const float threshold = 1e-6f)
{
  // lambda function for deciding whehther two triangles has same indices
  auto is_triangles_same = [cloud, mesh] (const int id0, const int id1)->bool
  {
    // const Eigen::Vector3f p00 = cloud->at (mesh->polygons.at (id0).vertices.at (0)).getVector3fMap ();
    // const Eigen::Vector3f p01 = cloud->at (mesh->polygons.at (id0).vertices.at (1)).getVector3fMap ();
    // const Eigen::Vector3f p02 = cloud->at (mesh->polygons.at (id0).vertices.at (2)).getVector3fMap ();
    // const Eigen::Vector3f p10 = cloud->at (mesh->polygons.at (id1).vertices.at (0)).getVector3fMap ();
    // const Eigen::Vector3f p11 = cloud->at (mesh->polygons.at (id1).vertices.at (1)).getVector3fMap ();
    // const Eigen::Vector3f p12 = cloud->at (mesh->polygons.at (id1).vertices.at (2)).getVector3fMap ();

    // return ((p00 - p10).norm () + (p01 - p11).norm () + (p02 - p12).norm ()) < threshold // 0 1 2
    //        || ((p00 - p10).norm () + (p01 - p12).norm () + (p02 - p11).norm ()) < threshold // 0 2 1
    //        || ((p00 - p11).norm () + (p01 - p10).norm () + (p02 - p12).norm ()) < threshold // 1 0 2
    //        || ((p00 - p11).norm () + (p01 - p12).norm () + (p02 - p10).norm ()) < threshold // 1 2 0
    //        || ((p00 - p12).norm () + (p01 - p11).norm () + (p02 - p10).norm ()) < threshold // 2 1 0
    //        || ((p00 - p12).norm () + (p01 - p10).norm () + (p02 - p11).norm ()) < threshold; // 2 0 1
    const std::vector<uint32_t> &ver0 = mesh->polygons.at (id0).vertices;
    const std::vector<uint32_t> &ver1 = mesh->polygons.at (id1).vertices;
    return (ver0.at (0) == ver1.at (0) && ver0.at (1) == ver1.at (1) && ver0.at (2) == ver1.at (2))  // 0 1 2
           || (ver0.at (0) == ver1.at (0) && ver0.at (1) == ver1.at (2) && ver0.at (2) == ver1.at (1))  // 0 2 1
           || (ver0.at (0) == ver1.at (1) && ver0.at (1) == ver1.at (0) && ver0.at (2) == ver1.at (2))  // 1 0 2
           || (ver0.at (0) == ver1.at (1) && ver0.at (1) == ver1.at (2) && ver0.at (2) == ver1.at (0))  // 1 2 0
           || (ver0.at (0) == ver1.at (2) && ver0.at (1) == ver1.at (1) && ver0.at (2) == ver1.at (0))  // 2 1 0
           || (ver0.at (0) == ver1.at (2) && ver0.at (1) == ver1.at (0) && ver0.at (2) == ver1.at (1)); // 2 0 1
  };
  CloudXYZ::Ptr cloud_centroid (new CloudXYZ ());
  pcl::KdTreeFLANN<PointXYZ> kdtree;
  std::vector<bool> is_duplicate (mesh->polygons.size (), false);

  // get cloud of centroids to accelerate the comparison
  cloud_centroid->resize (mesh->polygons.size ());
  size_t id_triangle = 0;
  std::generate (cloud_centroid->begin (), cloud_centroid->end (),
                 [&id_triangle, cloud, mesh]
                 {
                   const std::vector<uint32_t> &vertice = mesh->polygons.at (id_triangle).vertices;
                   ++id_triangle;
                   PointXYZ pos;
                   pos.getVector3fMap () = (cloud->at (vertice.at (0)).getVector3fMap ()
                                            + cloud->at (vertice.at (1)).getVector3fMap ()
                                            + cloud->at (vertice.at (2)).getVector3fMap ()) / 3.0f;
                   return pos;
                 });

  kdtree.setInputCloud (cloud_centroid);

  // label overlapping triangles with neighbouring centroids
  for (size_t id_centroid = 0; id_centroid < cloud_centroid->size (); ++id_centroid)
  {
    const PointXYZ &point = cloud_centroid->at (id_centroid);
    std::vector<int> indices;
    std::vector<float> sqr_dists;

    if (!is_duplicate.at (id_centroid) && kdtree.radiusSearch (point, threshold, indices, sqr_dists) > 0)
    {
      for (const int id_neighbor: indices)
      {
        if (id_neighbor > id_centroid && !is_duplicate.at (id_neighbor)
            && is_triangles_same ((int) id_centroid, id_neighbor))
        {
          is_duplicate.at (id_neighbor) = true;
        }
      }
    }
  }

  // filter
  std::vector<pcl::Vertices> original;
  original.swap (mesh->polygons);
  mesh->polygons.reserve (original.size ());
  for (size_t id_centroid = 0; id_centroid < cloud_centroid->size (); ++id_centroid)
  {
    if (!is_duplicate.at (id_centroid))
    {
      mesh->polygons.push_back (original.at (id_centroid));
    }
  }
}

/**
 * remap the overlapping vertices to the first occurrence
 * @param cloud
 * @param mesh
 * @param threshold
 */
void
remap_vertices (const CloudXYZ::ConstPtr &cloud,
                pcl::PolygonMesh::Ptr &mesh,
                const float threshold = 1e-6f)
{
  auto is_points_same = [cloud] (const int id0, const int id1, const float threshold)->bool
  {
    return (cloud->at (id0).getVector3fMap () - cloud->at (id1).getVector3fMap ()).norm () < threshold;
  };
  pcl::KdTreeFLANN<PointXYZ> kdtree;
  std::vector<bool> is_duplicate (cloud->size (), false);
  std::vector<uint32_t> map (cloud->size (), 0);

  kdtree.setInputCloud (cloud);
  {
    uint32_t tmp = 0;
    std::generate (map.begin (), map.end (), [&tmp]
    { return tmp++; });
  }

  // find overlapping vertices and assign indices to it
  for (size_t id_centroid = 0; id_centroid < cloud->size (); ++id_centroid)
  {
    const PointXYZ &point = cloud->at (id_centroid);
    std::vector<int> indices;
    std::vector<float> sqr_dists;

    if (kdtree.radiusSearch (point, threshold, indices, sqr_dists) > 0)
    {
      for (const int id_neighbor: indices)
      {
        if (id_neighbor > id_centroid && !is_duplicate.at (id_neighbor)
            && is_points_same ((int) id_centroid, id_neighbor, threshold))
        {
          is_duplicate.at (id_neighbor) = true;
          map.at (id_neighbor) = (uint32_t) id_centroid;
        }
      }
    }
  }

  remap_mesh (mesh, map);
}

/**
 *
 * @param mesh
 */
void
filter_invalid_triangles (pcl::PolygonMesh::Ptr &mesh)
{
  std::vector<bool> is_valid (mesh->polygons.size (), true);

  for (size_t id_tri = 0; id_tri < mesh->polygons.size (); ++id_tri)
  {
    const std::vector<uint32_t> &indices = mesh->polygons.at (id_tri).vertices;
    if (indices.at (0) == indices.at (1) || indices.at (1) == indices.at (2)
        || indices.at (2) == indices.at (0))
    {
      is_valid.at (id_tri) = false;
    }
  }

  std::vector<pcl::Vertices> original;
  original.swap (mesh->polygons);
  mesh->polygons.clear ();
  mesh->polygons.reserve (original.size ());
  for (size_t id = 0; id < original.size (); ++id)
  {
    if (is_valid.at (id))
    {
      mesh->polygons.push_back (original.at (id));
    }
  }
}

/**
 *
 * @param mesh
 */
void
filter_unused_points (pcl::PolygonMesh::Ptr &mesh)
{
  Cloud cloud;
  pcl::fromPCLPointCloud2 (mesh->cloud, cloud);
  std::vector<bool> is_used (cloud.size (), false);

  for (const pcl::Vertices &triangle: mesh->polygons)
  {
    for (const uint32_t &id_vertice: triangle.vertices)
    {
      is_used.at (id_vertice) = true;
    }
  }

  {
    std::vector<uint32_t> map (cloud.size (), 0);
    uint32_t count = 0;
    for (uint32_t id = 0; id < (uint32_t) map.size (); ++id)
    {
      if (is_used.at (id))
      {
        map.at (id) = count;
        ++count;
      }
    }
    remap_mesh (mesh, map);
  }

  Cloud cloud_filtered;
  cloud_filtered.reserve (cloud.size ());
  for (size_t id = 0; id < is_used.size (); ++id)
  {
    if (is_used.at (id))
    {
      cloud_filtered.push_back (cloud.at (id));
    }
  }
  pcl::toPCLPointCloud2 (cloud_filtered, mesh->cloud);
}

void
print_help
{
  std::cout << "./mc_simplify path_input_mesh path_output_mesh path_output_log"
            << std::endl;
};

int 
main (int argc, char **argv)
{
  if (argc <= 1)
  {
    print_help ();
    return 1;
  }
  const std::string fn_in (argv[1]);
  const std::string fn_out (argv[2]);
  const std::string fn_log (argv[3]);

  pcl::PolygonMesh::Ptr mesh (new pcl::PolygonMesh ());
  CloudXYZ::Ptr cloud (new CloudXYZ ());

  pcl::io::loadPLYFile (fn_in, *mesh);
  clock_t time_begin = clock ();
  pcl::fromPCLPointCloud2 (mesh->cloud, *cloud);

  size_t size_cloud_in, size_cloud_out;
  size_t size_mesh_in, size_mesh_out;

  size_cloud_in = mesh->cloud.width;
  size_mesh_in = mesh->polygons.size ();
  std::cout << mesh->cloud.width << " " << mesh->polygons.size () << std::endl;

  remap_vertices (cloud, mesh);
  filter_triangle (cloud, mesh);
  filter_invalid_triangles (mesh);
  cloud->clear ();
  filter_unused_points (mesh);

  clock_t time_end = clock ();
  double elapsed_secs = double (time_end - time_begin) / CLOCKS_PER_SEC;

  size_cloud_out = mesh->cloud.width;
  size_mesh_out = mesh->polygons.size ();
  std::cout << mesh->cloud.width << " " << mesh->polygons.size () << std::endl;

  pcl::io::savePLYFileBinary (fn_out, *mesh);

  std::ofstream out_area;
  out_area.open (fn_log, std::ios_base::app);
  out_area << size_cloud_in << "," << size_cloud_out << ","
           << size_mesh_in << "," << size_mesh_out << ","
           << elapsed_secs << std::endl;
  out_area.close ();

  return 0;
}
