import os
import sys

DIR_IN = sys.argv[1]
DIR_OUT = sys.argv[2]

EXE = "./build/mc_simplify"
assert os.path.isdir(DIR_IN)
assert os.path.isdir(DIR_OUT)
assert os.path.isfile(EXE)

DIR_IN = DIR_IN if DIR_IN[-1] == "/" else DIR_IN + "/"
DIR_OUT = DIR_OUT if DIR_OUT[-1] == "/" else DIR_OUT + "/"
FN_LOG = DIR_OUT + "log"

for it in [it for it in os.listdir(DIR_IN) if ".ply" in it and os.path.isfile(DIR_IN + it)]:
    fn_in = DIR_IN + it
    fn_out = DIR_OUT + it
    command = " ".join((EXE, fn_in, fn_out, FN_LOG))
    print command
    os.system("echo " + it + " >> " + DIR_OUT + "order")
    os.system(command)
