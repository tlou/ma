#include <iostream>
#include <dirent.h>
#include <vector>
#include <fstream>

#include <pcl/common/common_headers.h>
#include <pcl/filters/passthrough.h>
#include <pcl/io/ply_io.h>

#include "Timer.hpp"

/**
 *
 * @param filename
 * @return
 */
std::string
get_filename_no_suffix (const std::string &filename)
{
  size_t pos = filename.find_last_of (".");
  if (pos != std::string::npos)
  {
    return filename.substr (0, pos);
  }
  else
  {
    return filename;
  }
}

/**
 *
 * @param string
 * @param key
 * @return
 */
std::vector<std::string>
parse_string (const std::string &string,
              const std::string &key)
{
  std::vector<std::string> result;
  std::string copy = string;
  size_t pos = copy.find (key);
  while (!copy.empty () && pos != std::string::npos)
  {
    result.push_back (copy.substr (0, pos));
    copy = copy.substr (pos + 1);
    pos = copy.find (key);
  }
  if (!copy.empty ())
  {
    result.push_back (copy);
  }
  return result;
}

/**
 * get all filenames in a directory
 * @param dir_name
 * @param suffix
 * @return
 */
std::vector<std::string>
get_all_fn (const std::string &dir_name,
            const std::string &suffix)
{
  std::vector<std::string> result;
  DIR *dir = opendir (dir_name.c_str ());
  if (dir)
  {
    dirent *entry;
    while ((entry = readdir (dir)) != NULL)
    {
      std::string fn (std::string (entry->d_name));
      if (parse_string (fn, ".").back () == suffix)
      {
        result.push_back (fn);
      }
    }
    closedir (dir);
  }
  return result;
}

/**
 *
 * @param filename
 * @return
 */
pcl::PolygonMesh::Ptr
load_ply (const std::string &filename)
{
  pcl::PolygonMesh::Ptr result (new pcl::PolygonMesh ());
  pcl::io::loadPLYFile (filename, *result);
  return result;
}

/**
 * get boundaries from filename
 * @param filename
 * @param xlim
 * @param ylim
 */
void
parse_filename (const std::string &filename,
                Eigen::Vector2f &xlim,
                Eigen::Vector2f &ylim)
{
  std::vector<std::string> params = parse_string (
    get_filename_no_suffix (parse_string (filename, "/").back ()), "_");
  if (params.size () == 3)
  {
    float center_x = std::stof (params.at (0));
    float center_y = std::stof (params.at (1));
    float size = std::stof (params.at (2));
    xlim << center_x - size / 2.0f, center_x + size / 2.0f;
    ylim << center_y - size / 2.0f, center_y + size / 2.0f;
  }
}

/**
 * get indices of points in region
 * @param cloud
 * @param xlim
 * @param ylim
 * @return
 */
std::vector<int>
get_passthrough_index (const pcl::PCLPointCloud2 &cloud,
                       const Eigen::Vector2f &xlim,
                       const Eigen::Vector2f &ylim)
{
  pcl::PointCloud<pcl::PointXYZRGBNormal> cloud_explicit;
  std::vector<int> index;
  pcl::fromPCLPointCloud2 (cloud, cloud_explicit);
  index.reserve (cloud_explicit.size ());

  for (size_t id = 0; id < cloud_explicit.size (); ++id)
  {
    const pcl::PointXYZRGBNormal &pt = cloud_explicit.at (id);
    if (pt.x >= xlim (0) && pt.x <= xlim (1) && pt.y >= ylim (0) && pt.y <= ylim (1))
    {
      index.push_back ((int) id);
    }
  }

  return index;
}

/**
 * get indices of vertices of given triangles (vertices in arguments)
 * @param vertices
 * @param index_mesh
 * @param num_vertex
 * @return
 */
std::vector<int>
get_vertex_index (const std::vector<pcl::Vertices> &vertices,
                  const std::vector<int> &index_mesh,
                  const size_t num_vertex)
{
  std::vector<bool> is_in (num_vertex, false);
  std::vector<int> index;
  index.reserve (num_vertex);

  // binary indicator of occurrence
  for (const int id_mesh : index_mesh)
  {
    const pcl::Vertices &triangle = vertices.at (id_mesh);
    for (const uint32_t &id : triangle.vertices)
    {
      is_in.at (id) = true;
    }
  }

  // get indices of true indicators
  for (size_t id = 0; id < num_vertex; ++id)
  {
    if (is_in.at (id))
    {
      index.push_back ((int) id);
    }
  }

  return index;
}

/**
 * get triangles inside the region (at least one vertex in region)
 * @param vertices
 * @param index_vertex
 * @param num_point
 * @return
 */
std::vector<int>
get_involved_mesh_index (const std::vector<pcl::Vertices> &vertices,
                         const std::vector<int> &index_vertex,
                         const size_t num_point)
{
  std::vector<int> count (vertices.size (), 0);
  std::vector<bool> is_in (num_point, false);
  std::vector<int> index;
  index.reserve (vertices.size ());

  // binary indicator of whether vertices are in region
  for (const int id : index_vertex)
  {
    is_in.at (id) = true;
  }

  // number of vertices in region
  for (size_t id_mesh = 0; id_mesh < vertices.size (); ++id_mesh)
  {
    const pcl::Vertices &triangle = vertices.at (id_mesh);
    for (const uint32_t &id : triangle.vertices)
    {
      if (is_in.at (id))
      {
        ++count.at (id_mesh);
      }
    }
  }

  // push indices of triangles with at least one vertex in region
  for (size_t id_mesh = 0; id_mesh < vertices.size (); ++id_mesh)
  {
    if (count.at (id_mesh) >= 1)
    {
      index.push_back ((int) id_mesh);
    }
  }

  return index;
}

/**
 * get map of vertices
 * @param index_point
 * @param offset
 * @param size_cloud
 * @return
 */
std::vector<uint32_t>
get_mapping_point (const std::vector<int> &index_point,
                   const uint32_t offset,
                   const size_t size_cloud)
{
  std::vector<uint32_t> result;
  result.resize (size_cloud);

  for (size_t id = 0; id < index_point.size (); ++id)
  {
    result.at (index_point.at (id)) = (uint32_t) id + offset;
  }

  return result;
}

/**
 * add new_mesh to total_mesh
 * @param total_mesh
 * @param new_mesh
 * @param xlim
 * @param ylim
 */
void
merge (pcl::PolygonMesh::Ptr &total_mesh,
       const pcl::PolygonMesh::ConstPtr &new_mesh,
       const Eigen::Vector2f &xlim,
       const Eigen::Vector2f &ylim)
{
  const size_t size_cloud = new_mesh->cloud.height * new_mesh->cloud.width;
  const std::vector<int> index_point_in = get_passthrough_index (new_mesh->cloud, xlim, ylim);
  const std::vector<int> index_triangle = get_involved_mesh_index (new_mesh->polygons, index_point_in, size_cloud);
  const std::vector<int> index_point = get_vertex_index (new_mesh->polygons, index_triangle, size_cloud);
  const std::vector<uint32_t> mapping = get_mapping_point (index_point, total_mesh->cloud.width, new_mesh->cloud.width);

  // append cloud
  if (total_mesh->cloud.data.empty ())
  {
    total_mesh->cloud = new_mesh->cloud;
    total_mesh->cloud.width = 0;
    total_mesh->cloud.data.clear ();
  }

  {
    total_mesh->cloud.data.reserve ((total_mesh->cloud.width + index_point.size ())
                                    * new_mesh->cloud.point_step);
    std::vector<pcl::uint8_t> &data = total_mesh->cloud.data;
    const std::vector<pcl::uint8_t> &data_src = new_mesh->cloud.data;
    for (const int id_pt : index_point)
    {
      data.insert (data.end (),
                   data_src.begin () + id_pt * new_mesh->cloud.point_step,
                   data_src.begin () + (id_pt + 1) * new_mesh->cloud.point_step);
    }
    total_mesh->cloud.width += index_point.size ();
  }

  // append mesh
  total_mesh->polygons.reserve (total_mesh->polygons.size () + index_triangle.size ());
  for (const int id_tri : index_triangle)
  {
    total_mesh->polygons.push_back (new_mesh->polygons.at (id_tri));
    for (uint32_t &id : total_mesh->polygons.back ().vertices)
    {
      id = mapping.at (id);
    }
  }
}

void
print_help ()
{
  std::cout << "./merge_mesh path_input_directory path_output_directory"
            << std::endl;
}

int
main (int argc, char **argv)
{
  if (argc <= 1)
  {
    print_help ();
    return 1;
  }

  const std::string dir_in (argv[1]);
  const std::string dir_out (argv[2]);

  Timer timer;
  double time_load = 0.0;

  std::vector<std::string> fns = get_all_fn (dir_in, "ply");
  pcl::PolygonMesh::Ptr total_mesh (new pcl::PolygonMesh ());
  total_mesh->cloud.height = 1; // avoid undeleted size calculation

  for (std::string fn : fns)
  {
    Timer timer_load;
    Eigen::Vector2f xlim, ylim;
    std::string complete_fn = dir_in + "/" + fn;
    pcl::PolygonMesh::Ptr mesh = load_ply (complete_fn);

    time_load += timer_load.getSecFromStart ();
    parse_filename (complete_fn, xlim, ylim);
    merge (total_mesh, mesh, xlim, ylim);
  }

  std::ofstream os;
  os.open (dir_out + "/" + parse_string (dir_in, "/").back () + "_log");
  os << std::fixed << "running time is " << timer.getSecFromStart () - time_load << ", from " << timer.getStart ()
     << " to " << timer.getEnd () << ", plus " << time_load << "s for reading files" << std::endl;
  os.close ();

  pcl::io::savePLYFileBinary (dir_out + "/" + parse_string (dir_in, "/").back () + ".ply", *total_mesh);

  return 0;
}
