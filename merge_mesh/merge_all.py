import sys
import os

DIR = sys.argv[1]
KEY = sys.argv[2]

EXE = "./build/merge_mesh"

if not os.path.isfile(EXE):
    print "exe not found"
    sys.exit()

list_dir = [d for d in os.listdir(DIR) if KEY in d]
for it_dir in list_dir:
    command = EXE + " " + DIR + "/" + it_dir + " " + DIR
    print command
    os.system(command)
