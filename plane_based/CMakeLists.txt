cmake_minimum_required(VERSION 2.8)
project(plane_based)

set(CMAKE_CXX_STANDARD 11)
#set(CMAKE_BUILD_TYPE Release)
add_compile_options(-std=c++11)

find_package(PCL REQUIRED)
find_package(Eigen3 REQUIRED)
include_directories(
  ${PCL_INCLUDE_DIRS}
  ${EIGEN3_INCLUDE_DIR})

set(SOURCE_FILES main.cpp)
add_executable(plane_based ${SOURCE_FILES})
target_link_libraries(plane_based
  ${PCL_LIBRARIES}
  ${Eigen3_LIBRARIES})
