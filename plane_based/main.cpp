/**
 * this code is for testing possible use of concave hull to simplify the reconstruction and model
 *
 * concave hull directly as polygon works but looks strange, thus is not applied
 *
 * but the simplification is very efficient
 */

#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <sstream>
#include <algorithm>

#include <Eigen/Dense>
#include <Eigen/StdVector>

#include <pcl/point_types.h>
#include <pcl/common/common_headers.h>
#include <pcl/io/ply_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/filters/passthrough.h>
#include <pcl/sample_consensus/method_types.h>
//#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/common/centroid.h>
#include <pcl/common/transforms.h>
#include <pcl/surface/concave_hull.h>
#include <pcl/surface/ear_clipping.h>
#include <pcl/features/normal_3d.h>
#include <pcl/segmentation/region_growing.h>

#define IS_SAVE_INLIER

typedef pcl::PointXYZRGBNormal Point;
typedef pcl::PointCloud<Point> Cloud;
//typedef std::vector<Eigen::Vector4f, Eigen::aligned_allocator<Eigen::Vector4f> > VVector4;
typedef std::vector<int> Index;

//typedef std::pair<Index, Eigen::Vector4f, std::less<int>,
//    Eigen::aligned_allocator<std::pair<const Index, Eigen::Vector4f> > > Segment;
typedef std::pair<Index, Eigen::Vector4f> Segment;

/**
 * extract cloud with index
 * @param cloud
 * @param index
 * @return
 */
Cloud::Ptr
extract_cloud (const Cloud::ConstPtr &cloud, const Index &index)
{
  Cloud::Ptr sub_cloud (new Cloud ());
  pcl::PointIndices::Ptr sub_indice (new pcl::PointIndices ());
  pcl::ExtractIndices<Point> extract;

  sub_indice->indices = index;
  extract.setInputCloud (cloud);
  extract.setIndices (sub_indice);
  extract.setNegative (false);
  extract.filter (*sub_cloud);

  return sub_cloud;
}

/**
 * get a random vector which is perpendicular to normal
 * @param normal
 * @return
 */
Eigen::Vector3f
get_rand_perpendicular (const Eigen::Vector3f &normal)
{
  Eigen::Vector3f result;
  if (std::fabs (normal (2)) < 0.25f)
  {
    result << normal (1), -normal (0), 0.0f;
  }
  else
  {
    result << 0.0f, normal (2), -normal (1);
  }
  return result / result.norm ();
}

/**
 * get the index of maxima to direction
 * @param cloud
 * @param direction
 * @return
 */
int
get_id_maxima_direction (const Cloud::ConstPtr &cloud,
                         const Eigen::Vector3f &direction)
{
  Point center_pt;
  pcl::computeCentroid<Point, Point> (*cloud, center_pt);
  Eigen::Vector3f center (center_pt.getArray3fMap ());

  int result = 0;
  float max_dist = direction.dot (Eigen::Vector3f (cloud->front ().getArray3fMap ()) - center);
  for (size_t id = 1; id < cloud->size (); ++id)
  {
    float dist = direction.dot (Eigen::Vector3f (cloud->at (id).getArray3fMap ()) - center);
    if (dist > max_dist)
    {
      max_dist = dist;
      result = (int) id;
    }
  }

  return result;
}

/**
 * get the index of points of concave hull
 * @param cloud
 * @param normal
 * @param r_outer maximal step length
 * @param r_inner minimal step length (avoids too slow or stopping)
 * @param max_concave_angle
 * @return
 */
Index
find_index_concave_hull (const Cloud::ConstPtr &cloud,
                         const Eigen::Vector3f &normal,
                         const float r_outer,
                         const float r_inner,
                         const float max_concave_angle)
{
  Index index;
  std::vector<bool> is_explored (cloud->size (), false);
  Eigen::Vector3f current_dir (get_rand_perpendicular (normal));
  pcl::KdTreeFLANN<Point> kdtree;
  const float r_inner_sqr = r_inner * r_inner;

  kdtree.setInputCloud (cloud);

  // get maxima at one direction
  index.push_back (get_id_maxima_direction (cloud, current_dir));
  is_explored.at (index.back ()) = true;
  current_dir << normal.cross (current_dir);

  // add the next point to make direction, until it reaches itself
  do
  {
    Index index_near;
    std::vector<float> dist_near;
    kdtree.radiusSearch (index.back (), r_outer, index_near, dist_near);
    if (index_near.empty ())
    {
      // cannot find neighbours
      break;
    }

    // search for next point
    int next_id = -1;
    float best_angle = (float) (M_PI * 2.0);
    const Eigen::Vector3f expect_dir (-normal.cross (current_dir));
    const Eigen::Vector3f pos (cloud->at (index.back ()).getArray3fMap ());

    for (size_t i = 0; i < index_near.size (); ++i)
    {
      const int id = index_near.at (i);
      if (dist_near.at (i) > r_inner_sqr)
      {
        Eigen::Vector3f from_pos = Eigen::Vector3f (cloud->at (id).getArray3fMap ()) - pos;
        float x = current_dir.dot (from_pos);
        float y = expect_dir.dot (from_pos);
        float angle = std::atan2 (y, x);

        if (angle < best_angle && angle > -max_concave_angle)
        {
          best_angle = angle;
          next_id = id;
        }
      }
    }

    // whether it should end
    if (next_id >= 0 && !is_explored.at (next_id))
    {
      index.push_back (next_id);
      current_dir << Eigen::Vector3f (cloud->at (next_id).getArray3fMap ()) - pos;
      current_dir.normalize ();
      is_explored.at (next_id) = true;
    }
    else
    {
      break;
    }
  } while (index.back () != index.front ());

  return index;
}

/**
 * find the points to make concave hull, same arguments as the last function
 * @param cloud
 * @param normal
 * @param r_outer
 * @param r_inner
 * @param max_concave_angle
 * @return
 */
Cloud::Ptr
find_concave_hull (const Cloud::ConstPtr &cloud, const Eigen::Vector3f &normal,
                   const float r_outer, const float r_inner, const float max_concave_angle)
{
  Index index_ch = find_index_concave_hull (cloud, normal, r_outer, r_inner, max_concave_angle);
  Index index_ch_neg = find_index_concave_hull (cloud, -normal, r_outer, r_inner, max_concave_angle);
  if (index_ch.size () < index_ch_neg.size ())
  {
    index_ch.swap (index_ch_neg);
  }
  return extract_cloud (cloud, index_ch);
}

/**
 *
 * @param filename
 * @return
 */
Cloud::Ptr
load_ply (const std::string &filename)
{
  pcl::PCLPointCloud2::Ptr cloud_in (new pcl::PCLPointCloud2 ());
  Cloud::Ptr cloud (new Cloud ());
  pcl::PLYReader reader;
  reader.read (filename, *cloud_in);
  pcl::fromPCLPointCloud2 (*cloud_in, *cloud);
  return cloud;
}

/**
 * get transform to origin
 * @param cloud_in
 * @return
 */
Eigen::Matrix4f
getTransform (const Cloud::ConstPtr &cloud_in)
{
  Eigen::Matrix4f affine = Eigen::Matrix4f::Identity ();
  Eigen::Vector4f translate;
  Eigen::Matrix3f rotation;
  pcl::compute3DCentroid<Point> (*cloud_in, translate);
  affine.block (0, 3, 3, 1) << translate.segment (0, 3);
  return affine;
}

/**
 * get the original index of subset in index_total, and filter index_total
 * @param index_total
 * @param subset
 * @return
 */
Index
get_original_index (Index &index_total, const Index &subset)
{
  Index index_rest;
  Index index_reordered;
  std::vector<bool> is_subset (index_total.size (), false);

  index_rest.reserve (index_total.size () - subset.size ());
  index_reordered.reserve (subset.size ());

  for (const int id : subset)
  {
    is_subset.at (id) = true;
  }

  for (int id = 0; id < (int) index_total.size (); ++id)
  {
    const int id_origin = index_total.at (id);
    if (is_subset.at (id))
    {
      index_reordered.push_back (id_origin);
    }
    else
    {
      index_rest.push_back (id_origin);
    }
  }

  index_rest.swap (index_total);
  return index_reordered;
}

/**
 * like range in python
 * @param last
 * @return
 */
Index
range (const int last)
{
  Index list_index;
  list_index.reserve (last);
  for (int i = 0; i < last; ++i)
  {
    list_index.push_back (i);
  }
  return list_index;
}

/**
 *
 * @param total_mesh
 * @param new_mesh
 */
void
merge (pcl::PolygonMesh::Ptr &total_mesh, const pcl::PolygonMesh::ConstPtr &new_mesh)
{
  // append polygon
  {
    const size_t len_surface_0 = total_mesh->polygons.size ();
    const uint32_t len_cloud_0 = total_mesh->cloud.width;
    total_mesh->polygons.insert (total_mesh->polygons.end (),
                                 new_mesh->polygons.begin (), new_mesh->polygons.end ());
    for (size_t id_tri = len_surface_0; id_tri < total_mesh->polygons.size (); ++id_tri)
    {
      std::vector<uint32_t> &vertices = total_mesh->polygons.at (id_tri).vertices;
      for (uint32_t &v : vertices)
      {
        v += len_cloud_0;
      }
    }
  }

  // append cloud
  if (total_mesh->cloud.data.empty ())
  {
    total_mesh->cloud = new_mesh->cloud;
    total_mesh->cloud.width = 0;
    total_mesh->cloud.data.clear ();
  }
  total_mesh->cloud.width += new_mesh->cloud.width;
  total_mesh->cloud.data.insert (total_mesh->cloud.data.end (),
                                 new_mesh->cloud.data.begin (), new_mesh->cloud.data.end ());
}

/**
 * simply make cloud the vertices of a polygon (concave hull)
 * @param cloud
 * @return
 */
pcl::PolygonMesh::Ptr
simple_link (const Cloud::ConstPtr &cloud)
{
  pcl::PolygonMesh::Ptr mesh (new pcl::PolygonMesh ());

  pcl::toPCLPointCloud2 (*cloud, mesh->cloud);
  mesh->polygons.resize (1);
  mesh->polygons.at (0).vertices.resize (cloud->size ());
  for (unsigned int i = 0; i < cloud->size (); ++i)
  {
    mesh->polygons.at (0).vertices.push_back (i);
  }

  return mesh;
}

/**
 * get planes in cloud
 * @param cloud_in
 * @param thres_dist
 * @param thres_size
 * @return
 */
std::vector<Segment>
simple_planes_segment (const Cloud::ConstPtr &cloud_in,
                       const float thres_dist,
                       const int thres_size)
{
  std::vector<Segment> list_segment;
  pcl::SACSegmentationFromNormals<Point, Point> seg;
  pcl::ExtractIndices<Point> extract;
  Eigen::Matrix4f transform = getTransform (cloud_in);
  Cloud::Ptr cloud_tf (new Cloud ());
  Index index_point = range (cloud_in->size ());
  Index index_inlier; // for debug

  pcl::transformPointCloud<Point> (*cloud_in, *cloud_tf,
                                   Eigen::Matrix4f (transform.inverse ()));

  seg.setOptimizeCoefficients (true);
  seg.setModelType (pcl::SACMODEL_PLANE);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setDistanceThreshold (thres_dist);
  seg.setNormalDistanceWeight (1.0f);

  for (int iter = 0; iter < 50; ++iter)
  {
    Eigen::Vector4f plane;
    Index list_id_inlier;
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
    pcl::PointIndicesPtr inliers (new pcl::PointIndices ());

    seg.setInputCloud (cloud_tf);
    seg.setInputNormals (cloud_tf);
    seg.segment (*inliers, *coefficients);

    if (cloud_tf->size () < thres_size || inliers->indices.size () < thres_size)
    {
      break;
    }

    plane << coefficients->values[0], coefficients->values[1],
      coefficients->values[2], coefficients->values[3];
    plane (3) -= transform.col (3).segment (0, 3).dot (plane.segment (0, 3));

    list_id_inlier = get_original_index (index_point, inliers->indices);
    list_segment.push_back (Segment (list_id_inlier, plane));

    extract.setInputCloud (cloud_tf);
    extract.setIndices (inliers);
    extract.setNegative (true);
    extract.filter (*cloud_tf);

#ifdef IS_SAVE_INLIER
    {
      index_inlier.insert (index_inlier.end (),
                           list_id_inlier.begin (),
                           list_id_inlier.end ());
    }
#endif
  }

#ifdef IS_SAVE_INLIER
  {
    pcl::PointIndicesPtr inliers (new pcl::PointIndices ());
    Cloud::Ptr cloud_inlier (new Cloud ());
    inliers->indices = index_inlier;
    extract.setInputCloud (cloud_in);
    extract.setIndices (inliers);
    extract.setNegative (false);
    extract.filter (*cloud_inlier);
    pcl::io::savePLYFileBinary<Point> ("/tmp/inliers.ply", *cloud_inlier);
  }
#endif

  return list_segment;
}

/**
 * project the cloud onto plane
 * @param cloud
 * @param plane
 * @return
 */
Cloud::Ptr
proj_plane (const Cloud::ConstPtr &cloud,
            const Eigen::Vector4f &plane)
{
  Cloud::Ptr projected (new Cloud ());
  pcl::ProjectInliers<Point> proj;
  pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());

  coefficients->values.resize (4);
  coefficients->values[0] = plane (0);
  coefficients->values[1] = plane (1);
  coefficients->values[2] = plane (2);
  coefficients->values[3] = plane (3);

  proj.setModelType (pcl::SACMODEL_PLANE);
  proj.setInputCloud (cloud);
  proj.setModelCoefficients (coefficients);
  proj.filter (*projected);

  return projected;
}

int
main (int argc, char **argv)
{
  const std::string fn_in (argv[1]);
  const float thres_dist = std::stof (std::string (argv[2]));
  const int thres_size = std::stoi (std::string (argv[3]));

  Cloud::Ptr cloud (new Cloud ());
  pcl::io::loadPLYFile<Point> (fn_in, *cloud);
  std::vector<Segment> list_segment = simple_planes_segment (cloud, thres_dist, thres_size);

//  pcl::io::savePLYFileBinary ("/tmp/mesh.ply", *result);

  return 0;
}
