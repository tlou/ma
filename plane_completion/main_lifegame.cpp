#include <iostream>
#include <cmath>
#include <vector>
#include <set>
#include <algorithm>
#include <sstream>

#include <Eigen/Dense>
#include <Eigen/StdVector>

#include <pcl/point_types.h>
#include <pcl/common/common_headers.h>
#include <pcl/io/ply_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/common/centroid.h>
#include <pcl/common/transforms.h>
#include <pcl/surface/concave_hull.h>
#include <pcl/surface/ear_clipping.h>
#include <pcl/features/normal_3d.h>
#include <pcl/segmentation/region_growing.h>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

typedef pcl::PointXYZRGBNormal Point;
typedef pcl::PointCloud<Point> Cloud;
typedef std::vector<int> Index;
typedef std::pair<Cloud::Ptr, Eigen::Vector4f> Segment;

/**
 * get transform from centroid to origin
 * @param cloud_in
 * @return
 */
Eigen::Matrix4f
get_transform (const Cloud::ConstPtr &cloud_in)
{
  Eigen::Matrix4f affine = Eigen::Matrix4f::Identity ();
  Eigen::Vector4f translate;
  Eigen::Matrix3f rotation;
  pcl::compute3DCentroid<Point> (*cloud_in, translate);
  affine.block (0, 3, 3, 1) << translate.segment (0, 3);
  return affine;
}

/**
 * merge point clouds from segments
 * @param segments
 * @return
 */
Cloud::Ptr
merge_cloud (const std::vector<Segment> &segments)
{
  Cloud::Ptr re (new Cloud ());

  for (const Segment &segment: segments)
  {
    *re += *segment.first;
  }
  return re;
}

/**
 * extract cloud with index
 * @param cloud
 * @param index
 * @return
 */
Cloud::Ptr
extract_cloud (const Cloud::ConstPtr &cloud, const Index &index)
{
  Cloud::Ptr sub_cloud (new Cloud ());
  pcl::PointIndices::Ptr sub_indice (new pcl::PointIndices ());
  pcl::ExtractIndices<Point> extract;

  sub_indice->indices = index;
  extract.setInputCloud (cloud);
  extract.setIndices (sub_indice);
  extract.setNegative (false);
  extract.filter (*sub_cloud);

  return sub_cloud;
}

/**
 * get random vector perpendicular to normal
 * @param normal
 * @return
 */
Eigen::Vector3f
get_rand_perpendicular (const Eigen::Vector3f &normal)
{
  Eigen::Vector3f result;
  if (std::fabs (normal (2)) < 0.25f)
  {
    result << normal (1), -normal (0), 0.0f;
  }
  else
  {
    result << 0.0f, normal (2), -normal (1);
  }
  return result / result.norm ();
}

/**
 * get the index of maxima point in direction
 * @param cloud
 * @param direction
 * @return
 */
int
get_id_maxima_direction (const Cloud::ConstPtr &cloud,
                         const Eigen::Vector3f &direction)
{
  Point center_pt;
  pcl::computeCentroid<Point, Point> (*cloud, center_pt);
  Eigen::Vector3f center (center_pt.getArray3fMap ());

  int result = 0;
  float max_dist = direction.dot (Eigen::Vector3f (cloud->front ().getArray3fMap ()) - center);
  for (size_t id = 1; id < cloud->size (); ++id)
  {
    float dist = direction.dot (Eigen::Vector3f (cloud->at (id).getArray3fMap ()) - center);
    if (dist > max_dist)
    {
      max_dist = dist;
      result = (int) id;
    }
  }

  return result;
}

/**
 * @deprecated
 * find the index of concave hull in point cloud
 * @param cloud
 * @param normal
 * @param r_outer maximal step
 * @param r_inner minimal step
 * @param max_concave_angle
 * @return
 */
Index
find_index_concave_hull (const Cloud::ConstPtr &cloud,
                         const Eigen::Vector3f &normal,
                         const float r_outer,
                         const float r_inner,
                         const float max_concave_angle)
{
  Index index;
  std::vector<bool> is_explored (cloud->size (), false);
  Eigen::Vector3f current_dir (get_rand_perpendicular (normal));
  pcl::KdTreeFLANN<Point> kdtree;
  const float r_inner_sqr = r_inner * r_inner;

  kdtree.setInputCloud (cloud);

  index.push_back (get_id_maxima_direction (cloud, current_dir));
  is_explored.at (index.back ()) = true;
  current_dir << normal.cross (current_dir);

  do
  {
    Index index_near;
    std::vector<float> dist_near;
    kdtree.radiusSearch (index.back (), r_outer, index_near, dist_near);
    if (index_near.empty ())
    {
      break;
    }

    int next_id = -1;
    float best_angle = (float) (M_PI * 2.0);
    const Eigen::Vector3f expect_dir (-normal.cross (current_dir));
    const Eigen::Vector3f pos (cloud->at (index.back ()).getArray3fMap ());

    for (size_t i = 0; i < index_near.size (); ++i)
    {
      const int id = index_near.at (i);
      if (dist_near.at (i) > r_inner_sqr)
      {
        Eigen::Vector3f from_pos = Eigen::Vector3f (cloud->at (id).getArray3fMap ()) - pos;
        float x = current_dir.dot (from_pos);
        float y = expect_dir.dot (from_pos);
        float angle = std::atan2 (y, x);

        if (angle < best_angle && angle > -max_concave_angle)
        {
          best_angle = angle;
          next_id = id;
        }
      }
    }

    if (next_id >= 0 && !is_explored.at (next_id))
    {
      index.push_back (next_id);
      current_dir << Eigen::Vector3f (cloud->at (next_id).getArray3fMap ()) - pos;
      current_dir.normalize ();
      is_explored.at (next_id) = true;
    }
    else
    {
      break;
    }
  } while (index.back () != index.front ());

  return index;
}

/**
 * get cloud instead of index, similar to last function
 * @param cloud
 * @param normal
 * @param r_outer
 * @param r_inner
 * @param max_concave_angle
 * @return
 */
Cloud::Ptr
find_concave_hull (const Cloud::ConstPtr &cloud,
                   const Eigen::Vector3f &normal,
                   const float r_outer,
                   const float r_inner,
                   const float max_concave_angle)
{
  return extract_cloud (cloud,
                        find_index_concave_hull (cloud, normal, r_outer,
                                                 r_inner, max_concave_angle));
}

/**
 * remove point with different normal vectors
 * @param cloud_in
 * @param plane
 * @param thres_angle
 * @return
 */
Cloud::Ptr
filter_cloud_angle (const Cloud::ConstPtr &cloud_in,
                    const Eigen::Vector4f &plane,
                    const float thres_angle)
{
  const Eigen::Vector3f normal = plane.segment (0, 3);
  const float cos_thres_angle = std::cos (thres_angle);

  Cloud::Ptr re (new Cloud ());
  re->reserve (cloud_in->size ());

  for (const Point &point: *cloud_in)
  {
    if (std::abs (point.getNormalVector3fMap ().dot (normal)) > cos_thres_angle)
    {
      re->push_back (point);
    }
  }
  return re;
}

/**
 * planes fitting with normal
 * @param cloud_in
 * @param thres_angle
 * @param thres_dist
 * @param thres_size minimal number of points on plane
 * @return
 */
std::vector<Segment>
segment_plane_normal (const Cloud::ConstPtr &cloud_in,
                      const double thres_angle,
                      const double thres_dist,
                      const int thres_size)
{
  std::vector<Segment> list_segment;
  pcl::SACSegmentationFromNormals<Point, Point> seg;
  pcl::ExtractIndices<Point> extract;
  Eigen::Matrix4f transform = get_transform (cloud_in);
  Cloud::Ptr cloud_tf (new Cloud ());

  pcl::transformPointCloud<Point> (*cloud_in, *cloud_tf,
                                   Eigen::Matrix4f (transform.inverse ()));

  seg.setOptimizeCoefficients (true);
  seg.setModelType (pcl::SACMODEL_PLANE);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setDistanceThreshold (thres_dist);
  seg.setNormalDistanceWeight (1.0f);

  for (int iter = 0; iter < 50; ++iter)
  {
    Eigen::Vector4f plane;
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
    pcl::PointIndicesPtr inliers (new pcl::PointIndices ());
    Cloud::Ptr cloud_inlier_ (new Cloud ());

    seg.setInputCloud (cloud_tf);
    seg.setInputNormals (cloud_tf);
    seg.segment (*inliers, *coefficients);

    if (cloud_tf->size () < thres_size || inliers->indices.size () < thres_size)
    {
      break;
    }

    plane << coefficients->values[0], coefficients->values[1],
      coefficients->values[2], coefficients->values[3];
    plane (3) -= transform.col (3).segment (0, 3).dot (plane.segment (0, 3));

    Cloud::Ptr cloud_left (new Cloud ());
    pcl::transformPointCloud<Point> (*cloud_tf, *cloud_left,
                                     Eigen::Matrix4f (transform));
    cloud_inlier_ = filter_cloud_angle (extract_cloud (cloud_left,
                                                       inliers->indices),
                                        plane,
                                        thres_angle);

    list_segment.push_back (Segment (cloud_inlier_, plane));

    extract.setInputCloud (cloud_tf);
    extract.setIndices (inliers);
    extract.setNegative (true);
    extract.filter (*cloud_tf);
  }

  return list_segment;
}

/**
 * @deprecated
 * group the point cloud, too slow
 * @param cloud
 * @param thres_dist
 * @param thres_size
 * @return
 */
std::vector<Cloud::Ptr>
split_cloud (const Cloud::ConstPtr &cloud,
             const float thres_dist,
             const size_t thres_size)
{
  pcl::KdTreeFLANN<Point> kdtree;
  std::vector<int> labels (cloud->size (), 0);
  std::vector<Cloud::Ptr> re;

  kdtree.setInputCloud (cloud);

  int current_label = 1;
  for (size_t id_point = 0; id_point < cloud->size (); ++id_point)
  {
    if (labels.at (id_point) <= 0)
    {
      std::set<int> to_search;
      to_search.insert ((int) id_point);
      while (!to_search.empty ())
      {
        const int id_this = *to_search.begin ();
        to_search.erase (to_search.begin ());

        std::vector<int> id_neighbors;
        std::vector<float> dist_sqr_neighbors;

        kdtree.radiusSearch (cloud->at (id_this), thres_dist, id_neighbors, dist_sqr_neighbors);

        for (size_t idid_neighbor = 0; idid_neighbor < id_neighbors.size (); ++idid_neighbor)
        {
          const int id_neighbor = id_neighbors.at (idid_neighbor);
          if (labels.at (id_neighbor) <= 0)
          {
            to_search.insert (id_neighbor);
          }
        }

        labels.at (id_this) = current_label;
      }

      ++current_label;
    }
  }

  for (int id_label = current_label - 1; id_label > 0; --id_label)
  {
    std::vector<int> id_this_labels;
    id_this_labels.reserve (labels.size ());
    for (size_t id = 0; id < labels.size (); ++id)
    {
      if (labels.at (id) == id_label)
      {
        id_this_labels.push_back ((int) id);
      }
    }

    if (id_this_labels.size () > thres_size)
    {
      re.push_back (extract_cloud (cloud, id_this_labels));
    }
  }
  return re;
}

/**
 * @deprecated
 * split the point cloud further (from the point cloud splitted by planes fitting)
 * by separating groups on plane
 * @param segments
 * @param thres_dist
 * @param thres_size
 * @return
 */
std::vector<Segment>
split_segments (const std::vector<Segment> &segments,
                const float thres_dist,
                const size_t thres_size)
{
  std::vector<Segment> re;
  re.reserve (segments.size () * 2);

  for (const Segment &segment: segments)
  {
    std::vector<Cloud::Ptr> clouds = split_cloud (segment.first,
                                                  thres_dist,
                                                  thres_size);
    for (const Cloud::Ptr &cloud: clouds)
    {
      re.push_back (Segment (cloud, segment.second));
    }
  }

  return re;
}

/**
 * remove point with different normal vectors
 * @param segments point clouds
 * @param normal
 * @param thres_angle
 * @return
 */
std::vector<Segment>
filter_different_normal (const std::vector<Segment> &segments,
                         const Eigen::Vector3f &normal,
                         const float &thres_angle)
{
  const float thres_cos_angle = std::cos (thres_angle);
  std::vector<Segment> re;
  re.reserve (segments.size ());
  for (const Segment &segment: segments)
  {
    if (std::abs (normal.dot (segment.second.segment (0, 3))) > thres_cos_angle)
    {
      re.push_back (segment);
    }
  }
  return re;
}

/**
 * get the distribution map of point cloud on plane
 * @param cloud
 * @param resolution
 * @param u_dir
 * @param v_dir
 * @param px_max maximal pixel (2d index)
 * @param px_min minimal pixel (2d index)
 * @param boundary
 * @return
 */
cv::Mat
get_map (const Cloud::ConstPtr &cloud, const float resolution,
         const Eigen::Vector3f &u_dir, const Eigen::Vector3f &v_dir,
         const Eigen::Vector2f &px_max, const Eigen::Vector2f &px_min,
         const float boundary)
{
  cv::Mat map ((int) std::ceil (px_max (0) - px_min (0)) / resolution,
               (int) std::ceil (px_max (1) - px_min (1)) / resolution,
               CV_32FC1);
  map.setTo (0.0f);
  for (const Point &point: *cloud)
  {
    Eigen::Vector2f coordinate (point.getVector3fMap ().dot (u_dir),
                                point.getVector3fMap ().dot (v_dir));
    coordinate -= px_min;
    coordinate += boundary * Eigen::Vector2f (1.0f, 1.0f);
    coordinate /= resolution;
    coordinate += 0.5f * Eigen::Vector2f (1.0f, 1.0f);

    map.at<float> ((int) coordinate (0), (int) coordinate (1)) = 1.0f;
  }
  return map;
}

/**
 * regenerate point cloud with distribution map
 * @param cloud_original
 * @param map
 * @param u_dir
 * @param v_dir
 * @param plane
 * @param base minimal pixel (2d index)
 * @param resolution
 * @param boundary
 * @return
 */
Cloud::Ptr
demap (const Cloud::ConstPtr &cloud_original, const cv::Mat &map,
       const Eigen::Vector3f &u_dir, const Eigen::Vector3f &v_dir,
       const Eigen::Vector4f &plane, const Eigen::Vector2f &base,
       const float resolution, const float boundary)
{
  const Eigen::Vector3f normal = plane.segment (0, 3);
  const float plane_d = plane (3);
  Cloud::Ptr re (new Cloud ());
  re->reserve (cv::countNonZero (map));

  pcl::KdTreeFLANN<Point> kdtree;
  kdtree.setInputCloud (cloud_original);

  for (int r = 0; r < map.size ().height; ++r)
  {
    for (int c = 0; c < map.size ().width; ++c)
    {
      if (map.at<float> (r, c) > 0.1f)
      {
        std::vector<int> index_neighbors;
        std::vector<float> dist_sqr_neighbors;
        const Eigen::Vector2f px = base + Eigen::Vector2f (r, c) * resolution
                                   - boundary * Eigen::Vector2f (1.0f, 1.0);;
        const Eigen::Vector3f pos = u_dir * px (0) + v_dir * px (1);
        Point point;
        point.getVector3fMap () = pos - normal * (pos.dot (normal) + plane_d);

        if (kdtree.nearestKSearch (point, 1, index_neighbors, dist_sqr_neighbors))
        {
          const Point &point_nearest = cloud_original->at (index_neighbors.front ());
          point.getNormalVector3fMap () = point_nearest.getNormalVector3fMap ();
          point.rgba = point_nearest.rgba;
        }
        else
        {
          point.getNormalVector3fMap () = normal;
        }
        re->push_back (point);
      }
    }
  }

  return re;
}

/**
 * density evolvement for once
 * @param map
 * @return
 */
cv::Mat
evolve_once (const cv::Mat &map)
{
  const int size = 3;
  cv::Mat kernel (size, size, CV_32FC1);
  cv::Mat re = map.clone ();
  kernel.setTo (1.0f / (float) kernel.total ());
  cv::filter2D (map, re, -1, kernel);
  cv::threshold (re, re, 0.4f, 1.0f, cv::THRESH_BINARY);
  re.setTo (1.0f, map > re);
  return re;
}

/**
 * density evolvement until convergence
 * @param map
 * @return
 */
cv::Mat
evolve (const cv::Mat &map)
{
  cv::Mat re = map.clone ();

  {
    const int size = 3;
    cv::Mat kernel (size, size, CV_32FC1);
    kernel.setTo (1.0f / (float) kernel.total ());
    cv::filter2D (re, re, -1, kernel);
    cv::threshold (re, re, 0.01f, 1.0f, cv::THRESH_BINARY);
  }

  for (size_t iter = 0; iter < 5000; ++iter)
  {
    cv::Mat re_ = evolve_once (re);
    if (cv::countNonZero (cv::abs (re_ - re) > 1e-6f) == 0)
    {
      break;
    }
    re = re_;
  }

  {
    const int size = 3;
    cv::Mat kernel (size, size, CV_32FC1);
    kernel.setTo (1.0f / (float) kernel.total ());
    cv::filter2D (re, re, -1, kernel);
    re.setTo (0.0, re < 0.4f);
    re.setTo (1.0, re > 0.3f);
  }

  return re;
}

/**
 * load the result of active contour from Matlab (computed externally)
 * @param map
 * @return
 */
cv::Mat
hack_active_contour (const cv::Mat &map)
{
  cv::Mat re;
  static int count = 0;
  std::stringstream ss;
  // my directory for storing and processing the distribution with Matlab
  ss << "/home/tlou/workspace/testfield/plane_complete_contour/bw" << count << ".png";
  ++count;
  cv::imread (ss.str (), 0).convertTo (re, CV_32FC1);
  return re / 255.0f;
}

/**
 * densify the point cloud on plane with distribution on plane
 * @param cloud_in
 * @param plane
 * @param resolution
 * @param method "evolve" or "contour"
 * @return
 */
Cloud::Ptr
densify (const Cloud::ConstPtr &cloud_in, const Eigen::Vector4f &plane,
         const float resolution, const std::string &method)
{
  const Eigen::Vector3f normal = plane.segment (0, 3);
  const float plane_d = plane (3);
  Cloud::Ptr cloud (new Cloud ());
  cloud->reserve (cloud_in->size ());

  for (const Point &point: *cloud_in)
  {
    Point point_ = point;
    point_.getVector3fMap () -= normal * point_.getVector3fMap ().dot (normal);
    cloud->push_back (point_);
  }

  const Eigen::Vector3f u_dir = (Eigen::Vector3f (1.0f, 0.0f, 0.0f) - normal (0) * normal).normalized ();
  const Eigen::Vector3f v_dir = normal.dot (Eigen::Vector3f (0.0f, 0.0f, 1.0f)) ?
                                normal.cross (u_dir) : u_dir.cross (v_dir);

  Point pt_max, pt_min;
  pcl::getMinMax3D (*cloud, pt_min, pt_max);
  const float boundary = 0.1f;
  const Eigen::Vector2f px_min (pt_min.getVector3fMap ().dot (u_dir) - boundary,
                                pt_min.getVector3fMap ().dot (v_dir) - boundary);
  const Eigen::Vector2f px_max (pt_max.getVector3fMap ().dot (u_dir) + boundary,
                                pt_max.getVector3fMap ().dot (v_dir) + boundary);

  cv::Mat map = get_map (cloud, resolution,
                         u_dir, v_dir,
                         px_max, px_min, boundary);

  static int count = 0;
  std::stringstream ss0;
  ss0 << "/tmp/map_" << count << "_0.png";
  cv::imwrite (ss0.str (), map * 255.0f);

  if (method == "contour")
  {
    map = hack_active_contour (map);
  }
  else
  {
    map = evolve (map);
  }

  std::stringstream ss1;
  ss1 << "/tmp/map_" << count << "_1.png";
  cv::imwrite (ss1.str (), map * 255.0f);
  ++count;

  return demap (cloud_in, map, u_dir, v_dir,
                plane, px_min, resolution, boundary);
}

/**
 * remove points in segments
 * @param cloud
 * @param segments
 * @param thres_dist
 * @return
 */
Cloud::Ptr
filter_point_segments (const Cloud::ConstPtr &cloud,
                       const std::vector<Segment> &segments,
                       const float thres_dist)
{
  Cloud::Ptr re (new Cloud ());
  std::vector<bool> is_on (cloud->size (), false);
  pcl::KdTreeFLANN<Point> kdtree;

  kdtree.setInputCloud (cloud);
  re->reserve (cloud->size ());
  for (const Segment &segment: segments)
  {
    for (const Point &point: *segment.first)
    {
      std::vector<int> id_neighbors;
      std::vector<float> dist_sqr_neighbors;

      kdtree.radiusSearch (point, thres_dist, id_neighbors, dist_sqr_neighbors);

      for (const int id: id_neighbors)
      {
        is_on.at (id) = true;
      }
    }
  }

  for (size_t id = 0; id < is_on.size (); ++id)
  {
    if (!is_on.at (id))
    {
      re->push_back (cloud->at (id));
    }
  }
  return re;
}

void
print_help ()
{
  std::cout << "./plane_completion_lifegame path_input_cloud_ply resolution method(evolve or contour)"
            << std::endl;
}

int
main (int argn, char **argv)
{
  if (argn <= 1)
  {
    print_help ();
    return 1;
  }
  const std::string fn_in = argv[1];
  const float resolution = std::stof (argv[2]);
  const std::string method = argv[3];

  const float thres_dist = 0.01f;
  const size_t thres_size = 10240;

  Cloud::Ptr cloud_in (new Cloud ());

  pcl::io::loadPLYFile<Point> (fn_in, *cloud_in);

  std::vector<Segment> segments = segment_plane_normal (cloud_in, M_PI / 18.0,
                                                        thres_dist, thres_size);

  segments = filter_different_normal (segments,
                                      Eigen::Vector3f (0.0f, 0.0f, 1.0f),
                                      (float) M_PI / 6.0);

  Cloud::Ptr cloud_out (new Cloud ());
  for (const Segment &segment: segments)
  {
    *cloud_out += *densify (segment.first, segment.second, resolution, method);
  }

  *cloud_out += *filter_point_segments (cloud_in, segments, 1e-3f);

  pcl::io::savePLYFileBinary ("/tmp/cloud.ply", *cloud_out);

  //for (const Segment segment: segments)
  //  std::cout << segment.first->size () << std::endl;
  //std::cout << std::endl;

  //pcl::io::savePLYFileBinary ("/tmp/inliers0.ply", *merge_cloud (segments));

  // segments = split_segments (segments, 0.05, 10240);

  //for (const Segment segment: segments)
  //  std::cout << segment.first->size () << std::endl;

  //pcl::io::savePLYFileBinary ("/tmp/inliers1.ply", *merge_cloud (segments));

  return 0;
}
