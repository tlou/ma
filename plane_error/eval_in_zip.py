import os
import sys

PATH_ZIP = sys.argv[1]
PATH_DATA = sys.argv[2]
PATH_LOG = sys.argv[3]
THRES_DIST = float(sys.argv[4])
PLANE_PARAM = sys.argv[5] # in text

PATH_EXE = os.getenv('HOME') + "/workspace/ma/plane_error/build/plane_error"

def exist_or_quit(path):
    if not os.path.isfile(path):
        print path + " not found"
        sys.exit()

exist_or_quit(PATH_ZIP)
os.system("unzip " + PATH_ZIP + " " + PATH_DATA + " -d /tmp/ > /dev/null")
path_extracted = "/tmp/" + PATH_DATA
#exist_or_quit(path_extracted)
print PATH_EXE + " " + path_extracted + " " + PATH_LOG + " {} ".format(THRES_DIST) + PLANE_PARAM
os.system(PATH_EXE + " " + path_extracted + " " + PATH_LOG + " {} ".format(THRES_DIST) + PLANE_PARAM)

if os.path.isfile(path_extracted):
    os.remove(path_extracted)
