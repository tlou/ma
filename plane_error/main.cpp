#include <map>
#include <vector>
#include <set>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
#include <sys/stat.h>

#include <pcl/common/common_headers.h>
#include <pcl/PolygonMesh.h>
#include <pcl/io/ply_io.h>

typedef pcl::PointXYZRGBNormal Point;
typedef pcl::PointCloud<Point> Cloud;

/**
 *
 * @param filename
 * @return
 */
bool
is_file_exist (const std::string &filename)
{
  struct stat buffer;
  return (stat (filename.c_str (), &buffer) == 0);
}

/**
 *
 * @param cloud_in
 * @param plane
 * @param threshold
 * @return
 */
std::vector<bool>
get_is_points_on_plane (const Cloud::ConstPtr &cloud_in,
                        const Eigen::Vector4f &plane,
                        const float threshold)
{
  std::vector<bool> is_points_on_plane (cloud_in->size (), false);
  const Eigen::Vector3f normal = plane.segment (0, 3);
  const float plane_d = plane (3);

  std::vector<Point, Eigen::aligned_allocator<Point> >::const_iterator it
    = cloud_in->points.begin ();
  std::generate (is_points_on_plane.begin (), is_points_on_plane.end (),
                 [&it, normal, plane_d, threshold]
                 {
                   return std::abs ((it++)->getVector3fMap ().dot (normal) + plane_d) < threshold;
                 });

  return is_points_on_plane;
}

/**
 * get the statistics of the inliers, distance, variance of distance and number of vertices
 * @param cloud_in
 * @param plane
 * @param is_points_on_plane
 * @param mean mean of absolute distance
 * @param cov variance of distance
 * @param num_vertex
 */
void
compute_stat_inlier (const Cloud::ConstPtr &cloud_in,
                     const Eigen::Vector4f &plane,
                     const std::vector<bool> &is_points_on_plane,
                     float &mean,
                     float &cov,
                     size_t &num_vertex)
{
  float sum_d2 = 0.0f;
  float sum_d = 0.0f;
  float sum_d_abs = 0.0f;
  size_t count = 0;

  const Eigen::Vector3f normal = plane.segment (0, 3);
  const float plane_d = plane (3);

  for (size_t id = 0; id < cloud_in->size (); ++id)
  {
    if (is_points_on_plane.at (id))
    {
      const float dist = cloud_in->at (id).getVector3fMap ().dot (normal) + plane_d;

      sum_d += dist;
      sum_d_abs += std::abs (dist);
      sum_d2 += dist * dist;
      ++count;
    }
  }

  mean = sum_d / (float) count;
  cov = sum_d2 / (float) count - mean * mean;
  mean = sum_d_abs / (float) count; // absolute value makes more sense

  num_vertex = count;
}

/**
 *
 * @param cloud
 * @param id_vertices
 * @return
 */
float
get_triangle_area (const Cloud::ConstPtr &cloud,
                   const std::vector<uint32_t> &id_vertices)
{
  assert(id_vertices.size () == 3);
  const Eigen::Vector3f pos0 = cloud->at (id_vertices.at (0)).getVector3fMap ();
  const Eigen::Vector3f pos1 = cloud->at (id_vertices.at (1)).getVector3fMap ();
  const Eigen::Vector3f pos2 = cloud->at (id_vertices.at (2)).getVector3fMap ();
  const float a = (pos0 - pos1).norm ();
  const float b = (pos1 - pos2).norm ();
  const float c = (pos2 - pos0).norm ();
  const float s = (a + b + c) / 2.0f;
  return std::sqrt (std::abs (s * (s - a) * (s - b) * (s - c)));
}

/**
 *
 * @param cloud
 * @param mesh
 * @param is_points_on_plane
 * @param num_triangles
 * @return
 */
float
get_area_on_plane (const Cloud::ConstPtr &cloud,
                   const std::vector<pcl::Vertices> &mesh,
                   const std::vector<bool> &is_points_on_plane,
                   size_t &num_triangles)
{
  float area = 0.0f;
  size_t count = 0;

  for (const pcl::Vertices &vertice: mesh)
  {
    bool is_on = true;
    for (const uint32_t id_point: vertice.vertices)
    {
      is_on &= is_points_on_plane.at (id_point);
    }

    if (is_on)
    {
      const float area_ = get_triangle_area (cloud, vertice.vertices);
      if (!std::isnan (area_))
      {
        area += area_;
        ++count;
      }
    }
  }

  num_triangles = count;

  return area;
}

/**
 *
 * @param cloud
 * @param mesh
 * @param is_points_on_plane
 * @return
 */
float
get_length_boundary (const Cloud::ConstPtr &cloud,
                     const std::vector<pcl::Vertices> &mesh,
                     const std::vector<bool> &is_points_on_plane)
{
  typedef std::pair<uint32_t, uint32_t> Edge;
  typedef std::map<Edge, int> CountEdge;

  float length = 0.0f;
  CountEdge edge_occurance;
  std::vector<size_t> id_triangles_on;

  // find the open boundaries, which is edge of a triangle on plane but only occur once
  id_triangles_on.reserve (mesh.size ());
  for (size_t id_ver = 0; id_ver < mesh.size (); ++id_ver)
  {
    const pcl::Vertices &vertice = mesh.at (id_ver);
    bool is_on = true;
    for (const uint32_t id_point: vertice.vertices)
    {
      is_on &= is_points_on_plane.at (id_point);
    }

    if (is_on)
    {
      id_triangles_on.push_back (id_ver);
    }

    {
      for (size_t id = 0; id < vertice.vertices.size (); ++id)
      {
        const uint32_t id0 = vertice.vertices.at (id);
        const uint32_t id1 = vertice.vertices.at ((id + 1) % vertice.vertices.size ());

        if (edge_occurance.find (Edge (id0, id1)) != edge_occurance.end ())
        {
          ++edge_occurance[Edge (id0, id1)];
        }
        else if (edge_occurance.find (Edge (id1, id0)) != edge_occurance.end ())
        {
          ++edge_occurance[Edge (id1, id0)];
        }
        else
        {
          edge_occurance[Edge (id0, id1)] = 1;
        }
      }
    }
  }

  // get total length of open boundary
  for (const size_t id_ver: id_triangles_on)
  {
    const pcl::Vertices &vertice = mesh.at (id_ver);
    for (size_t id = 0; id < vertice.vertices.size (); ++id)
    {
      const uint32_t id0 = vertice.vertices.at (id);
      const uint32_t id1 = vertice.vertices.at ((id + 1) % vertice.vertices.size ());

      if ((edge_occurance.find (Edge (id0, id1)) != edge_occurance.end ()
           && edge_occurance[Edge (id0, id1)] == 1)
          || (edge_occurance.find (Edge (id1, id0)) != edge_occurance.end ()
              && edge_occurance[Edge (id1, id0)] == 1))
      {
        float length_ = (cloud->at (id0).getVector3fMap () - cloud->at (id1).getVector3fMap ()).norm ();
        if (!std::isnan (length_))
        {
          length += length_;
        }
      }
    }
  }

  return length;
}

void
print_help ()
{
  std::cout << "./plane_error path_input_mesh path_output_log threshold_distance_on_plane "
            << "plane_a plane_b plane_c plane_d (ax+by+cz+d=0 are points on plane)"
            << std::endl;
}

int
main (int argc, char **argv)
{
  if (argc <= 1)
  {
    print_help ();
    return 1;
  }
  const std::string fn_in (argv[1]);
  const std::string fn_log (argv[2]);
  const float threshold = std::stof (std::string (argv[3]));
  const float plane_a = std::stof (std::string (argv[4]));
  const float plane_b = std::stof (std::string (argv[5]));
  const float plane_c = std::stof (std::string (argv[6]));
  const float plane_d = std::stof (std::string (argv[7]));

  float mean = -1.0f, cov = -1.0f;
  float area = -1.0f, length_boundary = -1.0f;
  size_t num_vertex = 0;
  size_t num_triangles = 0;
  const Eigen::Vector4f plane_param (plane_a, plane_b, plane_c, plane_d);

  if (is_file_exist (fn_in))
  {
    pcl::PolygonMesh::Ptr mesh (new pcl::PolygonMesh ());
    Cloud::Ptr cloud (new Cloud ());
    pcl::io::loadPLYFile (fn_in, *mesh);
    pcl::fromPCLPointCloud2 (mesh->cloud, *cloud);

    std::vector<bool> is_points_on_plane = get_is_points_on_plane (cloud, plane_param, threshold);

    compute_stat_inlier (cloud, plane_param, is_points_on_plane, mean, cov, num_vertex);

    area = get_area_on_plane (cloud, mesh->polygons, is_points_on_plane, num_triangles);

    length_boundary = get_length_boundary (cloud, mesh->polygons, is_points_on_plane);

//    std::cout << area << " " << length_boundary << std::endl;
  }

  std::ofstream os;
  os.open (fn_log, std::ios_base::app);
  // print also number of inliers and plane parameters for debug
  os << mean << " ";
  os << cov << " ";
  os << num_vertex << " ";
  os << num_triangles << " ";
  os << area << " ";
  os << length_boundary << " ";
  os << plane_param (0) << " " << plane_param (1) << " "
     << plane_param (2) << " " << plane_param (3) << std::endl;
  os.close ();

  return 0;
}

