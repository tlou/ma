#include <iostream>
#include <string>
#include <fstream>

#include <Eigen/Geometry>

#include <pcl/common/common_headers.h>
#include <pcl/filters/passthrough.h>
#include <pcl/io/ply_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/common/centroid.h>
#include <pcl/common/transforms.h>

typedef pcl::PointXYZRGBNormal Point;
typedef pcl::PointCloud<Point> Cloud;

Cloud::Ptr load_ply(const std::string &filename)
{
  pcl::PCLPointCloud2::Ptr cloud_in(new pcl::PCLPointCloud2());
  Cloud::Ptr cloud(new Cloud());
  pcl::PLYReader reader;
  reader.read(filename, *cloud_in);
  pcl::fromPCLPointCloud2(*cloud_in, *cloud);
  return cloud;
}

Cloud::Ptr remove_ceil_floor(const Cloud::ConstPtr &cloud_in, 
                             const float z_ceil, const float z_floor)
{
  Cloud::Ptr cloud_out(new Cloud());
  cloud_out->reserve(cloud_in->size());
  for(const Point &pt : *cloud_in)
  {
    if(pt.z < z_ceil && pt.z > z_floor)
    {
      cloud_out->push_back(pt);
    }
  }
  std::cout << "filtered from " << cloud_in->size() << " to " 
            << cloud_out->size() << std::endl;
  return cloud_out;
}

Eigen::Matrix4f getTransform(const Cloud::ConstPtr &cloud_in)
{
  Eigen::Matrix4f affine = Eigen::Matrix4f::Identity();
  Eigen::Vector4f translate;
  Eigen::Matrix3f rotation;
  pcl::compute3DCentroid<Point>(*cloud_in, translate);
  //  pcl::computeCovarianceMatrix<Point>(*cloud_in, rotation);

  //  affine.fill(0.0f);
  //  affine.block(0, 0, 3, 3) << rotation;
  affine.block(0, 3, 3, 1) << translate.segment(0, 3);
  //  affine(3, 3) = 1.0f;
  return affine;
}

Eigen::Vector4f simple_plane_segment(const Cloud::ConstPtr &cloud_in,
                                     const float threshold,
                                     const Eigen::Vector3f &axis = Eigen::Vector3f::Zero())
{
  pcl::SACSegmentation<Point> seg;
  pcl::PointIndicesPtr inliers(new pcl::PointIndices());
  pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients());
  Eigen::Vector4f plane;
  Eigen::Matrix4f transform = getTransform(cloud_in);
  Cloud::Ptr cloud_tf(new Cloud());

  pcl::transformPointCloud<Point>(*cloud_in, *cloud_tf, Eigen::Matrix4f(transform.inverse()));

  seg.setOptimizeCoefficients(true);
  seg.setModelType(pcl::SACMODEL_PLANE);
  seg.setMethodType(pcl::SAC_RANSAC);
  seg.setDistanceThreshold(threshold);
  if(axis.norm() > 0.5f)
  {
    seg.setAxis(axis);
    seg.setEpsAngle(M_PI / 4.0);
  }
  seg.setInputCloud(cloud_tf);
  seg.segment(*inliers, *coefficients);

  plane << coefficients->values[0], coefficients->values[1],
      coefficients->values[2], coefficients->values[3];


  std::cout << plane(0) << " " << plane(1) << " "
            << plane(2) << " " << plane(3) << std::endl;
  plane(3) -= transform.col(3).segment(0, 3).dot(plane.segment(0, 3));
  std::cout << plane(0) << " " << plane(1) << " "
            << plane(2) << " " << plane(3) << std::endl;

  std::cout << inliers->indices.size() << " inliers from "
            << cloud_in->size() << " points found\n";

  return plane;
}

int main(int argc, char **argv)
{
  const std::string fn_in(argv[1]);
  const float threshold = std::stof(std::string(argv[2]));
  const std::string fn_log(argv[3]);

  Cloud::Ptr cloud(new Cloud());
  pcl::io::loadPLYFile<Point>(fn_in, *cloud);
  //cloud = remove_ceil_floor(cloud, 0.8f, 0.6f);
  //const Eigen::Vector4f plane_param(simple_plane_segment(cloud, threshold, Eigen::Vector3f(0,0,1)));
  const Eigen::Vector4f plane_param(simple_plane_segment(cloud, threshold));

  std::cout << plane_param(0) << " " << plane_param(1) << " "
            << plane_param(2) << " " << plane_param(3) << std::endl;

  std::ofstream out;
  out.open(fn_log, std::ios_base::app);
  if(out.is_open())
  {
    //out << plane_param(0) << std::endl << plane_param(1) << std::endl
    //    << plane_param(2) << std::endl << plane_param(3) << std::endl;
    out << plane_param(0) << " " << plane_param(1) << " "
        << plane_param(2) << " " << plane_param(3) << std::endl;
    out.close();
  }

  return 0;
}
