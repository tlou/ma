import os
import sys

PATH_ZIP = sys.argv[1]
PATH_DATA = sys.argv[2]
PATH_LOG = sys.argv[3]
THRES_DIST = float(sys.argv[4])

PATH_EXE = os.getenv('HOME') + "/workspace/ma/plane_error/build/plane_find"

def exist_or_quit(path):
    if not os.path.isfile(path):
        print path + " not found"
        sys.exit()

exist_or_quit(PATH_ZIP)
os.system("unzip " + PATH_ZIP + " " + PATH_DATA + " -d /tmp/ > /dev/null")
path_extracted = "/tmp/" + PATH_DATA
exist_or_quit(path_extracted)
print PATH_EXE + " " + path_extracted + " {} ".format(THRES_DIST) + PATH_LOG
os.system(PATH_EXE + " " + path_extracted + " {} ".format(THRES_DIST) + PATH_LOG)
#os.system(PATH_EXE + " " + path_extracted + " {}".format(THRES_DIST))

os.remove(path_extracted)
