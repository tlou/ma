#! /bin/python2

import os
import sys
import time

SIZE = float(sys.argv[1])
LEVEL = int(sys.argv[2])
LEV_TRIM = int(sys.argv[3])
SCENE_ID = sys.argv[4]

HOME = os.getenv('HOME')
_SIZE_INT = int(round(SIZE * 2.0))
STR_SIZE = "{}_{}".format(_SIZE_INT / 2, (_SIZE_INT % 2) * 5)
SUBNAME = STR_SIZE + "_{}".format(LEVEL)
DIR_SRC = HOME + "/workspace/navvis_data/" + SCENE_ID + "/sub{}".format(STR_SIZE)
DIR_BASE = HOME + "/workspace/navvis_data/" + SCENE_ID + "/poisson"
DIR_RECON = DIR_BASE + "/origin_" + SUBNAME
DIR_TRIM = DIR_BASE + "/trim_" + SUBNAME + "_{}".format(LEV_TRIM)
EXE_RECON = HOME + "/workspace/PoissonRecon/PoissonRecon/Bin/Linux/SSDRecon"
EXE_TRIM = HOME + "/workspace/PoissonRecon/PoissonRecon/Bin/Linux/SurfaceTrimmer"

if not os.path.isdir(DIR_SRC):
    print "src dir not found"
    sys.exit()

if not os.path.isfile(EXE_RECON) or not os.path.isfile(EXE_TRIM):
    print "exe not found"
    sys.exit()

os.system("rm -rf " + DIR_RECON)
os.system("rm -rf " + DIR_TRIM)
os.system("mkdir -p " + DIR_RECON)
os.system("mkdir -p " + DIR_TRIM)

list_fn = tuple([fn for fn in os.listdir(DIR_SRC) if ".ply" in fn])
time_start = time.time()
for fn in list_fn:
    command = EXE_RECON + " --in " + DIR_SRC + "/" + fn \
            + " --out " + DIR_RECON + "/" + fn \
            + " --depth {} --color 16 --density".format(LEVEL)
    os.system(command)

time_end = time.time()
with open(DIR_RECON + "/log", 'w') as out:
    out.write("running time is {}, from {} to {}".format(\
            time_end - time_start, time_start, time_end))

time_start = time.time()

for fn in list_fn:
    command = EXE_TRIM + " --in " + DIR_RECON + "/" + fn + \
            " --out " + DIR_TRIM + "/" + fn + " --trim {}".format(LEV_TRIM)
    #os.system(command)

time_end = time.time()
with open(DIR_TRIM + "/log", 'w') as out:
    out.write("running time is {}, from {} to {}".format(\
            time_end - time_start, time_start, time_end))


