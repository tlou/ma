import sys
import os
import time

DIR_IN = sys.argv[1]
DIR_OUT = sys.argv[2]
TRIM_LEVEL = int(sys.argv[3])

EXE = os.getenv('HOME') + "/workspace/PoissonRecon/PoissonRecon/Bin/Linux/SurfaceTrimmer"

if not os.path.isdir(DIR_IN):
    print "dir input not found"
    sys.exit()

if not os.path.isfile(EXE):
    print "exe not found"
    sys.exit()

if not os.path.isdir(DIR_OUT):
    os.makedirs(DIR_OUT)

time_start = time.time()

list_fn = tuple([fn for fn in os.listdir(DIR_IN) if ".ply" in fn])
for fn in list_fn:
    command = EXE + " --in " + DIR_IN + "/" + fn + " --out " + DIR_OUT + "/" + fn \
            + " --trim {}".format(TRIM_LEVEL)
    print command
    os.system(command)

time_end = time.time()
with open(DIR_OUT + "/log", 'w') as out:
    out.write("running time is {}, from {} to {}".format(\
            time_end - time_start, time_start, time_end))

