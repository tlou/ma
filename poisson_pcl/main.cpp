#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <sstream>

#include <Eigen/Dense>

#include <pcl/common/common_headers.h>
#include <pcl/filters/passthrough.h>
#include <pcl/io/ply_io.h>
#include <pcl/surface/poisson.h>

//#define IS_PLY_READ_AVAILABLE

typedef pcl::PointXYZRGBNormal Point;
typedef pcl::PointCloud<Point> Cloud;

/**
 *
 * @param string
 * @param key
 * @return
 */
std::vector<std::string>
parse_string (const std::string &string, const std::string &key)
{
  std::vector<std::string> result;
  std::string copy = string;
  size_t pos = copy.find (key);
  while (!copy.empty () && pos != std::string::npos)
  {
    result.push_back (copy.substr (0, pos));
    copy = copy.substr (pos + 1);
    pos = copy.find (key);
  }
  if (!copy.empty ())
  {
    result.push_back (copy);
  }
  return result;
}

/**
 *
 * @param filename
 * @return
 */
std::string
get_filename_no_suffix (const std::string &filename)
{
  size_t pos = filename.find_last_of (".");
  if (pos != std::string::npos)
  {
    return filename.substr (0, pos);
  }
  else
  {
    return filename;
  }
}

void
parse_filename (const std::string &filename,
                float &center_x, float &center_y, float &size)
{
  std::vector<std::string> params = parse_string (
    get_filename_no_suffix (parse_string (filename, "/").back ()), "_");
  if (params.size () == 3)
  {
    center_x = std::stof (params.at (0));
    center_y = std::stof (params.at (1));
    size = std::stof (params.at (2));
  }
}

template<class T>
typename pcl::search::KdTree<T>::Ptr
get_search_kdtree (const typename pcl::PointCloud<T>::ConstPtr &cloud)
{
  typename pcl::search::KdTree<T>::Ptr kdtree (new pcl::search::KdTree<T>);
  kdtree->setInputCloud (cloud);
  return kdtree;
}

template<class T>
pcl::PolygonMesh::Ptr
trianglulate_mesh_poisson (const typename pcl::PointCloud<T>::ConstPtr &cloud,
                           const typename pcl::search::KdTree<T>::Ptr &kdtree,
                           const int depth)
{
  pcl::PolygonMesh::Ptr mesh (new pcl::PolygonMesh ());
  typename pcl::Poisson<T> poisson;
  poisson.setDepth (depth);
  poisson.setInputCloud (cloud);
  poisson.setSearchMethod (kdtree);
  poisson.reconstruct (*mesh);
  return mesh;
}

std::vector<int>
get_near_point_index (const pcl::PolygonMesh::ConstPtr &mesh,
                      const pcl::search::KdTree<Point>::ConstPtr &kdtree,
                      const float threshold_distance)
{
  const size_t length_cloud = mesh->cloud.height * mesh->cloud.width;
  Cloud cloud_explicit;
  pcl::fromPCLPointCloud2 (mesh->cloud, cloud_explicit);
  std::vector<int> index;
  index.reserve (cloud_explicit.size ());

  std::vector<int> k_indices (1, 0);
  std::vector<float> k_sqr_distances (1, 0.0f);
  for (size_t id = 0; id < cloud_explicit.size (); ++id)
  {
    kdtree->nearestKSearch (cloud_explicit.at (id), 1, k_indices, k_sqr_distances);
    if (k_sqr_distances.front () < threshold_distance)
    {
      index.push_back ((int) id);
    }
  }

  return index;
}


std::vector<int>
get_vertex_index (const std::vector<pcl::Vertices> &vertices,
                  const std::vector<int> &index_mesh, const size_t num_vertex)
{
  std::vector<bool> is_in (num_vertex, false);
  std::vector<int> index;
  index.reserve (num_vertex);

  for (const int id_mesh : index_mesh)
  {
    const pcl::Vertices &triangle = vertices.at (id_mesh);
    for (const uint32_t &id : triangle.vertices)
    {
      is_in.at (id) = true;
    }
  }

  for (size_t id = 0; id < num_vertex; ++id)
  {
    if (is_in.at (id))
    {
      index.push_back ((int) id);
    }
  }

  return index;
}

std::vector<int>
get_involved_mesh_index (const std::vector<pcl::Vertices> &vertices,
                         const std::vector<int> &index_vertex, const size_t num_point)
{
  std::vector<int> count (vertices.size (), 0);
  std::vector<bool> is_in (num_point, false);
  std::vector<int> index;
  index.reserve (vertices.size ());

  for (const int id : index_vertex)
  {
    is_in.at (id) = true;
  }

  for (size_t id_mesh = 0; id_mesh < vertices.size (); ++id_mesh)
  {
    const pcl::Vertices &triangle = vertices.at (id_mesh);
    for (const uint32_t &id : triangle.vertices)
    {
      if (is_in.at (id))
      {
        ++count.at (id_mesh);
      }
    }
  }

  for (size_t id_mesh = 0; id_mesh < vertices.size (); ++id_mesh)
  {
    if (count.at (id_mesh) > 1)
    {
      index.push_back ((int) id_mesh);
    }
  }

  return index;
}

std::vector<uint32_t>
get_mapping_point (const std::vector<int> &index_point,
                   const size_t size_cloud)
{
  std::vector<uint32_t> result;
  result.resize (size_cloud);

  for (size_t id = 0; id < index_point.size (); ++id)
  {
    result.at (index_point.at (id)) = (uint32_t) id;
  }

  return result;
}

Cloud::Ptr
load_ply (const std::string &filename)
{
  pcl::PCLPointCloud2::Ptr cloud_in (new pcl::PCLPointCloud2 ());
  Cloud::Ptr cloud (new Cloud ());
  pcl::PLYReader reader;
  reader.read (filename, *cloud_in);
  pcl::fromPCLPointCloud2 (*cloud_in, *cloud);
  return cloud;
}

#ifdef IS_PLY_READ_AVAILABLE
pcl::PolygonMesh::Ptr
load_mesh(const std::string &filename)
{
  pcl::PolygonMesh::Ptr mesh(new pcl::PolygonMesh());
  pcl::PLYReader reader;
  reader.read(filename, *mesh);
  return mesh;
}
#endif

pcl::PolygonMesh::Ptr
trim_mesh (const pcl::PolygonMesh::ConstPtr &total_mesh,
           const pcl::search::KdTree<Point>::ConstPtr &kdtree,
           const float threshold_distance)
{
  pcl::PolygonMesh::Ptr mesh (new pcl::PolygonMesh ());
  *mesh = *total_mesh;
  const size_t size_cloud = total_mesh->cloud.height * total_mesh->cloud.width;
  const uint32_t offset_point = total_mesh->cloud.width;
  const std::vector<int> index_point_in = get_near_point_index (total_mesh, kdtree, threshold_distance);
  const std::vector<int> index_triangle = get_involved_mesh_index (total_mesh->polygons, index_point_in, size_cloud);
  const std::vector<int> index_point = get_vertex_index (total_mesh->polygons, index_triangle, size_cloud);
  const std::vector<uint32_t> mapping = get_mapping_point (index_point, size_cloud);

  mesh->cloud.data.clear ();
  mesh->cloud.data.reserve (index_point.size () * total_mesh->cloud.point_step);
  std::vector<pcl::uint8_t> &data = mesh->cloud.data;
  const std::vector<pcl::uint8_t> &data_src = total_mesh->cloud.data;
  for (const int id_pt : index_point)
  {
    data.insert (data.end (),
                 data_src.begin () + id_pt * total_mesh->cloud.point_step,
                 data_src.begin () + (id_pt + 1) * total_mesh->cloud.point_step);
  }
  mesh->cloud.height = 1;
  mesh->cloud.width = (uint32_t) index_point.size ();

  mesh->polygons.clear ();
  mesh->polygons.reserve (index_triangle.size ());
  for (const int id_pt : index_triangle)
  {
    mesh->polygons.push_back (total_mesh->polygons.at (id_pt));
    for (uint32_t &id : mesh->polygons.back ().vertices)
    {
      id = mapping.at (id);
    }
  }

  return mesh;
}

void
print_help ()
{
  std::cout << "./poisson path_input_cloud_ply path_output_mesh_all path_output_mesh_trimmed "
            << "maximal_depth distance_trim"
            << std::endl;
}

int
main (int argc, char **argv)
{
  if (argc <= 1)
  {
    print_help ();
    return 1;
  }
  std::string mode ("both");
  std::string fn_in (argv[1]);
  std::string fn_all (argv[2]);
  std::string fn_trim (argv[3]);
  const int depth = std::stoi (argv[4]);
  const float threshold_distance = std::stof (argv[5]);

#ifdef IS_PLY_READ_AVAILABLE
  if(argc > 6)
  {
    if(std::string(argv[6]) == "trim")
    {
      mode = "trim";
    }
  }
#endif

  Cloud::Ptr cloud = load_ply (fn_in);
  pcl::search::KdTree<Point>::Ptr kdtree = get_search_kdtree<Point> (cloud);

  pcl::PolygonMesh::Ptr result;
  if (mode == "both")
  {
    result = trianglulate_mesh_poisson<Point> (cloud, kdtree, depth);
    pcl::io::savePLYFileBinary (fn_all, *result);
  }
#ifdef IS_PLY_READ_AVAILABLE
  else if(mode == "trim")
  {
    result = load_mesh(fn_all);
//    pcl::io::loadPLYFile(fn_all, *result);
  }
#endif

  result = trim_mesh (result, kdtree, threshold_distance);
  pcl::io::savePLYFileBinary (fn_trim, *result);

  return 0;
}
