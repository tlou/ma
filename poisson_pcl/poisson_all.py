#! /bin/python2

import os
import sys
import time

SIZE = int(sys.argv[1])
LEVEL = int(sys.argv[2])
THRESHOLD = float(sys.argv[3])
SCENE_ID = sys.argv[4]

HOME = os.getenv('HOME')
SUBNAME = "{}_{}_{}".format(SIZE, LEVEL, THRESHOLD)
DIR_SRC = HOME + "/workspace/navvis_data/" + SCENE_ID + "/sub{}".format(SIZE)
DIR_BASE = HOME + "/workspace/navvis_data/" + SCENE_ID + "/poisson_pcl"
DIR_RECON = DIR_BASE + "/origin_" + SUBNAME
DIR_TRIM = DIR_BASE + "/trim_" + SUBNAME
EXE_RECON = HOME + "/workspace/ma/poisson_pcl/build/poisson"

if not os.path.isdir(DIR_SRC):
    print "src dir not found"
    sys.exit()

if not os.path.isfile(EXE_RECON):
    print "exe not found"
    sys.exit()

#print DIR_SRC
#print DIR_BASE
#print DIR_RECON
#print DIR_TRIM
#sys.exit()

os.system("rm -rf " + DIR_RECON)
os.system("rm -rf " + DIR_TRIM)
os.system("mkdir -p " + DIR_RECON)
os.system("mkdir -p " + DIR_TRIM)

time_start = time.time()

task_pool = os.listdir(DIR_SRC)
for task in task_pool:
    fn = "/" + task
    command = EXE_RECON + " " + DIR_SRC + fn + " " + DIR_RECON + fn + " " \
            + DIR_TRIM + fn + " {} {}".format(LEVEL, THRESHOLD)
    print command
    os.system(command)

time_end = time.time()
with open(DIR_TRIM + "/log", 'w') as out:
    out.write("running time is {}, from {} to {}".format(\
            time_end - time_start, time_start, time_end))
