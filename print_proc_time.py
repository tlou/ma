import sys
import os

import proc_done

list_done = proc_done.load_list("list_finished_bpa")

def load_log(path):
    log = proc_done.load_list(path)
    if len(log) == 0:
        return ""
    else:
        return log[0]

for item_done in list_done:
    print item_done
    print load_log(item_done + "log").replace(",", " ")
    print load_log(item_done[:-1] + "_log").replace(",", " ")
    print ""
