import sys
import os

load_filename = lambda path: [p for p in os.listdir(path) if ".ply" in p] if os.path.isdir(path) else []

compare_fn_list = lambda ls0, ls1: [it for it in ls0 if it not in ls1]

def load_list(path):
    if not os.path.isfile(path):
        return []
    lines = []
    with open(path, 'r') as filein:
        for line in filein:
            lines.append(line.replace("\n", ""))
    return lines

if __name__ == "__main__":
    targets = load_list("list_target_bpa")
    done = load_list("list_finished_bpa")
    todoes = []

    for target in targets:
        dir_src, dir_dst = target.split(" ")
        dir_src = os.path.expanduser(dir_src)
        dir_dst = os.path.expanduser(dir_dst)
        if os.path.isdir(dir_dst) and len(compare_fn_list(load_filename(dir_src), load_filename(dir_dst))) == 0 and dir_dst not in done:
            todoes.append(dir_dst)

    for todo in todoes:
        print todo
        os.system("python2 fix_indice_name.py " + todo)
        os.system("./merge_mesh/build/merge_mesh " + todo + " " + todo + "/..")
        with open("list_finished_bpa", 'a') as fileout:
            fileout.write(todo + "\n")

