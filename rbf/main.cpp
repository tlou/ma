/**
 * this code is an experiment of applying radial basis function based reconstruction
 *
 * it require too much memory even after splitting and subsampling, applying sparse matrix is slow and helpless
 *
 * this code is mirror version of Hoppe's method (hoppe)
 */

#include <iostream>
#include <string>
#include <thread>
#include <chrono>
#include <mutex>
#include <vector>
#include <cstdlib>
#include <sstream>

#include <pcl/common/common_headers.h>
#include <pcl/filters/passthrough.h>
#include <pcl/io/ply_io.h>
#include <pcl/conversions.h>

#include "surface/include/pcl/surface/marching_cubes_rbf.h"
#include "surface/include/pcl/surface/impl/marching_cubes_rbf.hpp"

#include "Timer.hpp"

typedef pcl::PointXYZRGBNormal Point;
typedef pcl::PointCloud<Point> Cloud;

std::vector<std::string>
parse_string(const std::string &string, const std::string &key)
{
  std::vector<std::string> result;
  std::string copy = string;
  size_t pos = copy.find(key);
  while(!copy.empty() && pos != std::string::npos)
  {
    result.push_back(copy.substr(0, pos));
    copy = copy.substr(pos + 1);
    pos = copy.find(key);
  }
  if(!copy.empty())
  {
    result.push_back(copy);
  }
  return result;
}

std::pair<std::string, std::string>
split_path_filename(const std::string &full_path)
{ 
  std::pair<std::string, std::string> re;
  std::size_t id = full_path.find_last_of("/");

  re.first = full_path.substr(0, id);
  re.second = full_path.substr(id + 1);

  return re;
}

std::string
get_filename_no_suffix(const std::string &filename)
{
  size_t pos = filename.find_last_of(".");
  if(pos != std::string::npos)
  {
    return filename.substr(0, pos);
  }
  else
  {
    return filename;
  }
}

void
parse_filename(const std::string &filename, float &center_x, float &center_y, float &size)
{
  std::vector<std::string> params = parse_string(
      get_filename_no_suffix(parse_string(filename, "/").back()), "_");
  if(params.size() == 3)
  {
    center_x = std::stof(params.at(0));
    center_y = std::stof(params.at(1));
    size = std::stof(params.at(2));
  }
}

template<class T>
typename pcl::search::KdTree<T>::Ptr
get_search_kdtree(const typename pcl::PointCloud<T>::ConstPtr &cloud)
{
  typename pcl::search::KdTree<T>::Ptr kdtree(new pcl::search::KdTree<T>);
  kdtree->setInputCloud(cloud);
  return kdtree;
}

template<class T>
pcl::PolygonMesh::Ptr
trianglulate(const typename pcl::PointCloud<T>::ConstPtr &cloud,
    const Eigen::Vector3i &resolution)
{
  pcl::PolygonMesh::Ptr mesh(new pcl::PolygonMesh());

  typename pcl::MarchingCubesRBF<T> mc;
  mc.setInputCloud(cloud);
  mc.setGridResolution(resolution(0), resolution(1), resolution(2));
  mc.setSearchMethod(get_search_kdtree<T>(cloud));
  mc.setIsDense(false);
  mc.reconstruct(*mesh);
  return mesh;
}

Eigen::Vector3i
get_resolution(const Cloud::ConstPtr &cloud,
    const float size,
    const float size_grid)
{
  Point max, min;
  pcl::getMinMax3D(*cloud, min, max);
  return Eigen::Vector3i((int) std::ceil(std::fabs(max.x - min.x) / size_grid),
      (int) std::ceil(std::fabs(max.y - min.y) / size_grid),
      (int) std::ceil(std::fabs(max.z - min.z) / size_grid));
}

Cloud::Ptr
load_ply(const std::string &filename)
{
  pcl::PCLPointCloud2::Ptr cloud_in(new pcl::PCLPointCloud2());
  Cloud::Ptr cloud(new Cloud());
  pcl::PLYReader reader;
  reader.read(filename, *cloud_in);
  pcl::fromPCLPointCloud2(*cloud_in, *cloud);
  return cloud;
}

bool
read_log(const std::string &filename, double &duration, double &time0, double &time1)
{   
  std::fstream in;
  std::string shit;
  in.open(filename, std::fstream::in);
  if(in.is_open())
  {
    in >> shit >> shit >> shit >> duration >> shit >> shit
      >> time0 >> shit >> time1;
    in.close();
    if(duration > 1e-3 && duration < 1e12)
    { // likely wrong input
      return true;
    }
  }
  return false;
}

int main(int argc, char **argv)
{
  const std::string fn_in(argv[1]);
  const std::string fn_out(argv[2]);
  const float size_grid = std::stof(argv[3]);

  Cloud::Ptr cloud = load_ply(fn_in);
  std::pair<std::string, std::string> fn_o_split = split_path_filename(fn_out);

  float center_x, center_y, size;
  parse_filename(fn_in, center_x, center_y, size);

  Timer timer;
  pcl::PolygonMesh::Ptr result =
    trianglulate<Point>(cloud, get_resolution(cloud, size, size_grid));
  double time_elapsed = timer.getSecFromStart();
  double time_start = timer.getStart();
  double time_end = timer.getEnd();
  double duration_log, time0_log, time1_log; // from log
  if(read_log(fn_o_split.first + "/" + "log", duration_log, time0_log, time1_log))
  {
    time_elapsed += duration_log;
    time_start = time0_log;
  }

  pcl::io::savePLYFileBinary(fn_out, *result);

  std::ofstream os;
  os.open(fn_o_split.first + "/" + "log", std::ios_base::out);
  os << std::fixed << "running time is " << time_elapsed << ", from " << time_start
    << " to " << time_end << std::endl;
  os.close();

  return 0;
}
