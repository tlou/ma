#! /bin/python2
#../cloud_proc/build/cloud_proc hoppe /home/tlou/workspace/navvis_data/subs/56_-24.ply /home/tlou/workspace/navvis_data/56_-24_hoppe.ply

import os
import sys
import time
import threading

if len(sys.argv) < 4:
    print "argv insufficient"
    sys.exit()

SIZE = float(sys.argv[1])
SIZE_GRID = float(sys.argv[2])
SCENE_ID = sys.argv[3]

_SIZE_INT = int(round(SIZE * 2.0))
STR_SIZE = "{}_{}".format(_SIZE_INT / 2, (_SIZE_INT % 2) * 5)
HOME = os.getenv('HOME')
DIR_SRC = HOME + "/workspace/navvis_data/" + SCENE_ID + "/sub" + STR_SIZE
DIR_RECON = HOME+ "/workspace/navvis_data/" + SCENE_ID \
        + "/rbf/{}_{}".format(STR_SIZE, SIZE_GRID)
EXE = HOME+ "/workspace/ma/rbf/build/rbf"

os.system("rm -rf " + DIR_RECON)
os.system("mkdir -p " + DIR_RECON)

#task_pool = os.listdir(DIR_SRC)
task_pool = [fn for fn in os.listdir(DIR_SRC) if ".ply" in fn]

time_start = time.time()
for task in task_pool:
    fn_in = DIR_SRC + "/" + task
    fn_out = DIR_RECON + "/" + task
    os.system(EXE + " " + fn_in + " " + fn_out + " {}".format(SIZE_GRID))
    print (EXE + " " + fn_in + " " + fn_out + " {}".format(SIZE_GRID))

time_end = time.time()

#with open(DIR_RECON + "/log", 'w') as out:
#    out.write("running time is {}, from {} to {}".format(\
#            time_end - time_start, time_start, time_end))
