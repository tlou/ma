#ifndef INDOOR_RECON_COMMON_H
#define INDOOR_RECON_COMMON_H

// common include and data types

#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <cstdlib>
#include <sstream>
#include <sys/stat.h>
#include <experimental/filesystem>

#include <pcl/common/common_headers.h>
#include <pcl/io/ply_io.h>
#include <pcl/conversions.h>

#include <Eigen/Dense>

typedef pcl::PointXYZRGBNormal Point;
typedef pcl::PointCloud<Point> Cloud;

typedef pcl::PointXYZ PointP;
typedef pcl::PointCloud<PointP> CloudP;

typedef pcl::PointXYZI PointI;
typedef pcl::PointCloud<PointI> CloudI;

typedef std::vector<int> Index;
typedef std::pair<Index, Eigen::Vector4f> Segment;

#endif
