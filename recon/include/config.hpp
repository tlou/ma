#ifndef INDOOR_RECON_CONFIG_HPP
#define INDOOR_RECON_CONFIG_HPP

#include <experimental/filesystem>
#include <yaml-cpp/yaml.h>
#include "common.h"

/**
 * read config from path file
 * @param path_config
 * @param name_config
 * @return
 */
std::string
read_config (const std::string &path_config,
             const std::string &name_config)
{
  namespace fs = std::experimental::filesystem;

  if (fs::is_regular_file (fs::path (path_config)))
  {
    YAML::Node node = YAML::LoadFile (path_config);
    if (node[name_config])
    {
      return node[name_config].as<std::string> ();
    }
    else
    {
      return "";
    }
  }
  else
  {
    return std::string ("");
  }
}

#endif