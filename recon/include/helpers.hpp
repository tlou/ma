#ifndef INDOOR_RECON_HELPERS_HPP
#define INDOOR_RECON_HELPERS_HPP

#include <experimental/filesystem>
#include "common.h"

/**
 * return the filenames with extension in the directory
 * @param path
 * @param extension "" if all should be returned, including "." after filename
 * @return
 */
std::vector<std::string>
get_all_files (const std::string &path, const std::string &extension)
{
  namespace fs = std::experimental::filesystem;
  std::vector<std::string> re;
  for (fs::directory_entry entry: fs::directory_iterator (path))
  {
    if (extension.empty () || entry.path ().extension ().string () == extension)
    {
      re.push_back (entry.path ().filename ().string ());
    }
  }
  return re;
}

#endif