#ifndef INDOOR_RECON_MERGE_HPP
#define INDOOR_RECON_MERGE_HPP

#include "common.h"

/**
 * split the string in with respect to the key
 * @param string "a_b_c_d"
 * @param key "_"
 * @return "a" "b" "c" "d"
 */
std::vector <std::string>
parse_string (const std::string &string,
              const std::string &key)
{
  std::vector <std::string> result;
  std::string copy = string;
  size_t pos = copy.find (key);
  while (!copy.empty () && pos != std::string::npos)
  {
    result.push_back (copy.substr (0, pos));
    copy = copy.substr (pos + 1);
    pos = copy.find (key);
  }
  if (!copy.empty ())
  {
    result.push_back (copy);
  }
  return result;
}

/**
 * read the boundary information from filename
 * @param filename 4_5_2
 * @param xlim 2,6
 * @param ylim 3,7
 */
void
parse_filename (const std::string &filename,
                Eigen::Vector2f &xlim,
                Eigen::Vector2f &ylim)
{
  namespace fs = std::experimental::filesystem;
  // the filename should be (stem part, excluding directory and extension)
  // centerx_centery_splitsize
  std::vector <std::string> params = parse_string (
    fs::path (filename).stem ().string (), "_");

  if (params.size () == 3)
  {
    const float center_x = std::stof (params.at (0));
    const float center_y = std::stof (params.at (1));
    const float size = std::stof (params.at (2));
    xlim << center_x - size / 2.0f, center_x + size / 2.0f;
    ylim << center_y - size / 2.0f, center_y + size / 2.0f;
  }
}

/**
 * get the index of points between the boundary
 * @param cloud
 * @param xlim
 * @param ylim
 * @return
 */
Index
get_passthrough_index (const pcl::PCLPointCloud2 &cloud,
                       const Eigen::Vector2f &xlim,
                       const Eigen::Vector2f &ylim)
{
  pcl::PointCloud <pcl::PointXYZRGBNormal> cloud_explicit;
  Index index;
  pcl::fromPCLPointCloud2 (cloud, cloud_explicit);
  index.reserve (cloud_explicit.size ());

  for (size_t id = 0; id < cloud_explicit.size (); ++id)
  {
    const pcl::PointXYZRGBNormal &pt = cloud_explicit.at (id);
    if (pt.x >= xlim (0) && pt.x <= xlim (1) && pt.y >= ylim (0) && pt.y <= ylim (1))
    {
      index.push_back ((int) id);
    }
  }

  return index;
}

/**
 * get the index of vertices, which index_mesh includes its triangle index
 * @param vertices vertices id in mesh
 * @param index_mesh indexes of target triangles
 * @param num_vertex number of vertices in mesh
 * @return
 */
Index
get_vertex_index (const std::vector <pcl::Vertices> &vertices,
                  const std::vector<int> &index_mesh,
                  const size_t num_vertex)
{
  std::vector<bool> is_in (num_vertex, false);
  Index index;
  index.reserve (num_vertex);

  // binary indicator of whether vertices are in target triangles
  for (const int id_mesh: index_mesh)
  {
    const pcl::Vertices &triangle = vertices.at (id_mesh);
    for (const uint32_t &id : triangle.vertices)
    {
      is_in.at (id) = true;
    }
  }

  // extract vertex indices
  for (size_t id = 0; id < num_vertex; ++id)
  {
    if (is_in.at (id))
    {
      index.push_back ((int) id);
    }
  }

  return index;
}

/**
 * get the indices of triangles in which at least one vertex is in index_vertex
 * @param vertices all triangles
 * @param index_vertex index of vertices in boundary
 * @param num_point total number of points
 * @return
 */
std::vector<int>
get_involved_mesh_index (const std::vector <pcl::Vertices> &vertices,
                         const std::vector<int> &index_vertex,
                         const size_t num_point)
{
  Index count (vertices.size (), 0);
  std::vector<bool> is_in (num_point, false);
  Index index;
  index.reserve (vertices.size ());

  for (const int id : index_vertex)
  {
    is_in.at (id) = true;
  }

  // count the number of vertices in boundary for all triangles
  for (size_t id_mesh = 0; id_mesh < vertices.size (); ++id_mesh)
  {
    const pcl::Vertices &triangle = vertices.at (id_mesh);
    for (const uint32_t &id : triangle.vertices)
    {
      if (is_in.at (id))
      {
        ++count.at (id_mesh);
      }
    }
  }

  // get the indices of triangles which has at least on vertex in index_vertex
  for (size_t id_mesh = 0; id_mesh < vertices.size (); ++id_mesh)
  {
    if (count.at (id_mesh) >= 1)
    {
      index.push_back ((int) id_mesh);
    }
  }

  return index;
}

/**
 * get the new index for new vertices
 * @param index_point old indices of the vertices
 * @param offset first new index
 * @param size_cloud
 * @return
 */
std::vector <uint32_t>
get_mapping_point (const Index &index_point,
                   const uint32_t offset,
                   const size_t size_cloud)
{
  std::vector <uint32_t> result;
  result.resize (size_cloud);

  for (size_t id = 0; id < index_point.size (); ++id)
  {
    result.at (index_point.at (id)) = (uint32_t) id + offset;
  }

  return result;
}

/**
 * add new_mesh to total_mesh
 * @param total_mesh
 * @param new_mesh
 * @param xlim boundary for x
 * @param ylim boundary for y
 */
void
add_mesh (pcl::PolygonMesh::Ptr &total_mesh,
          const pcl::PolygonMesh::ConstPtr &new_mesh,
          const Eigen::Vector2f &xlim,
          const Eigen::Vector2f &ylim)
{
  const size_t size_cloud = new_mesh->cloud.height * new_mesh->cloud.width;
  // indices of points in boundary
  const Index index_point_in = get_passthrough_index (new_mesh->cloud, xlim, ylim);
  // indices of triangles involved in region
  const Index index_triangle = get_involved_mesh_index (new_mesh->polygons, index_point_in, size_cloud);
  // get the indices of vertices from last list
  const Index index_point = get_vertex_index (new_mesh->polygons, index_triangle, size_cloud);
  const std::vector <uint32_t> mapping = get_mapping_point (index_point, total_mesh->cloud.width,
                                                            new_mesh->cloud.width);

  // init total mesh if needed
  if (total_mesh->cloud.data.empty ())
  {
    total_mesh->cloud = new_mesh->cloud;
    total_mesh->cloud.width = 0;
    total_mesh->cloud.data.clear ();
  }

  {
    // append the new mesh: vertices part
    total_mesh->cloud.data.reserve ((total_mesh->cloud.width + index_point.size ()) * new_mesh->cloud.point_step);
    std::vector <pcl::uint8_t> &data = total_mesh->cloud.data;
    const std::vector <pcl::uint8_t> &data_src = new_mesh->cloud.data;
    for (const int id_pt : index_point)
    {
      data.insert (data.end (),
                   data_src.begin () + id_pt * new_mesh->cloud.point_step,
                   data_src.begin () + (id_pt + 1) * new_mesh->cloud.point_step);
    }
    total_mesh->cloud.width += index_point.size ();
  }

  // append the new mesh: triangles part
  total_mesh->polygons.reserve (total_mesh->polygons.size () + index_triangle.size ());
  for (const int id_tri : index_triangle)
  {
    total_mesh->polygons.push_back (new_mesh->polygons.at (id_tri));
    for (uint32_t &id : total_mesh->polygons.back ().vertices)
    {
      id = mapping.at (id);
    }
  }
}

/**
 * add mesh with filename to total_mesh
 * @param total_mesh
 * @param filename
 * @param time
 */
void
add_mesh_file (pcl::PolygonMesh::Ptr &total_mesh,
               const std::string &filename,
               double &time)
{
  Timer timer;
  Eigen::Vector2f xlim, ylim;
  pcl::PolygonMesh::Ptr mesh (new pcl::PolygonMesh ());
  pcl::io::loadPLYFile (filename, *mesh);

  timer.startNow ();

  parse_filename (filename, xlim, ylim);
  add_mesh (total_mesh, mesh, xlim, ylim);

  time += timer.getSecFromStart ();
}

/**
 * add all meshed in path to total_mesh
 * @param total_mesh
 * @param path
 * @param time
 */
void
add_mesh_dir (pcl::PolygonMesh::Ptr &total_mesh, const std::string &path, double &time)
{
  const std::vector <std::string> filenames = get_all_files (path, ".ply");

  for (const std::string &filename: filenames)
  {
    add_mesh_file (total_mesh, path + "/" + filename, time);
  }
}

/**
 * return the total_mesh for meshes in directory
 * @param path
 * @param time
 * @return
 */
pcl::PolygonMesh::Ptr
add_mesh_dir (const std::string &path, double &time)
{
  pcl::PolygonMesh::Ptr re (new pcl::PolygonMesh ());
  re->cloud.height = 1;

  add_mesh_dir (re, path, time);

  return re;
}

/**
 * merge meshes in directory to path
 * @param path_dir_meshes input path, directory
 * @param path_mesh output path, .ply file
 * @param time
 */
void
merge_mesh_dir (const std::string &path_dir_meshes,
                const std::string &path_mesh,
                double &time)
{
  time = 0.0;
  pcl::io::savePLYFileBinary (path_mesh, *add_mesh_dir (path_dir_meshes, time));
}

#endif
