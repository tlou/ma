#ifndef INDOOR_RECON_MLS_HPP
#define INDOOR_RECON_MLS_HPP

#include "common.h"
#include "Timer.hpp"
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/surface/mls.h>

/**
 * get the result after mls
 * @param cloud_in
 * @param radius_search
 * @param time
 * @return
 */
Cloud::Ptr
mls (const Cloud::ConstPtr &cloud_in,
     const double radius_search,
     double &time)
{
  Cloud::Ptr cloud_regen (new Cloud ());
  pcl::search::KdTree<Point>::Ptr tree (new pcl::search::KdTree<Point> ());
  pcl::MovingLeastSquares <Point, Point> mls;
  Timer timer;
  time = 0.0;

  timer.startNow ();
  mls.setInputCloud (cloud_in);
  mls.setComputeNormals (true);
  mls.setPolynomialFit (true);
  mls.setSearchMethod (tree);
  mls.setSearchRadius (radius_search);
  mls.process (*cloud_regen);

  time = timer.getSecFromStart ();

  return cloud_regen;
}

#endif