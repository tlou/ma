#ifndef INDOOR_RECON_RECONSTRUCTION_HPP
#define INDOOR_RECON_RECONSTRUCTION_HPP

#include "common.h"
#include "helpers.hpp"
#include "Timer.hpp"
#include "surface/include/pcl/surface/marching_cubes.h"
#include "surface/include/pcl/surface/impl/marching_cubes.hpp"
#include "surface/include/pcl/surface/marching_cubes_hoppe.h"
#include "surface/include/pcl/surface/impl/marching_cubes_hoppe.hpp"

#include "impl/Pivoter.hpp"

/**
 * get the kdtree for searching
 * @param cloud
 * @return
 */
pcl::search::KdTree<Point>::Ptr
get_search_kdtree (const Cloud::ConstPtr &cloud)
{
  pcl::search::KdTree<Point>::Ptr kdtree (new pcl::search::KdTree<Point> ());
  kdtree->setInputCloud (cloud);
  return kdtree;
}

/**
 * reconstruct with hoppe's method, return the mesh
 * @param cloud
 * @param resolution
 * @param size_grid
 * @return
 */
pcl::PolygonMesh::Ptr
triangulate_hoppe (const Cloud::ConstPtr &cloud,
                   const Eigen::Vector3i &resolution,
                   const float size_grid)
{
  const float thres_dist = size_grid * 2.0f;
  pcl::PolygonMesh::Ptr mesh (new pcl::PolygonMesh ());

  pcl::MarchingCubesHoppe <Point> mc;
  mc.setInputCloud (cloud);
  mc.setGridResolution (resolution (0), resolution (1), resolution (2));
  mc.setSearchMethod (get_search_kdtree (cloud));
  mc.setDistanceIgnore (thres_dist * thres_dist);
  mc.reconstruct (*mesh);
  return mesh;
}

/**
 * reconstruct with hoppe's method, from path to path
 * @param path_in path for input, .ply file
 * @param path_out path for output, .ply file
 * @param size_grid
 * @param time
 */
void
triangulate_hoppe (const std::string &path_in,
                   const std::string &path_out,
                   const float size_grid,
                   double &time)
{
  Cloud::Ptr cloud (new Cloud ());
  pcl::io::loadPLYFile (path_in, *cloud);
  // get resolution from maxima and size_grid
  Point pmax, pmin;
  pcl::getMinMax3D (*cloud, pmin, pmax);
  const Eigen::Vector3f resolution_f = (pmax.getVector3fMap () - pmin.getVector3fMap ()) / size_grid;
  const Eigen::Vector3i resolution ((int) std::ceil (resolution_f (0)),
                                    (int) std::ceil (resolution_f (1)),
                                    (int) std::ceil (resolution_f (2)));

  Timer timer;
  timer.startNow ();
  pcl::PolygonMesh::Ptr mesh = triangulate_hoppe (cloud, resolution, size_grid);
  time += timer.getSecFromStart ();

  pcl::io::savePLYFileBinary (path_out, *mesh);
}

/**
 * reconstruct all files in path_in to files in path_out
 * @param path_in path for input, directory
 * @param path_out path for output, directory
 * @param size_grid
 * @param time
 */
void
triangulate_hoppe_dir (const std::string &path_in,
                       const std::string &path_out,
                       const float size_grid,
                       double &time)
{
  time = 0.0;
  const std::vector <std::string> filenames = get_all_files (path_in, ".ply");
  for (const std::string &filename: filenames)
  {
    triangulate_hoppe (path_in + "/" + filename,
                       path_out + "/" + filename,
                       size_grid, time);
  }
}

/**
 * reconstruct with poisson (external binary program)
 * @param path_exe_recon path for program for reconstruction
 * @param path_exe_trim path for program for trimming
 * @param path_in path for input file, .ply
 * @param path_out path for output file, .ply
 * @param level_recon
 * @param level_trim
 */
void
triangulate_poisson (const std::string &path_exe_recon,
                     const std::string &path_exe_trim,
                     const std::string &path_in,
                     const std::string &path_out,
                     const int level_recon,
                     const int level_trim)
{
  const std::string path_tmp = "./_.ply"; // tmp file
  const std::string command_recon = path_exe_recon
                                    + " --in " + path_in
                                    + " --out " + path_tmp
                                    + " --depth " + std::to_string (level_recon)
                                    + " --color 16 --density";
  const std::string commond_trim = path_exe_trim
                                   + " --in " + path_tmp
                                   + " --out " + path_out
                                   + " --trim " + std::to_string (level_trim);

  std::system (command_recon.c_str ());
  std::system (commond_trim.c_str ());
  std::system ((std::string ("rm ") + path_tmp).c_str ());
}

/**
 * reconstruct with poisson (with external program) all meshes from path_in to path_out
 * @param path_exe_recon path for program for reconstruction
 * @param path_exe_trim path for program for trimming
 * @param path_in path for input files, directory
 * @param path_out path for output files, directory
 * @param level_recon
 * @param level_trim
 * @param time
 */
void
triangulate_poisson_dir (const std::string &path_exe_recon,
                         const std::string &path_exe_trim,
                         const std::string &path_in,
                         const std::string &path_out,
                         const int level_recon,
                         const int level_trim,
                         double &time)
{
  Timer timer;
  time = 0.0;
  const std::vector <std::string> filenames = get_all_files (path_in, ".ply");
  for (const std::string &filename: filenames)
  {
    timer.startNow ();
    triangulate_poisson (path_exe_recon, path_exe_trim,
                         path_in + "/" + filename,
                         path_out + "/" + filename,
                         level_recon, level_trim);
    time += timer.getSecFromStart ();
  }
}

/**
 * reconstruct with BPA, returns mesh
 * @param cloud
 * @param radius
 * @return
 */
pcl::PolygonMesh::Ptr
triangulate_bpa (const Cloud::ConstPtr &cloud,
                 const float radius)
{
  Pivoter <Point> pivoter;
  pivoter.setSearchRadius (radius);
  pivoter.setInputCloud (cloud);
  return pivoter.proceed ();
}

/**
 * reconstruct with BPA all clouds in path_in to meshes in path_out
 * @param path_in path for input, directory
 * @param path_out path for output, directory
 * @param radius
 * @param time
 */
void
triangulate_bpa_dir (const std::string &path_in,
                     const std::string &path_out,
                     const float radius, double &time)
{
  Timer timer;
  time = 0.0;
  const std::vector <std::string> filenames = get_all_files (path_in, ".ply");
  for (const std::string &filename: filenames)
  {
    Cloud::Ptr cloud (new Cloud ());
    pcl::io::loadPLYFile (path_in + "/" + filename, *cloud);
    timer.startNow ();
    pcl::PolygonMesh::Ptr mesh = triangulate_bpa (cloud, radius);
    time += timer.getSecFromStart ();
    pcl::io::savePLYFileBinary (path_out + "/" + filename, *mesh);
  }
}

#endif
