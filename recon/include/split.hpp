#ifndef INDOOR_RECON_SPLIT_HPP
#define INDOOR_RECON_SPLIT_HPP

#include "common.h"
#include "Timer.hpp"
#include <pcl/filters/passthrough.h>

typedef std::vector <Eigen::Vector2f, Eigen::aligned_allocator<Eigen::Vector2f>> VVector2f;

/**
 * apply passthrough filter to cloud. in all directions, the filtering is applied
 * if lim*(0) < lim*(1)
 * @param cloud_in
 * @param limx
 * @param limy
 * @param limz
 * @return
 */
Cloud::Ptr
pass_through (const Cloud::ConstPtr &cloud_in,
              const Eigen::Vector2f &limx,
              const Eigen::Vector2f &limy,
              const Eigen::Vector2f &limz)
{
  Cloud::Ptr filtered (new Cloud ());
  pcl::PassThrough <Point> pass;

  *filtered = *cloud_in;

  if (limx (0) < limx (1))
  {
    pass.setInputCloud (filtered);
    pass.setFilterFieldName ("x");
    pass.setFilterLimits (limx (0), limx (1));
    pass.filter (*filtered);
  }

  if (limy (0) < limy (1))
  {
    pass.setInputCloud (filtered);
    pass.setFilterFieldName ("y");
    pass.setFilterLimits (limy (0), limy (1));
    pass.filter (*filtered);
  }

  if (limz (0) < limz (1))
  {
    pass.setInputCloud (filtered);
    pass.setFilterFieldName ("z");
    pass.setFilterLimits (limz (0), limz (1));
    pass.filter (*filtered);
  }

  return filtered;
}

/**
 * get the boundary from the size, range and boundary_ext
 * @param range range 0,10
 * @param size_sub_range size 5
 * @param size_boundary boundary_ext 0.3
 * @return ((-0.3,5.3),(4.7,10.3))
 */
VVector2f
get_sub_range (const Eigen::Vector2f &range,
               const float size_sub_range,
               const float size_boundary)
{
  const float boundary = size_boundary;
  // length of boundary
  const float len = range (1) - range (0);
  // number of sub regions
  const int num = (int) std::ceil (len / size_sub_range);
  // starting point
  const float start = (range (1) + range (0)) / 2.0f - size_sub_range * 0.5f * (float) num;

  VVector2f result;
  result.reserve (num);
  // get the boundary for all sub-regions
  for (int i = 0; i < num; ++i)
  {
    const float start_ = start + size_sub_range * (float) i;
    result.push_back (Eigen::Vector2f (start_ - boundary, start_ + boundary + size_sub_range));
  }
  return result;
}

/**
 * split the cloud and save to path_subcloud with filename about location and size
 * @param cloud
 * @param path_subcloud
 * @param size_sub_range
 * @param size_boundary
 * @param time
 */
void
split (const Cloud::ConstPtr &cloud,
       const std::string &path_subcloud,
       const float size_sub_range,
       const float size_boundary,
       double &time)
{
  // get the range of cloud
  Eigen::Vector2f limx, limy;
  Point pmin, pmax;
  pcl::getMinMax3D (*cloud, pmin, pmax);
  limx << pmin.x, pmax.x;
  limy << pmin.y, pmax.y;

  // list of boundaries at x and y directions
  const VVector2f vec_range_x = get_sub_range (limx, size_sub_range, size_boundary);
  const VVector2f vec_range_y = get_sub_range (limy, size_sub_range, size_boundary);
  Timer timer;
  time = 0.0;

  // passthrough for each sub-region
  for (const Eigen::Vector2f &range_x : vec_range_x)
  {
    float mean_x = (range_x (0) + range_x (1)) / 2.0f;
    for (const Eigen::Vector2f &range_y : vec_range_y)
    {
      float mean_y = (range_y (0) + range_y (1)) / 2.0f;

      std::stringstream ss;
      ss << path_subcloud << "/" << mean_x << "_" << mean_y << "_" << size_sub_range << ".ply";
      timer.startNow ();
      Cloud::Ptr sub = pass_through (cloud, range_x, range_y,
                                     Eigen::Vector2f (1.0f, 0.0f));
      time += timer.getSecFromStart ();
      pcl::io::savePLYFileBinary (ss.str (), *sub);
    }
  }
}

#endif
