#ifndef INDOOR_RECON_SUBSAMPLE_HPP
#define INDOOR_RECON_SUBSAMPLE_HPP

#include "common.h"
#include "helpers.hpp"
#include "Timer.hpp"

#include <pcl/common/centroid.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/random_sample.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/search/kdtree.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/region_growing.h>

/**
 * overall subsample
 * @param cloud
 * @param ratio
 * @return
 */
Cloud::Ptr
subsample_all (const Cloud::ConstPtr &cloud, const float ratio)
{
  pcl::RandomSample <Point> sampler;
  Cloud::Ptr sampled (new Cloud ());

  time_t seconds;
  time (&seconds);

  sampler.setInputCloud (cloud);
  sampler.setSample ((unsigned int) (ratio * (double) cloud->size ()));
  sampler.setSeed ((unsigned int) seconds);
  sampler.filter (*sampled);

  return sampled;
}

/**
 * get the transform of cloud (origin to centroid)
 * @param cloud_in
 * @return
 */
Eigen::Matrix4f
getTransform (const Cloud::ConstPtr &cloud_in)
{
  Eigen::Matrix4f affine = Eigen::Matrix4f::Identity ();
  Eigen::Vector4f translate;
  Eigen::Matrix3f rotation;
  pcl::compute3DCentroid<Point> (*cloud_in, translate);
  affine.block (0, 3, 3, 1) << translate.segment (0, 3);
  return affine;
}

/**
 * fit one plane from cloud
 * @param cloud
 * @param thres_dist
 * @return
 */
Segment
fit_plane (const Cloud::ConstPtr &cloud, const double thres_dist)
{
  pcl::SACSegmentation <Point> seg;
  Eigen::Matrix4f transform = getTransform (cloud);
  Cloud::Ptr cloud_tf (new Cloud ());
  pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
  pcl::PointIndicesPtr inliers (new pcl::PointIndices ());
  Eigen::Vector4f plane;

  // move the cloud to origin, otherwise it can be very wrong for minimize w
  pcl::transformPointCloud<Point> (*cloud, *cloud_tf,
                                   Eigen::Matrix4f (transform.inverse ()));

  // fit
  seg.setOptimizeCoefficients (true);
  seg.setModelType (pcl::SACMODEL_PLANE);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setDistanceThreshold (thres_dist);
  seg.setInputCloud (cloud_tf);
  seg.segment (*inliers, *coefficients);

  // save the plane parameters and compensate the translate
  plane << coefficients->values[0], coefficients->values[1],
    coefficients->values[2], coefficients->values[3];
  plane (3) -= transform.col (3).segment (0, 3).dot (plane.segment (0, 3));

  return Segment (inliers->indices, plane);
}

/**
 * get the indices which are not in segments
 * @param segments
 * @param size_total
 * @return
 */
pcl::PointIndices::Ptr
get_indice_rest (const std::vector <Segment> &segments,
                 const size_t size_total)
{
  std::vector<bool> is_in (size_total, false);
  pcl::PointIndices::Ptr id_rest (new pcl::PointIndices ());

  for (const Segment &seg : segments)
  {
    for (const int id : seg.first)
    {
      is_in.at (id) = true;
    }
  }

  for (size_t id = 0; id < size_total; ++id)
  {
    if (!is_in.at (id))
    {
      id_rest->indices.push_back ((int) id);
    }
  }

  return id_rest;
}

/**
 * cluster with growing region
 * @param cloud
 * @param thres_curvature
 * @return
 */
std::vector <pcl::PointIndices>
cluster_gr (const Cloud::ConstPtr &cloud,
            const float thres_curvature)
{
  typedef pcl::PointXYZ PointIn0;
  typedef pcl::Normal PointIn1;
  std::vector <Segment> list_segment;
  pcl::RegionGrowing <PointIn0, PointIn1> rg;
  pcl::search::Search<PointIn0>::Ptr tree (new pcl::search::KdTree<PointIn0> ());
  pcl::PCLPointCloud2 cloud2;
  pcl::PointCloud<PointIn0>::Ptr cloud_xyz (new pcl::PointCloud<PointIn0> ());
  pcl::PointCloud<PointIn1>::Ptr cloud_normal (new pcl::PointCloud<PointIn1> ());

  // split data elements
  pcl::toPCLPointCloud2 (*cloud, cloud2);
  pcl::fromPCLPointCloud2 (cloud2, *cloud_xyz);
  pcl::fromPCLPointCloud2 (cloud2, *cloud_normal);

  // apply growing region
  rg.setMinClusterSize (50);
  rg.setMaxClusterSize (1000000);
  rg.setSearchMethod (tree);
  rg.setNumberOfNeighbours (10);
  rg.setInputCloud (cloud_xyz);
  rg.setInputNormals (cloud_normal);
  rg.setSmoothnessThreshold ((float) (3.0 / 180.0 * M_PI));
  rg.setCurvatureThreshold (thres_curvature);

  std::vector <pcl::PointIndices> clusters;
  rg.extract (clusters);

  return clusters;
}

/**
 * get the segments after clustering with growing-region
 * @param cloud
 * @param thres_curvature
 * @param thres_dist
 * @param thres_size
 * @return
 */
std::vector <Segment>
seg_gr (const Cloud::ConstPtr &cloud,
        const float thres_curvature,
        const float thres_dist,
        const size_t thres_size)
{
  std::vector <Segment> planes;
  // get clusters from growing region
  std::vector <pcl::PointIndices> clusters = cluster_gr (cloud, thres_curvature);

  planes.reserve (clusters.size ());
  // fit at most one plane on one segment
  for (const pcl::PointIndices &indice : clusters)
  {
    if (indice.indices.size () > thres_size)
    {
      Cloud::Ptr sub_cloud (new Cloud ());
      boost::shared_ptr <pcl::PointIndices> inliers = boost::make_shared<pcl::PointIndices> (indice);
      pcl::ExtractIndices <Point> extract;

      extract.setInputCloud (cloud);
      extract.setIndices (inliers);
      extract.setNegative (false);
      extract.filter (*sub_cloud);

      Segment seg = fit_plane (sub_cloud, thres_dist);
      // valid if plane has at least 25% of points
      if (seg.first.size () > indice.indices.size () / 4)
      {
        Index indice_origin;
        planes.push_back (seg);
        for (int i : seg.first)
        {
          indice_origin.push_back (indice.indices.at (i));
        }
        planes.back ().first.swap (indice_origin);
      }
    }
  }

  return planes;
}

/**
 * deprecated, segment the point cloud into a sery of planes
 * @param cloud_in
 * @param thres_dist
 * @param thres_size
 * @return
 */
std::vector <Segment>
seg_plane (const Cloud::ConstPtr &cloud_in,
           const double thres_dist,
           const size_t thres_size)
{
  Cloud::Ptr cloud (new Cloud ());
  std::vector <Segment> planes;
  pcl::copyPointCloud (*cloud_in, *cloud);

  // segment into planes, maximal 100 times
  for (size_t iter = 0; iter < 100; ++iter)
  {
    Segment seg = fit_plane (cloud, thres_dist);

    if (seg.first.size () < thres_size)
    {
      break;
    }
    else
    {
      pcl::ExtractIndices <Point> extract;
      pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());

      inliers->indices = seg.first;
      extract.setInputCloud (cloud);
      extract.setIndices (inliers);
      extract.setNegative (true);
      extract.filter (*cloud);

      planes.push_back (seg);

      if (cloud->size () < thres_size)
      {
        break;
      }
    }
  }
  return planes;
}

/**
 * subsample cloud on planes
 * @param cloud_in
 * @param ratio
 * @param thres_curvature
 * @param thres_dist
 * @param thres_size
 * @return
 */
Cloud::Ptr
subsample_plane (const Cloud::ConstPtr &cloud_in,
                 const float ratio,
                 const float thres_curvature,
                 const float thres_dist,
                 const size_t thres_size)
{
  pcl::ExtractIndices <Point> extract;
  Cloud::Ptr cloud (new Cloud ());
  std::vector <Cloud> cloud_on_planes;

  const std::vector <Segment> &planes = seg_gr (cloud_in, thres_curvature,
                                                thres_dist, thres_size);

  // extracts the clouds on planes
  for (const Segment &seg : planes)
  {
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());

    cloud_on_planes.push_back (Cloud ());
    inliers->indices = seg.first;

    extract.setInputCloud (cloud_in);
    extract.setIndices (inliers);
    extract.setNegative (false);
    extract.filter (cloud_on_planes.back ());
  }

  // extract the rest of cloud
  extract.setInputCloud (cloud_in);
  extract.setIndices (get_indice_rest (planes, cloud_in->size ()));
  extract.setNegative (false);
  extract.filter (*cloud);

  // now the planes are saved in cloud_on_planes, and the rest in *cloud
  for (size_t id_cloud = 0; id_cloud < cloud_on_planes.size (); ++id_cloud)
  {
    Cloud::Ptr sampled (new Cloud ());
    pcl::RandomSample <Point> sampler;
    pcl::ProjectInliers <Point> proj;
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());

    time_t seconds;
    time (&seconds);

    // subsample on each plane
    sampler.setInputCloud (boost::make_shared<Cloud> (cloud_on_planes.at (id_cloud)));
    sampler.setSample ((unsigned int) (ratio * (double) cloud_on_planes.at (id_cloud).size ()));
    sampler.setSeed ((unsigned int) seconds);
    sampler.filter (*sampled);

    coefficients->values.resize (4);
    coefficients->values[0] = planes.at (id_cloud).second (0);
    coefficients->values[1] = planes.at (id_cloud).second (1);
    coefficients->values[2] = planes.at (id_cloud).second (2);
    coefficients->values[3] = planes.at (id_cloud).second (3);

    // project the subsampled point onto planes
    proj.setModelType (pcl::SACMODEL_PLANE);
    proj.setInputCloud (sampled);
    proj.setModelCoefficients (coefficients);
    proj.filter (*sampled);

    *cloud += *sampled;
  }
  return cloud;
}

/**
 * subsample cloud
 * @param cloud
 * @param method should be {ratio} or pl{ratio}, p{ratio} is accepted
 * @param thres_dist
 * @param thres_size
 * @return
 */
Cloud::Ptr
subsample (const Cloud::ConstPtr &cloud,
           const std::string &method,
           const float thres_dist,
           const size_t thres_size)
{
  auto filter_number_string = [] (const std::string &string)->std::string
  {
    std::string re;
    re.reserve (string.size ());
    for (const char ch: string)
    {
      if ((ch >= '0' && ch <= '9') || ch == '.' || ch == ',')
      {
        re.push_back (ch);
      }
    }
    return re;
  };

  const bool is_planar = method.find ("p") != std::string::npos;
  const float ratio = std::stof (filter_number_string (method));
  Cloud::Ptr re (new Cloud ());

  if (is_planar)
  {
    return subsample_plane (cloud, ratio, 0.05f, thres_dist, thres_size);
  }
  else
  {
    return subsample_all (cloud, ratio);
  }
}

/**
 * subsample all clouds in dir_in to dir_out with method
 * @param dir_in
 * @param dir_out
 * @param method should be {ratio} or pl{ratio}, ratio is float between [0,1]
 * @param thres_dist
 * @param thres_size
 * @param time
 */
void
subsample_dir (const std::string &dir_in,
               const std::string &dir_out,
               const std::string &method,
               const float thres_dist,
               const size_t thres_size,
               double &time)
{
  Timer timer;
  const std::vector <std::string> filenames = get_all_files (dir_in, ".ply");
  time = 0.0;
  for (const std::string &filename: filenames)
  {
    Cloud::Ptr cloud (new Cloud ());
    pcl::io::loadPLYFile (dir_in + "/" + filename, *cloud);
    timer.startNow ();
    Cloud::Ptr sub = subsample (cloud, method, thres_dist, thres_size);
    time += timer.getSecFromStart ();
    std::cout << time << std::endl;
    pcl::io::savePLYFileBinary (dir_out + "/" + filename, *sub);
  }
}

#endif
