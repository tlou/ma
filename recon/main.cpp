#include <fstream>
#include <boost/program_options.hpp>

#include "common.h"
#include "mls.hpp"
#include "split.hpp"
#include "config.hpp"
#include "subsample.hpp"
#include "reconstruction.hpp"
#include "merge.hpp"

/**
 * real processing part, it loads the input data and config file, process all the way through
 * the workflow and save the data to path_output. Inbetween the tmp data will be saved
 * to "workspace" as in config.yaml
 * @param path_input **.ply
 * @param path_output **.ply
 * @param path_log path to log the processing time, can be any ascii file (existing or not)
 * @param path_config **.yaml
 */
void
proc (const std::string &path_input,
      const std::string &path_output,
      const std::string &path_log,
      const std::string &path_config)
{
  /**
   * function tp log the processing time
   */
  auto log = [] (const std::string &path,
                 const std::string &text,
                 const double time)->void
  {
    std::ofstream os;
    os.open (path, std::ios_base::app);
    os << std::fixed << text << ": " << time << std::endl;
    os.close ();
  };
  namespace fs = std::experimental::filesystem;
  // read parameters and configurations for all
  const float mls_radius = std::stof (read_config (path_config, "mls_radius"));
  const float split_size = std::stof (read_config (path_config, "split_size"));
  const float boundary_size = std::stof (read_config (path_config, "boundary_size"));
  const std::string subsample_method (read_config (path_config, "subsample_method"));
  const float thres_dist = std::stof (read_config (path_config, "threshold_distance_plane"));
  const size_t thres_size = std::stoul (read_config (path_config, "threshold_size_group"));
  const std::string reconstruction_method (read_config (path_config, "reconstruction_method"));
  const std::string path_tmp (read_config (path_config, "workspace"));

  // read the cloud from the file
  Cloud::Ptr cloud (new Cloud ());
  if (fs::is_regular_file (fs::path (path_input))
      && fs::path (path_input).extension ().string () == ".ply")
  {
    // file exists and has right extension
    std::cout << "read from file " << path_input << std::endl;
    pcl::io::loadPLYFile (path_input, *cloud);
  }

  if (mls_radius > 0.0f)
  {
    // radius for mls is positive, then apply mls
    double time = 0.0;
    std::cout << "applying mls to the point cloud" << std::endl;
    cloud = mls (cloud, mls_radius, time);
    log (path_log, "mls", time);
  }

  if (split_size > 0.0f)
  {
    // splitting size is positive, then split the cloud to dir
    double time = 0.0;
    std::cout << "splitting the point cloud with size " << split_size << std::endl;
    std::system ((std::string ("mkdir -p ") + path_tmp + "/subclouds").c_str ());
    split (cloud, path_tmp + "/subclouds", split_size, boundary_size, time);
    log (path_log, "split", time);
  }

  if (!subsample_method.empty ())
  {
    if (!fs::is_directory (fs::path (path_tmp + "/subclouds")))
    {
      std::cout << path_tmp + "/subclouds" << "not found" << std::endl;
      return;
    }
    // subsample config valid
    double time = 0.0;
    std::cout << "subsampling cloud in " << path_tmp + "/subclouds"
              << " to " << path_tmp + "/subsampled" << std::endl;
    std::system ((std::string ("mkdir -p ") + path_tmp + "/subsampled").c_str ());
    subsample_dir (path_tmp + "/subclouds", path_tmp + "/subsampled",
                   subsample_method, thres_dist, thres_size, time);
    log (path_log, "subsample", time);
  }

  // reconstruct accordingly
  std::system ((std::string ("mkdir -p ") + path_tmp + "/recon").c_str ());
  if (reconstruction_method == "poisson")
  {
    double time = 0.0;
    const std::string path_exe_recon (read_config (path_config, "path_exe_poisson_recon"));
    const std::string path_exe_trim (read_config (path_config, "path_exe_poisson_trim"));
    const int level_recon = std::stoi (read_config (path_config, "level_recon_poisson"));
    const int level_trim = std::stoi (read_config (path_config, "level_trim_poisson"));
    std::cout << "triangulating with poisson and maximal depth " << level_recon
              << " and trimming with level " << level_trim << std::endl;
    triangulate_poisson_dir (path_exe_recon, path_exe_trim,
                             path_tmp + "/subsampled",
                             path_tmp + "/recon",
                             level_recon, level_trim, time);
    log (path_log, "poisson", time);
  }
  else if (reconstruction_method == "hoppe")
  {
    double time = 0.0;
    const float size = std::stof (read_config (path_config, "size_cube_hoppe"));
    std::cout << "triangulating with hoppe and cube radius " << size << std::endl;
    triangulate_hoppe_dir (path_tmp + "/subsampled",
                           path_tmp + "/recon",
                           size, time);
    log (path_log, "hoppe", time);
  }
  else if (reconstruction_method == "bpa")
  {
    double time = 0.0;
    const float radius = std::stof (read_config (path_config, "radius_bpa"));
    std::cout << "triangulating with bpa and radius " << radius << std::endl;
    triangulate_bpa_dir (path_tmp + "/subsampled",
                         path_tmp + "/recon",
                         radius, time);
    log (path_log, "bpa", time);
  }
  else
  {
    std::cout << "method is wrong, should be poisson, hoppe or bpa" << std::endl;
    return;
  }

  {
    // merge and save to path_output
    double time = 0.0;
    merge_mesh_dir (path_tmp + "/recon", path_output, time);
    log (path_log, "merge", time);
  }
}

int
main (int argn, char **argv)
{
  namespace po = boost::program_options;
  // get absolute path from relative path
  auto get_abs_path = [] (const std::string &path)->std::string
  {
    namespace fs = std::experimental::filesystem;
    return fs::absolute (fs::path (path)).string ();
  };

  // parse the arguments
  po::options_description desc{"Options"};
  desc.add_options ()
    ("input,i", po::value<std::string> (), "path for input file")
    ("output,o", po::value<std::string> (), "path output file")
    ("log,l", po::value<std::string> (), "path for log file")
    ("config,c", po::value<std::string> (), "path for config file");

  po::variables_map vm;
  po::store (po::parse_command_line (argn, argv, desc), vm);
  po::notify (vm);

  assert (vm.count ("input") && vm.count ("output") && vm.count ("log"));

  const std::string path_in (get_abs_path (vm["input"].as<std::string> ()));
  const std::string path_log (get_abs_path (vm["log"].as<std::string> ()));
  const std::string path_out (get_abs_path (vm["output"].as<std::string> ()));
  const std::string path_config (get_abs_path (vm["config"].as<std::string> ()));
  std::cout << path_in << std::endl;
  std::cout << path_log << std::endl;
  std::cout << path_out << std::endl;
  std::cout << path_config << std::endl;

  proc (path_in, path_out, path_log, path_config);

  return 0;
}
