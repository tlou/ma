#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <sstream>
#include <fstream>

#include <Eigen/Dense>

#include <pcl/point_types.h>
#include <pcl/io/ply_io.h>
#include <pcl/common/common_headers.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/surface/mls.h>

#include "Timer.hpp"

typedef pcl::PointXYZRGBNormal Point;
typedef pcl::PointCloud<Point> Cloud;

/**
 *
 * @param filename
 * @return
 */
Cloud::Ptr
load_ply (const std::string &filename)
{
  pcl::PCLPointCloud2::Ptr cloud_in (new pcl::PCLPointCloud2 ());
  Cloud::Ptr cloud (new Cloud ());
  pcl::PLYReader reader;
  reader.read (filename, *cloud_in);
  pcl::fromPCLPointCloud2 (*cloud_in, *cloud);
  return cloud;
}

/**
 *
 * @param cloud_in
 * @param radius_search
 * @param radius_sample
 * @return
 */
Cloud::Ptr
regenerate (const Cloud::ConstPtr &cloud_in,
            const double radius_search,
            const double radius_sample)
{
  Cloud::Ptr cloud_regen (new Cloud ());
  pcl::search::KdTree<Point>::Ptr tree (new pcl::search::KdTree<Point> ());
  pcl::MovingLeastSquares<Point, Point> mls;

  mls.setInputCloud (cloud_in);
  mls.setComputeNormals (true);
  mls.setPolynomialFit (true);
  mls.setSearchMethod (tree);
  mls.setSearchRadius (radius_search);
  //mls.setUpsamplingRadius(radius_sample); // not seem to work
  mls.process (*cloud_regen);

  return cloud_regen;
}

void
print_help ()
{
  std::cout << "./regenerate_mls path_input_cloud_ply path_output_cloud_ply search_radius resample_radius(not used)"
            << std::endl;
}

int
main (int argc, char **argv)
{
  const std::string fn_in (argv[1]);
  const std::string fn_out (argv[2]);
  const double radius_search = std::stof (argv[3]);
  const double radius_sample = std::stof (argv[4]);

  Cloud::Ptr cloud_in = load_ply (fn_in);
  Timer timer;
  Cloud::Ptr result = regenerate (cloud_in, radius_search, radius_sample);
  pcl::io::savePLYFileBinary<Point> (fn_out, *result);

  std::ofstream os;
  os.open (fn_out + "_log");
  os << std::fixed << "running time is " << timer.getSecFromStart () << ", from " << timer.getStart ()
     << " to " << timer.getEnd () << std::endl;
  os.close ();

  return 0;
}
