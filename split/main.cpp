#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <sstream>
#include <fstream>

#include <pcl/common/common_headers.h>
#include <pcl/filters/passthrough.h>
#include <pcl/io/ply_io.h>

#include "Timer.hpp"

typedef pcl::PointXYZRGBNormal Point;
typedef pcl::PointCloud<Point> Cloud;
typedef std::vector<Eigen::Vector2f, Eigen::aligned_allocator<Eigen::Vector2f> > VVector2f;

std::mutex mutex;

/**
 * get the ranges of segments in one dimension with the total range, number of segments and boundary length
 * @param range
 * @param size_sub_range
 * @param size_boundary
 * @return
 */
VVector2f
get_sub_range (const Eigen::Vector2f &range,
               const float size_sub_range,
               const float size_boundary)
{
  const float boundary = size_boundary;
  const float len = range (1) - range (0);
  const int num = (int) std::ceil (len / size_sub_range);
  const float start = (range (1) + range (0)) / 2.0f - size_sub_range * 0.5f * (float) num;

  VVector2f result;
  result.reserve (num);
  for (int i = 0; i < num; ++i)
  {
    const float start_ = start + size_sub_range * (float) i;
    result.push_back (Eigen::Vector2f (start_ - boundary, start_ + boundary + size_sub_range));
  }
  return result;
}

/**
 * passthrough filter
 * @param cloud_in
 * @param limx
 * @param limy
 * @param cloud_out
 */
void
pass_through (const Cloud::ConstPtr &cloud_in,
              const Eigen::Vector2f &limx,
              const Eigen::Vector2f &limy,
              Cloud::Ptr &cloud_out)
{
  pcl::PassThrough<Point> pass;
  cloud_out->clear ();

  pass.setInputCloud (cloud_in);
  pass.setFilterFieldName ("y");
  pass.setFilterLimits (limy (0), limy (1));
  pass.filter (*cloud_out);

  pass.setInputCloud (cloud_out);
  pass.setFilterFieldName ("x");
  pass.setFilterLimits (limx (0), limx (1));
  pass.filter (*cloud_out);
}

/**
 *
 * @param filename
 * @return
 */
Cloud::Ptr
load_ply (const std::string &filename)
{
  pcl::PCLPointCloud2::Ptr cloud_in (new pcl::PCLPointCloud2 ());
  Cloud::Ptr cloud (new Cloud ());
  pcl::PLYReader reader;
  reader.read (filename, *cloud_in);
  pcl::fromPCLPointCloud2 (*cloud_in, *cloud);
  return cloud;
}

/**
 * get the maxima of cloud in horizontal coordinate
 * @param cloud
 * @param limx
 * @param limy
 */
void
get_maxima (const Cloud::ConstPtr &cloud,
            Eigen::Vector2f &limx,
            Eigen::Vector2f &limy)
{
  Point pmin, pmax;
  pcl::getMinMax3D (*cloud, pmin, pmax);
  limx << pmin.x, pmax.x;
  limy << pmin.y, pmax.y;
}

/**
 * thread function for splitting
 * @param cloud_in
 * @param filename
 * @param limx
 * @param limy
 * @param remaining
 */
void
filter_thread (const Cloud::ConstPtr &cloud_in,
               const std::string &filename,
               const Eigen::Vector2f &limx,
               const Eigen::Vector2f &limy,
               int &remaining)
{
  mutex.lock ();
  --remaining;
  mutex.unlock ();

  std::cout << filename << "\n";
  Cloud::Ptr cloud_out (new Cloud ());
  pass_through (cloud_in, limx, limy, cloud_out);
  if (!cloud_out->empty ())
  {
    pcl::PCLPointCloud2::Ptr cloud_write (new pcl::PCLPointCloud2 ());
    pcl::toPCLPointCloud2 (*cloud_out, *cloud_write);
    pcl::PLYWriter writer;
    writer.write (filename, cloud_write, Eigen::Vector4f::Zero (),
                  Eigen::Quaternionf::Identity (), true, true);
  }

  mutex.lock ();
  ++remaining;
  mutex.unlock ();
}

/**
 *
 * @param cloud_in
 * @param directory
 * @param size_sub_range
 * @param size_boundary
 */
void
thread_manager (const Cloud::ConstPtr &cloud_in,
                const std::string &directory,
                const float size_sub_range,
                const float size_boundary)
{
  // number of threads
  int remaining = 1;
  int count = 0;
  Eigen::Vector2f limx, limy;
  get_maxima (cloud_in, limx, limy);

  VVector2f vec_range_x = get_sub_range (limx, size_sub_range, size_boundary);
  VVector2f vec_range_y = get_sub_range (limy, size_sub_range, size_boundary);
  std::vector<std::thread> vec_thread;

  for (const Eigen::Vector2f &range_x : vec_range_x)
  {
    float mean_x = (range_x (0) + range_x (1)) / 2.0f;
    for (const Eigen::Vector2f &range_y : vec_range_y)
    {
      float mean_y = (range_y (0) + range_y (1)) / 2.0f;
      while (remaining <= 0)
      {
        std::this_thread::sleep_for (std::chrono::milliseconds (10));
      }

      std::stringstream ss;
      ss << directory << "/" << mean_x << "_" << mean_y << "_" << size_sub_range << ".ply";

      vec_thread.push_back (std::thread (&filter_thread, cloud_in, ss.str (),
                                         range_x, range_y, std::ref (remaining)));
      ++count;
    }
  }

  for (std::thread &thread : vec_thread)
  {
    thread.join ();
  }
}

void
print_help ()
{
  std::cout << "./split path_input_cloud path_output_directory splitting_size size_boundary"
            << std::endl;
}

int
main (int argc, char **argv)
{
  if (argc)
  {
    print_help ();
    return 1;
  }
  const std::string filename (argv[1]);
  const std::string directory (argv[2]);
  const float size_sub_range = std::stof (argv[3]);
  const float size_boundary = std::stof (argv[4]);

  std::system ((std::string ("rm -rf ") + directory).c_str ());
  std::system ((std::string ("mkdir ") + directory).c_str ());

  Cloud::Ptr cloud_in = load_ply (filename);
  Timer timer;
  Cloud::Ptr cloud_out (new Cloud);

  thread_manager (cloud_in, directory, size_sub_range, size_boundary);

  std::ofstream os;
  os.open (directory + "/log");
  os << std::fixed << "running time is " << timer.getSecFromStart () << ", from " << timer.getStart ()
     << " to " << timer.getEnd () << std::endl;
  os.close ();


  return 0;
}

