#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <sstream>

#include <Eigen/Dense>

#include <pcl/point_types.h>
#include <pcl/io/ply_io.h>
#include <pcl/common/common_headers.h>
#include <pcl/filters/random_sample.h>

typedef pcl::PointXYZRGBNormal Point;
typedef pcl::PointCloud<Point> Cloud;

/**
 *
 * @param filename
 * @return
 */
Cloud::Ptr
load_ply (const std::string &filename)
{
  pcl::PCLPointCloud2::Ptr cloud_in (new pcl::PCLPointCloud2 ());
  Cloud::Ptr cloud (new Cloud ());
  pcl::PLYReader reader;
  reader.read (filename, *cloud_in);
  pcl::fromPCLPointCloud2 (*cloud_in, *cloud);
  return cloud;
}

/**
 *
 * @param cloud
 * @param ratio
 * @return
 */
Cloud::Ptr
subsample (const Cloud::ConstPtr &cloud, const double ratio)
{
  pcl::RandomSample<Point> sampler;
  Cloud::Ptr sampled (new Cloud ());

  time_t seconds;
  time (&seconds);

  sampler.setInputCloud (cloud);
  sampler.setSample ((unsigned int) (ratio * (double) cloud->size ()));
  sampler.setSeed ((unsigned int) seconds);
  sampler.filter (*sampled);

  return sampled;
}

void
print_help ()
{
  std::cout << "./subsample path_input_cloud_ply path_output_cloud_ply ratio(float)"
            << std::endl;
}

int
main (int argc, char **argv)
{
  if (argc <= 1)
  {
    print_help ();
    return 1;
  }
  const std::string fn_in (argv[1]);
  const std::string fn_out (argv[2]);
  const double ratio = std::stof (argv[3]);

  Cloud::Ptr cloud_in = load_ply (fn_in);
  Cloud::Ptr result = subsample (cloud_in, ratio);
  pcl::io::savePLYFileBinary<Point> (fn_out, *result);

  return 0;
}
