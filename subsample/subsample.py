import sys
import os
import time

DIR_IN = sys.argv[1]
DIR_OUT = sys.argv[2]
RATIO = float(sys.argv[3])
EXE = os.getenv('HOME') + "/workspace/ma/subsample/build/subsample"

if not os.path.isdir(DIR_IN):
    print "input dir not found"
    sys.exit()

if not os.path.isfile(EXE):
    print "exe not found"
    sys.exit()

os.makedirs(DIR_OUT)

list_fn = tuple([fn for fn in os.listdir(DIR_IN) if ".ply" in fn])
time_start = time.time()
for fn in list_fn:
    command = EXE + " " + DIR_IN + "/" + fn + " " + DIR_OUT + "/" + fn + " {}".format(RATIO)
    os.system(command)

time_end = time.time()
with open(DIR_OUT + "/log", 'w') as out:
    out.write("running time is {}, from {} to {}".format(\
            time_end - time_start, time_start, time_end))
