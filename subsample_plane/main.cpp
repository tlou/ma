#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <sstream>

#include <Eigen/Dense>

#include <pcl/point_types.h>
#include <pcl/io/ply_io.h>
#include <pcl/common/common_headers.h>
#include <pcl/common/centroid.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/random_sample.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/search/kdtree.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/region_growing.h>

typedef pcl::PointXYZRGBNormal Point;
typedef pcl::PointCloud<Point> Cloud;
typedef std::vector<int> Index;
typedef std::pair<Index, Eigen::Vector4f> Segment;

/**
 *
 * @param filename
 * @return
 */
Cloud::Ptr
load_ply (const std::string &filename)
{
  pcl::PCLPointCloud2::Ptr cloud_in (new pcl::PCLPointCloud2 ());
  Cloud::Ptr cloud (new Cloud ());
  pcl::PLYReader reader;
  reader.read (filename, *cloud_in);
  pcl::fromPCLPointCloud2 (*cloud_in, *cloud);
  return cloud;
}

/**
 * transform from centroid to origin
 * @param cloud_in
 * @return
 */
Eigen::Matrix4f
getTransform (const Cloud::ConstPtr &cloud_in)
{
  Eigen::Matrix4f affine = Eigen::Matrix4f::Identity ();
  Eigen::Vector4f translate;
  Eigen::Matrix3f rotation;
  pcl::compute3DCentroid<Point> (*cloud_in, translate);
  affine.block (0, 3, 3, 1) << translate.segment (0, 3);
  return affine;
}

/**
 *
 * @param cloud
 * @param thres_dist
 * @return
 */
Segment
fit_plane (const Cloud::ConstPtr &cloud, const double thres_dist)
{
  pcl::SACSegmentation<Point> seg;
  Eigen::Matrix4f transform = getTransform (cloud);
  Cloud::Ptr cloud_tf (new Cloud ());
  pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
  pcl::PointIndicesPtr inliers (new pcl::PointIndices ());
  Eigen::Vector4f plane;

  pcl::transformPointCloud<Point> (*cloud, *cloud_tf,
                                   Eigen::Matrix4f (transform.inverse ()));

  seg.setOptimizeCoefficients (true);
  seg.setModelType (pcl::SACMODEL_PLANE);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setDistanceThreshold (thres_dist);
  seg.setInputCloud (cloud_tf);
  seg.segment (*inliers, *coefficients);

  plane << coefficients->values[0], coefficients->values[1],
    coefficients->values[2], coefficients->values[3];
  plane (3) -= transform.col (3).segment (0, 3).dot (plane.segment (0, 3));

  return Segment (inliers->indices, plane);
}

/**
 * get indices of points not on segments
 * @param segments
 * @param size_total
 * @return
 */
pcl::PointIndices::Ptr
get_indice_rest (const std::vector<Segment> &segments, const size_t size_total)
{
  std::vector<bool> is_in (size_total, false);
  pcl::PointIndices::Ptr id_rest (new pcl::PointIndices ());

  for (const Segment &seg : segments)
  {
    for (const int id : seg.first)
    {
      is_in.at (id) = true;
    }
  }

  for (size_t id = 0; id < size_total; ++id)
  {
    if (!is_in.at (id))
    {
      id_rest->indices.push_back ((int) id);
    }
  }

  return id_rest;
}

/**
 * cluster with growing region
 * @param cloud
 * @param thres_curvature
 * @return
 */
std::vector<pcl::PointIndices>
cluster_gr (const Cloud::ConstPtr &cloud, const float thres_curvature)
{
  typedef pcl::PointXYZ PointIn0;
  typedef pcl::Normal PointIn1;
  std::vector<Segment> list_segment;
  pcl::RegionGrowing<PointIn0, PointIn1> rg;
  pcl::search::Search<PointIn0>::Ptr tree (new pcl::search::KdTree<PointIn0> ());
  pcl::PCLPointCloud2 cloud2;
  pcl::PointCloud<PointIn0>::Ptr cloud_xyz (new pcl::PointCloud<PointIn0> ());
  pcl::PointCloud<PointIn1>::Ptr cloud_normal (new pcl::PointCloud<PointIn1> ());

  pcl::toPCLPointCloud2 (*cloud, cloud2);
  pcl::fromPCLPointCloud2 (cloud2, *cloud_xyz);
  pcl::fromPCLPointCloud2 (cloud2, *cloud_normal);

  rg.setMinClusterSize (50);
  rg.setMaxClusterSize (1000000);
  rg.setSearchMethod (tree);
  rg.setNumberOfNeighbours (10);
  rg.setInputCloud (cloud_xyz);
  rg.setInputNormals (cloud_normal);
  rg.setSmoothnessThreshold ((float) (3.0 / 180.0 * M_PI));
  rg.setCurvatureThreshold (thres_curvature);

  std::vector<pcl::PointIndices> clusters;
  rg.extract (clusters);

  return clusters;
}

/**
 * plane fitting on growing regions
 * @param cloud
 * @param thres_curvature
 * @param thres_size
 * @return
 */
std::vector<Segment>
seg_gr (const Cloud::ConstPtr &cloud, const float thres_curvature,
        const size_t thres_size)
{
  std::vector<Segment> planes;
  std::vector<pcl::PointIndices> clusters = cluster_gr (cloud, thres_curvature);

  planes.reserve (clusters.size ());
  for (const pcl::PointIndices &indice : clusters)
  {
    if (indice.indices.size () > thres_size)
    {
      Cloud::Ptr sub_cloud (new Cloud ());
      boost::shared_ptr<pcl::PointIndices> inliers = boost::make_shared<pcl::PointIndices> (indice);
      pcl::ExtractIndices<Point> extract;

      extract.setInputCloud (cloud);
      extract.setIndices (inliers);
      extract.setNegative (false);
      extract.filter (*sub_cloud);

      Segment seg = fit_plane (sub_cloud, 0.01);
      if (seg.first.size () > indice.indices.size () / 4)
      {
        Index indice_origin;
        planes.push_back (seg);
        // seg.first is the indices of inliers in sub_cloud, need to map back
        for (int i : seg.first)
        {
          indice_origin.push_back (indice.indices.at (i));
        }
        planes.back ().first.swap (indice_origin);
      }
    }
  }

  return planes;
}

/**
 * @deprecated
 * subsample only on planes without clustering
 * @param cloud_in
 * @param thres_dist
 * @param thres_size
 * @return
 */
std::vector<Segment>
seg_plane (const Cloud::ConstPtr &cloud_in,
           const double thres_dist,
           const size_t thres_size)
{
  Cloud::Ptr cloud (new Cloud ());
  std::vector<Segment> planes;
  pcl::copyPointCloud (*cloud_in, *cloud);

  for (size_t iter = 0; iter < 100; ++iter)
  {
    Segment seg = fit_plane (cloud, thres_dist);

    if (seg.first.size () < thres_size)
    {
      break;
    }
    else
    {
      pcl::ExtractIndices<Point> extract;
      pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());

      inliers->indices = seg.first;
      extract.setInputCloud (cloud);
      extract.setIndices (inliers);
      extract.setNegative (true);
      extract.filter (*cloud);

      planes.push_back (seg);

      if (cloud->size () < thres_size)
      {
        break;

      }
    }
  }
  return planes;
}

/**
 * subsample on planes after clustering
 * @param cloud_in
 * @param planes
 * @param thres_size
 * @param ratio
 * @return
 */
Cloud::Ptr
subsample_plane (const Cloud::ConstPtr &cloud_in,
                 const std::vector<Segment> &planes,
                 const size_t thres_size,
                 const double ratio)
{
  pcl::ExtractIndices<Point> extract;
  Cloud::Ptr cloud (new Cloud ());
  std::vector<Cloud> cloud_on_planes;

  for (const Segment &seg : planes)
  {
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());

    cloud_on_planes.push_back (Cloud ());
    inliers->indices = seg.first;

    extract.setInputCloud (cloud_in);
    extract.setIndices (inliers);
    extract.setNegative (false);
    extract.filter (cloud_on_planes.back ());
  }

  // get remaining points
  extract.setInputCloud (cloud_in);
  extract.setIndices (get_indice_rest (planes, cloud_in->size ()));
  extract.setNegative (false);
  extract.filter (*cloud);

  // now the planes are saved in cloud_on_planes, and the rest in *cloud
  for (size_t id_cloud = 0; id_cloud < cloud_on_planes.size (); ++id_cloud)
  {
    Cloud::Ptr sampled (new Cloud ());
    pcl::RandomSample<Point> sampler;
    pcl::ProjectInliers<Point> proj;
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());

    time_t seconds;
    time (&seconds);

    // subsample
    sampler.setInputCloud (boost::make_shared<Cloud> (cloud_on_planes.at (id_cloud)));
    sampler.setSample ((unsigned int) (ratio * (double) cloud_on_planes.at (id_cloud).size ()));
    sampler.setSeed ((unsigned int) seconds);
    sampler.filter (*sampled);

    coefficients->values.resize (4);
    coefficients->values[0] = planes.at (id_cloud).second (0);
    coefficients->values[1] = planes.at (id_cloud).second (1);
    coefficients->values[2] = planes.at (id_cloud).second (2);
    coefficients->values[3] = planes.at (id_cloud).second (3);

    // project onto plane
    proj.setModelType (pcl::SACMODEL_PLANE);
    proj.setInputCloud (sampled);
    proj.setModelCoefficients (coefficients);
    proj.filter (*sampled);

    *cloud += *sampled;
  }
  return cloud;
}

void
print_help ()
{
  std::cout << "./subsample_plane path_input_cloud_ply path_output_cloud_ply ratio(float) size_plane"
            << std::endl;
}

int
main (int argc, char **argv)
{
  if (argc <= 1)
  {
    print_help ();
    return 1;
  }
  const std::string fn_in (argv[1]);
  const std::string fn_out (argv[2]);
  const double ratio = std::stof (argv[3]);
  const size_t size_plane = (size_t) std::stoi (argv[4]);

  Cloud::Ptr cloud_in = load_ply (fn_in);

  Cloud::Ptr result = subsample_plane (cloud_in, seg_gr (cloud_in, 0.05, size_plane), size_plane, ratio);
  pcl::io::savePLYFileBinary<Point> (fn_out, *result);

  return 0;
}
