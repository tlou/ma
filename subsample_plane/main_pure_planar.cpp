/*
 * this code subsamples the point cloud on all fitted planes, serves as comparison only
 */
#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <sstream>

#include <Eigen/Dense>

#include <pcl/point_types.h>
#include <pcl/io/ply_io.h>
#include <pcl/common/common_headers.h>
#include <pcl/common/centroid.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/random_sample.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/search/kdtree.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/region_growing.h>

typedef pcl::PointXYZRGBNormal Point;
typedef pcl::PointCloud<Point> Cloud;
typedef std::vector<int> Index;
typedef std::pair<Index, Eigen::Vector4f> Segment;

/**
 *
 * @param filename
 * @return
 */
Cloud::Ptr
load_ply (const std::string &filename)
{
  pcl::PCLPointCloud2::Ptr cloud_in (new pcl::PCLPointCloud2 ());
  Cloud::Ptr cloud (new Cloud ());
  pcl::PLYReader reader;
  reader.read (filename, *cloud_in);
  pcl::fromPCLPointCloud2 (*cloud_in, *cloud);
  return cloud;
}

/**
 * transform from centroid to origin
 * @param cloud_in
 * @return
 */
Eigen::Matrix4f
getTransform (const Cloud::ConstPtr &cloud_in)
{
  Eigen::Matrix4f affine = Eigen::Matrix4f::Identity ();
  Eigen::Vector4f translate;
  Eigen::Matrix3f rotation;
  pcl::compute3DCentroid<Point> (*cloud_in, translate);
  affine.block (0, 3, 3, 1) << translate.segment (0, 3);
  return affine;
}

/**
 *
 * @param cloud
 * @param thres_dist
 * @return
 */
Segment
fit_plane (const Cloud::ConstPtr &cloud, const double thres_dist)
{
  pcl::SACSegmentation<Point> seg;
  Eigen::Matrix4f transform = getTransform (cloud);
  Cloud::Ptr cloud_tf (new Cloud ());
  pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
  pcl::PointIndicesPtr inliers (new pcl::PointIndices ());
  Eigen::Vector4f plane;

  pcl::transformPointCloud<Point> (*cloud, *cloud_tf,
                                   Eigen::Matrix4f (transform.inverse ()));

  seg.setOptimizeCoefficients (true);
  seg.setModelType (pcl::SACMODEL_PLANE);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setDistanceThreshold (thres_dist);
  seg.setInputCloud (cloud_tf);
  seg.segment (*inliers, *coefficients);

  plane << coefficients->values[0], coefficients->values[1],
    coefficients->values[2], coefficients->values[3];
  plane (3) -= transform.col (3).segment (0, 3).dot (plane.segment (0, 3));

  return Segment (inliers->indices, plane);
}

/**
 *
 * @param cloud_in
 * @param thres_size minimal number of points on plane
 * @param ratio
 * @return
 */
Cloud::Ptr
subsample_plane (const Cloud::ConstPtr &cloud_in,
                 const size_t thres_size,
                 const double ratio)
{
  Cloud::Ptr re (new Cloud ());
  Cloud::Ptr copy (new Cloud ());

  re->reserve (cloud_in->size ());
  *copy = *cloud_in;
  for (size_t iter = 0; iter < 200; ++iter)
  {
    // fit plane
    Segment segment = fit_plane (copy, 0.01);
    if (segment.first.size () > thres_size)
    {
      Cloud::Ptr cloud_plane (new Cloud ());
      Cloud::Ptr cloud_plane_sampled (new Cloud ());
      Cloud::Ptr cloud_plane_sampled_proj (new Cloud ());
      cloud_plane->reserve (segment.first.size ());
      for (const int id: segment.first)
      {
        cloud_plane->push_back (copy->at (id));
      }

      // extract points on plane
      pcl::PointIndices::Ptr indices (new pcl::PointIndices ());
      pcl::ExtractIndices<Point> extract;
      pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
      indices->indices = segment.first;
      extract.setIndices (indices);
      extract.setInputCloud (copy);

      extract.setNegative (false);
      extract.filter (*cloud_plane);

      extract.setNegative (true);
      extract.filter (*copy);

      // subsample
      pcl::RandomSample<Point> sampler;
      pcl::ProjectInliers<Point> proj;
      time_t seconds;
      time (&seconds);

      sampler.setInputCloud (cloud_plane);
      sampler.setSample ((unsigned int) (ratio * (double) cloud_plane->size ()));
      sampler.setSeed ((unsigned int) seconds);
      sampler.filter (*cloud_plane_sampled);

      coefficients->values.resize (4);
      coefficients->values[0] = segment.second (0);
      coefficients->values[1] = segment.second (1);
      coefficients->values[2] = segment.second (2);
      coefficients->values[3] = segment.second (3);

      // project on plane
      proj.setModelType (pcl::SACMODEL_PLANE);
      proj.setInputCloud (cloud_plane_sampled);
      proj.setModelCoefficients (coefficients);
      proj.filter (*cloud_plane_sampled_proj);

      *re += *cloud_plane_sampled_proj;
    }
    else
    {
      *re += *copy;
      break;
    }
  }


  return re;
}

void
print_help ()
{
  std::cout << "./subsample_plane_purely path_input_cloud_ply path_output_cloud_ply ratio(float) size_plane"
            << std::endl;
}

int
main (int argc, char **argv)
{
  if (argc <= 1)
  {
    print_help ();
    return 1;
  }
  const std::string fn_in (argv[1]);
  const std::string fn_out (argv[2]);
  const double ratio = std::stof (argv[3]);
  const size_t size_plane = (size_t) std::stoi (argv[4]);

  Cloud::Ptr cloud_in = load_ply (fn_in);

  Cloud::Ptr result = subsample_plane (cloud_in, size_plane, ratio);
  pcl::io::savePLYFileBinary<Point> (fn_out, *result);

  return 0;
}
